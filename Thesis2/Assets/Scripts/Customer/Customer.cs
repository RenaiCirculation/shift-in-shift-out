﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Customer : MonoBehaviour
{
    GameObject GM;
    public NavMeshAgent agent;

    GameObject Cashier;

    public bool InQueue;
    
    public float MaxOrderTime;
    float orderTime;

    [SerializeField]
    int orderNumber;

    bool IsExiting;
    GameObject ExitPos;

    QueueHandler QH;
    bool IsMovingToCenter;

    Vector3 curDes;

    public float IdleResetTimer;
    float internalTimer;

    public CustomerMood CM;

    public bool UseRandomOrders;
    [Range(0,3)]
    public int SetOrderNumber;

    GameObject tableToMove;
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameController");
        CM = GetComponent<CustomerMood>();
        agent = GetComponent<NavMeshAgent>();
        InQueue = false;
        Cashier = null;
        orderTime = Random.Range(0, MaxOrderTime);

        internalTimer = 0;

        IsMovingToCenter = true;

        tableToMove = null;
        IsExiting = false;
        ExitPos = null;
        QH = GameObject.Find("LineManager").GetComponent<QueueHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        //CheckState();
        if(IsExiting)
        {
            if(ExitPos != null)
            {
                float des = Vector3.Distance(transform.position, ExitPos.transform.position);
                if(des <= 1)
                {
                    Destroy(gameObject, 2);
                }
            }
        }

        if (!InQueue)
        {            
            if (IsMovingToCenter)
            {                

                float des = Vector3.Distance(QH.IdleCenter.transform.position, transform.position);
                if (des < .9f)
                {
                    IsMovingToCenter = false;
                }
                else
                {
                    MoveToCenterIdle();
                }
            }
            else
            {
                IdleMovement();
            }
            agent.SetDestination(curDes);
        }        
    }

    void MoveToCenterIdle()
    {
        //agent.SetDestination(QH.IdleCenter.transform.position);
        curDes = QH.IdleCenter.transform.position;
    }

    void IdleMovement()
    {
        if (internalTimer >= IdleResetTimer)
        {
            curDes = ReturnNextIdlePos();
            internalTimer = 0;
        }
        else
        {
            internalTimer += Time.deltaTime;
        }
    }

    Vector3 ReturnNextIdlePos()
    {
        Vector3 V3;
        float x = Random.Range(QH.Idle2.transform.position.x, QH.Idle3.transform.position.x);
        float z = Random.Range(QH.Idle1.transform.position.z, QH.Idle2.transform.position.z);

        V3 = new Vector3(x, transform.position.y, z);
        //Debug.Log(V3);
        return V3;
    }

    
    public float ReturnOrderTime()
    {        
        return MaxOrderTime;
    }

    public int GetOrder()
    {

        if(UseRandomOrders)
        {
            orderNumber = Random.Range(0, GM.GetComponent<Menu>().NumberOfItems);
        }
        else
        {
            orderNumber = SetOrderNumber;
        }
        return orderNumber;
    }

    public int ReturnOrderNumber()
    {
        return orderNumber;
    }

    public void MoveToExit(GameObject x)
    {
        ExitPos = x;
        IsExiting = true;
        agent.SetDestination(x.transform.position);
    }

    public void MoveTo(Transform x)
    {
        agent.SetDestination(x.position);
    }     
    
    public void RecievedOrder()
    {
        int x = Random.Range(0, 100);
        if(x < 1)
        {
            //takeout
            MoveToExit(QH.RestoExitPos);
        }
        else
        {
            //dine in
            tableToMove = GM.GetComponent<GameManager>().FindEmptyTable();
            if(tableToMove != null)
            {
                MoveTo(tableToMove.GetComponent<Table>()
                    .ReturnEmptySeat(this.gameObject).transform);
            }
        }
    }
    

}
