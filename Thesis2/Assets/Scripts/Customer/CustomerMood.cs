﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CustomerMood : MonoBehaviour
{
    GameObject camera;
    Customer C;
    QueueHandler QH;
    StringsDatabase SD;

    GameManager GM;

    public Image CircleIndicator;

    public GameObject Canvas;
    public GameObject TabDisplay;

    public float QueueTime;
    float internalQueueTime;

    public float WaitingTime;
    float internalWaitingTime;

    bool IsInQueue, IsInWaiting;

    bool enableCanvas;

    bool DisplayQueueMsg, DisplayWaitingMsg;

    public GameObject LargeTxtBox, MedTxtBox, SmallTxtBox;
    bool LargeBoxEnabled, MedBoxEnabled, SmallBoxEnabled;

    public float MessageTimer;
    float internalMT;
    string MsgToDisplay;

    // Start is called before the first frame update
    void Start()
    {
        internalQueueTime = QueueTime;
        internalWaitingTime = WaitingTime;
        Canvas.SetActive(true);
        TabDisplay.SetActive(false);
        C = GetComponent<Customer>();
        QH = GameObject.Find("LineManager").GetComponent<QueueHandler>();
        SD = GameObject.FindGameObjectWithTag("GameController").GetComponent<StringsDatabase>();
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        camera = GameObject.Find("Main Camera");
        enableCanvas = false;

        DisplayQueueMsg = false;
        DisplayWaitingMsg = false;
        MsgToDisplay = null;

        LargeBoxEnabled = false;
        MedBoxEnabled = false;
        SmallBoxEnabled = false;

        LargeTxtBox.SetActive(false);
        MedTxtBox.SetActive(false);
        SmallTxtBox.SetActive(false);

        internalMT = 0;
    }
    public void StartedQueuing()
    {
        IsInQueue = true;
    }
    public void EndedQueue()
    {
        IsInQueue = false;
    }
    public void StartedWaiting()
    {
        IsInWaiting = true;
    }
    public void EndedWaiting()
    {
        IsInWaiting = false;
    }

    // Update is called once per frame
    void Update()
    {
        Canvas.transform.rotation = Quaternion.LookRotation(transform.position - camera.transform.position);
        //Canvas.transform.LookAt(camera.transform);
        //Canvas.SetActive(enableCanvas);
        
        if (IsInQueue)
        {
            QueueTimer();
            CheckQueueDisplayMessage();
            CheckEnabledMenu();
        }
        else
        {
            //StopMessageBox();
            internalQueueTime = QueueTime;
        }

        if(IsInWaiting)
        {
            WaitingTimer();
            CheckWaitingDisplayMessage();            
            CheckEnabledMenu();
        }
        else
        {
            //StopMessageBox();
            internalWaitingTime = WaitingTime;
        }

        if (Input.GetKey(KeyCode.Tab))
        {
            //enableCanvas = true;
            TabDisplay.SetActive(true);
        }
        else
        {
            TabDisplay.SetActive(false);
            //enableCanvas = false;
        }
    }

    void QueueTimer()
    {
        CircleIndicator.fillAmount = internalQueueTime / QueueTime;
        if(internalQueueTime > 0)
        {
            internalQueueTime -= Time.deltaTime;
        }
        else
        {
            QH.FindCustomerInQueue(C);
            IsInQueue = false;
            GM.WalkOutCustomers += 1;
            Debug.Log("Exiting");
            DisplayExitMessage();
            GM.DepleteWorkerStamina(5);
            C.MoveToExit(QH.RestoExitPos);
        }

    }

    void CheckQueueDisplayMessage()
    {
        if(internalQueueTime < QueueTime/2)
        {
            if(!DisplayQueueMsg)
            {
                DisplayQueueMsg = true;
                DisplayQueueMessage();
            }
        }
    }

    void DisplayQueueMessage()
    {
        if(DisplayQueueMsg)
        {
            Debug.Log("displaying queue msg");
            MsgToDisplay = SD.ReturnQueueComplaint();
            if(MsgToDisplay.Length < 20)
            {
                SmallTxtBox.SetActive(true);
                SmallTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                SmallBoxEnabled = true;
            }
            else if(MsgToDisplay.Length >= 21 && MsgToDisplay.Length < 50)
            {
                MedTxtBox.SetActive(true);
                MedTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                MedBoxEnabled = true;
            }
            else
            {
                LargeTxtBox.SetActive(true);
                LargeTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                LargeBoxEnabled = true;
            }
            
        }
    }

    void CheckEnabledMenu()
    {
        if (internalMT > MessageTimer)
        {
            if(SmallBoxEnabled)
            {
                SmallTxtBox.SetActive(false);
                SmallBoxEnabled = false;
            }
            if (MedBoxEnabled)
            {
                MedTxtBox.SetActive(false);
                MedBoxEnabled = false;
            }
            if (LargeBoxEnabled)
            {
                LargeTxtBox.SetActive(false);
                LargeBoxEnabled = false;
            }
            MsgToDisplay = null;
            internalMT = 0;
        }
        else
        {
            internalMT += Time.deltaTime;
        }
    }

    void StopMessageBox()
    {
        if (SmallBoxEnabled)
        {
            SmallTxtBox.SetActive(false);
            SmallBoxEnabled = false;
        }
        if (MedBoxEnabled)
        {
            MedTxtBox.SetActive(false);
            MedBoxEnabled = false;
        }
        if (LargeBoxEnabled)
        {
            LargeTxtBox.SetActive(false);
            LargeBoxEnabled = false;
        }
        MsgToDisplay = null;
        internalMT = 0;
    }

    void WaitingTimer()
    {
        CircleIndicator.fillAmount = internalWaitingTime / WaitingTime;
        if(internalWaitingTime > 0)
        {
            internalWaitingTime -= Time.deltaTime;
        }
        else
        {
            QH.FindCustomerInWaitingQueue(C);
            IsInWaiting = false;
            Debug.Log("Exiting");
            DisplayExitMessage();
            GM.WalkOutCustomers += 1;
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().
                    CanvasUI.GetComponent<OrderDisplay>().RemoveOrder();
            GM.DepleteWorkerStamina(5);
            C.MoveToExit(QH.RestoExitPos);
        }
    }

    void CheckWaitingDisplayMessage()
    {        
        if (internalWaitingTime < WaitingTime / 2)
        {            
            if (!DisplayWaitingMsg)
            {
                DisplayWaitingMsg = true;
                DisplayWaitingMessage();
            }
        }
    }

    void DisplayWaitingMessage()
    {
        if (DisplayWaitingMsg)
        {
            Debug.Log("displaying waiting msg");
            MsgToDisplay = SD.ReturnWaitingComplaint();
            if (MsgToDisplay.Length < 20)
            {
                SmallTxtBox.SetActive(true);
                SmallTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                SmallBoxEnabled = true;
            }
            else if (MsgToDisplay.Length >= 21 && MsgToDisplay.Length < 50)
            {
                MedTxtBox.SetActive(true);
                MedTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                MedBoxEnabled = true;
            }
            else
            {
                LargeTxtBox.SetActive(true);
                LargeTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
                LargeBoxEnabled = true;
            }

        }
    }

    void DisplayExitMessage()
    {
        StopMessageBox();
        MsgToDisplay = SD.ReturnExitComplaint();
        if (MsgToDisplay.Length < 20)
        {
            SmallTxtBox.SetActive(true);
            SmallTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
            SmallBoxEnabled = true;
        }
        else if (MsgToDisplay.Length >= 21 && MsgToDisplay.Length < 50)
        {
            MedTxtBox.SetActive(true);
            MedTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
            MedBoxEnabled = true;
        }
        else
        {
            LargeTxtBox.SetActive(true);
            LargeTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = MsgToDisplay;
            LargeBoxEnabled = true;
        }
    }

}
