﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodStationStorage : MonoBehaviour
{
    WorkStation WS;


    public Image CircleIndicator;
    public Text CurrentAmtDisplay;

    public GameObject CircleObject, AmtObject;   

    public int FoodAmountMax;
    
    public int currentFoodAmount;

    public float TimeToReplenish;
    float internalTTR;

    // Start is called before the first frame update
    void Start()
    {
        currentFoodAmount = 0;
        internalTTR = 0;

        WS = GetComponent<WorkStation>();
    }

    // Update is called once per frame
    void Update()
    {
        UIUpdate();
        Replenish();
    }

    void UIUpdate()
    {
        CurrentAmtDisplay.text = currentFoodAmount.ToString();
        CircleIndicator.fillAmount = internalTTR / TimeToReplenish;
    }

    void Replenish()
    {
        if(currentFoodAmount < FoodAmountMax)
        {
            if (!WS.ReturnAvailable())
            {
                if (internalTTR > TimeToReplenish)
                {
                    currentFoodAmount += 1;
                    internalTTR = 0;
                    Debug.Log("depleted");
                    WS.DepleteStaminaCurrentWorker(2);
                }
                else
                {
                    internalTTR += Time.deltaTime * WS.ReturnCurrentWorker().GetComponent<WorkerMood>().ReturnStaminaPercentage(); ;
                }
            }
            
            
        }
    }

    public void GetFoodItem()
    {
        currentFoodAmount--;
    }

    public bool HasReserve()
    {
        if(currentFoodAmount > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
