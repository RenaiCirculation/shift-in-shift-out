﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PrepUIObj : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{
    StationManager SM;
    public PrepUI prepUI;

    public PrepUI.FoodItems ThisItem;

    [SerializeField]
    bool inside;
    public bool HeldDown;

    public GameObject MousePrefab;
    GameObject MouseObj;

    GameObject CanvasUI;

    public Text CurAmtLeft;
    public int currentStock;
    GameObject curMachine;
    void Start()
    {
        HeldDown = false;
        MouseObj = null;
        inside = false;

        SM = GameObject.FindGameObjectWithTag("GameController").GetComponent<StationManager>();
        CanvasUI = GameObject.Find("CanvasUI");
    }

    // Update is called once per frame
    void Update()
    {
        if(curMachine == null)
        {
            switch(ThisItem)
            {
                case PrepUI.FoodItems.Burger:
                    curMachine = SM.BurgerMachine;
                    break;

                case PrepUI.FoodItems.Chicken:
                    curMachine = SM.ChickenMachine;
                    break;

                case PrepUI.FoodItems.Drink:
                    curMachine = SM.DrinkMachine;
                    break;

                case PrepUI.FoodItems.FrenchFries:
                    curMachine = SM.FriesMachine;
                    break;

                case PrepUI.FoodItems.Milkshake:
                    curMachine = SM.MilkShakeMachine;
                    break;

                case PrepUI.FoodItems.Pasta:
                    curMachine = SM.PastaMachine;
                    break;

                case PrepUI.FoodItems.Rice:
                    curMachine = SM.RiceMachine;
                    break;
            }
        }
        else
        {
            currentStock = curMachine.GetComponent<FoodStationStorage>().currentFoodAmount;
            CurAmtLeft.text = currentStock.ToString();
        }
        if(inside && HeldDown == false)
        {
            if (Input.GetMouseButton(0))
            {
                prepUI.RecievePrepObj(this);
                HeldDown = true;
            }
        }

        if(HeldDown)
        {
            if (Input.GetMouseButtonUp(0))
            {
                HeldDown = false;
                prepUI.curObj = null;
            }

            if (MouseObj == null)
            {
                MouseObj = Instantiate(MousePrefab, transform.position, Quaternion.identity);
                MouseObj.transform.parent = CanvasUI.transform;
            }
            else
            {
                MouseObj.transform.position =
                new Vector3(Input.mousePosition.x + 30.0f,
                Input.mousePosition.y - 15.0f, 0);
            }
        }

        else
        {
            Destroy(MouseObj);
        }
    }

    public void MinusInventory()
    {
        curMachine.GetComponent<FoodStationStorage>().currentFoodAmount--;
    }

    public void OnPointerEnter(PointerEventData eventData) // 3
    {       
        if(currentStock> 0)
        {
            inside = true;
        }
        
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        inside = false;
    }
}
