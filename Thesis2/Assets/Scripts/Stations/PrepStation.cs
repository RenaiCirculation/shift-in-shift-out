﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PrepStation : MonoBehaviour
{

    bool IsMenuOpen;

    public GameObject PrepUI;
    public GameObject Clicker;

    CameraMover CM;

    WorkStation WS;
    ClaimStation CS;
    public FoodStation FS;

    public GameObject UItoDisplay;
    
    // Start is called before the first frame update
    void Start()
    {
        IsMenuOpen = false;
        PrepUI = GameObject.Find("PrepAreaUI");
        PrepUI.SetActive(false);
        UItoDisplay.SetActive(false);

        WS = GetComponent<WorkStation>();
        CS = GameObject.Find("ClaimArea").GetComponent<ClaimStation>();
        CM = GameObject.Find("Main Camera").GetComponent<CameraMover>();

        FS = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!WS.ReturnAvailable())
        {
            UItoDisplay.SetActive(true);
            //if (Clicker.GetComponent<ReturnClickedObject>().IsClicked)
            //{
            //    if (!IsMenuOpen)
            //    {
            //        IsMenuOpen = true;
            //        OpenPrepMenu();
            //    }
            //}
        }
        else
        {
            UItoDisplay.SetActive(false);
        }
        
    }

    public void SendToClaimArea(int x)
    {
        Debug.Log("SENT ORDER --- " + x);
        CS.RecieveOrder(x);
    }

    public void GetFS(FoodStation x)
    {
        FS = x;
        //Debug.Log(x.name);
    }

    public void RemoveFS()
    {
       
        FS = null;
    }

    public void OpenPrepMenu()
    {
        WS.DisableWorker();
        PrepUI.SetActive(true);
        //CM.LockCamToLoc(this.gameObject);
        CM.LockCamera = true;
        IsMenuOpen = true;
        PrepUI.GetComponent<PrepUI>().StartMenu();
    }

    public void ClosePrepMenu()
    {
        WS.EnableWorker();
        CM.LockCamera = false   ;
        PrepUI.SetActive(false);
        IsMenuOpen = false;
        CM.LockCamera = false;
    }

    public bool ClaimStationHasWorker()
    {
        return CS.HasWorker;
    }
}