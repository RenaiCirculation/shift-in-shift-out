﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkStation : MonoBehaviour
{
    public Transform WorkerStationPos;
    GameObject currentWorker;
    bool HasWorker;

    GameObject DragWorker;
    Collider objCollider;

    Renderer rend;
    public Shader NormalShader;
    public Shader HighlightedShader;

    public bool AssignMovement;

    public enum WorkerDirection
    {
        Up,
        Down,
        Left,
        Right
    }
    public WorkerDirection WD;


    // Start is called before the first frame update
    void Start()
    {
        DragWorker = null;
        HasWorker = false;

        objCollider = GetComponent<Collider>();
        rend = GetComponent<Renderer>();
        rend.material.shader = NormalShader;
        objCollider.enabled = true;

        AssignMovement = false;
    }

    public GameObject ReturnCurrentWorker()
    {
        return currentWorker;
    }

    public bool ReturnAvailable()
    {
        if (HasWorker)
            return false;
        else
            return true;
    }

    public void ChangeShader(int x )
    {
        switch(x)
        {
            case 1:
                rend.material.shader = NormalShader;
                break;

            case 2:
                rend.material.shader = HighlightedShader;
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!HasWorker )
        {
            if (currentWorker == null)
            {
                if (DragWorker != null)
                {
                    if (DragWorker.GetComponent<DragController>().IsDragged == false)
                    {
                        //Debug.Log("ENTERED");
                        AssignWorker();
                    }
                }
            }
            objCollider.enabled = true;
        }
        else
        {
            if (!AssignMovement)
            {
                float dist = Vector3.Distance(currentWorker.transform.position, WorkerStationPos.position);
                if (currentWorker != null)
                {

                    if (dist > 2f)
                    {
                        DragWorker = null;
                        currentWorker = null;
                        HasWorker = false;
                    }
                    else
                    {
                        if (DragWorker.GetComponent<DragController>().IsDragged == false)
                            AssignWorker();
                    }
                }
            }
            objCollider.enabled = false;
        }

        
    }

    void AssignWorker()
    {
        HasWorker = true;
        currentWorker = DragWorker;
        switch(WD)
        {
            case WorkerDirection.Down:
                currentWorker.transform.rotation = Quaternion.Euler(0, 270, currentWorker.transform.rotation.z);
                break;

            case WorkerDirection.Left:
                currentWorker.transform.rotation = Quaternion.Euler(0, 0, currentWorker.transform.rotation.z);
                break;

            case WorkerDirection.Right:
                currentWorker.transform.rotation = Quaternion.Euler(0, 180, currentWorker.transform.rotation.z);
                break;

            case WorkerDirection.Up:
                currentWorker.transform.rotation = Quaternion.Euler(0, 90, currentWorker.transform.rotation.z);
                break;
        }
       
        currentWorker.transform.position = 
            WorkerStationPos.transform.position;
    }

    public void DisableWorker()
    {
        currentWorker.GetComponent<DragController>().DisableCollider();
    }

    public void EnableWorker()
    {
        currentWorker.GetComponent<DragController>().EnableCollider();
    }

    public void DepleteStaminaCurrentWorker(float x)
    {
        if(currentWorker != null)
        {
            currentWorker.GetComponent<WorkerMood>().DepleteStamina(x,2);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "Worker")
        {
            DragWorker = other.transform.gameObject;
            //Debug.Log(DragWorker.name);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Worker")
        {
            DragWorker = null;
            currentWorker = null;
            HasWorker = false;
        }
    }
}
