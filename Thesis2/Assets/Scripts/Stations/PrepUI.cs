﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PrepUI : MonoBehaviour
{
    public bool StartedMenu;
    bool cursorInside;

    PrepStation PS;
    public Image Side1, Side2, Side3;

    public Sprite BlankSprite;
    public Sprite DrinkSprite, FriesSprite, RiceSprite,
        BurgerSprite, ChickenSprite, PastaSprite, MilkshakeSprite;   

    public enum FoodItems
    {
        None,
        Drink,
        FrenchFries,
        Rice,
        Burger,
        Chicken,
        Pasta,
        Milkshake
    }

    public FoodItems TrayArea1, TrayArea2, TrayArea3;

    public GameObject DrinksObj, ShakeObj, FriesObj, RiceObj,
        ChickenObj, PastaObj, BurgerObj;

    public PrepUIObj curObj;
    PrepUIObj internalObj;

    public void RecievePrepObj(PrepUIObj x)
    {
        if(curObj == null)
        {
            curObj = x;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartedMenu = false;

        PS = GameObject.Find("PrepArea").GetComponent<PrepStation>();
        TrayArea1 = FoodItems.None;
        TrayArea2 = FoodItems.None;
        TrayArea3 = FoodItems.None;

        cursorInside = false;
        curObj = null;
        internalObj = null;
    }

    // Update is called once per frame
    void Update()
    {        
        UpdateSprites();

        if(cursorInside)
        {
            //Inside();
            GetIngredient();
        }           
    }

    public void StartMenu()
    {
        StartedMenu = true;
    }

    public void GarbageBin()
    {
        TrayArea1 = FoodItems.None;
        TrayArea2 = FoodItems.None;
        TrayArea3 = FoodItems.None;
    }

    void UpdateSprites()
    {
        switch(TrayArea1)
        {
            case FoodItems.None:
                Side1.sprite = BlankSprite;
                break;

            case FoodItems.Drink:
                Side1.sprite = DrinkSprite;
                break;

            case FoodItems.FrenchFries:
                Side1.sprite = FriesSprite;
                break;

            case FoodItems.Rice:
                Side1.sprite = RiceSprite;
                break;

            case FoodItems.Burger:
                Side1.sprite = BurgerSprite;
                break;

            case FoodItems.Chicken:
                Side1.sprite = ChickenSprite;
                break;

            case FoodItems.Pasta:
                Side1.sprite = PastaSprite;
                break;

            case FoodItems.Milkshake:
                Side1.sprite = MilkshakeSprite;
                break;

        }
        switch (TrayArea2)
        {
            case FoodItems.None:
                Side2.sprite = BlankSprite;
                break;

            case FoodItems.Drink:
                Side2.sprite = DrinkSprite;
                break;

            case FoodItems.FrenchFries:
                Side2.sprite = FriesSprite;
                break;

            case FoodItems.Rice:
                Side2.sprite = RiceSprite;
                break;

            case FoodItems.Burger:
                Side2.sprite = BurgerSprite;
                break;

            case FoodItems.Chicken:
                Side2.sprite = ChickenSprite;
                break;

            case FoodItems.Pasta:
                Side2.sprite = PastaSprite;
                break;

            case FoodItems.Milkshake:
                Side2.sprite = MilkshakeSprite;
                break;

        }
        switch (TrayArea3)
        {
            case FoodItems.None:
                Side3.sprite = BlankSprite;
                break;

            case FoodItems.Drink:
                Side3.sprite = DrinkSprite;
                break;

            case FoodItems.FrenchFries:
                Side3.sprite = FriesSprite;
                break;

            case FoodItems.Rice:
                Side3.sprite = RiceSprite;
                break;

            case FoodItems.Burger:
                Side3.sprite = BurgerSprite;
                break;

            case FoodItems.Chicken:
                Side3.sprite = ChickenSprite;
                break;

            case FoodItems.Pasta:
                Side3.sprite = PastaSprite;
                break;

            case FoodItems.Milkshake:
                Side3.sprite = MilkshakeSprite;
                break;

        }

    }

    public void RecieveIngredient()
    {        
        if (PS.FS != null)//revamp
        {           
            if (PS.FS.FSD.HeldDown)
            {  
                
                cursorInside = true;
                Debug.Log("over");
            }            
        }
    }

    public void UIRecieve()
    {   
        if(curObj != null)
        {
            Debug.Log("inside");
            internalObj = curObj;
            cursorInside = true;
        }
    }

    public void GetIngredient()
    {
        if (Input.GetMouseButtonUp(0))
        {
            cursorInside = false;
            if (TrayArea1 == FoodItems.None)
            {
                TrayArea1 = internalObj.ThisItem;
                internalObj.MinusInventory();
                internalObj = null;
                return;
            }

            if (TrayArea2 == FoodItems.None)
            {
                TrayArea2 = internalObj.ThisItem;
                internalObj.MinusInventory();
                internalObj = null;
                return;
            }

            if (TrayArea3 == FoodItems.None)
            {
                TrayArea3 = internalObj.ThisItem;
                internalObj.MinusInventory();
                internalObj = null;
                return;
            }

        }
    }

    public void Inside()
    {
       if(Input.GetMouseButtonUp(0))
       {
            cursorInside = false;            
            if(TrayArea1 == FoodItems.None)
            {
                switch (PS.FS.CurrentStationType)
                {
                    case FoodStation.FoodStationType.Drink:
                        TrayArea1 = FoodItems.Drink;
                        break;
                    case FoodStation.FoodStationType.FrenchFries:
                        TrayArea1 = FoodItems.FrenchFries;
                        break;
                    case FoodStation.FoodStationType.Rice:
                        TrayArea1 = FoodItems.Rice;
                        break;
                    case FoodStation.FoodStationType.Burger:
                        TrayArea1 = FoodItems.Burger;
                        break;
                    case FoodStation.FoodStationType.Chicken:
                        TrayArea1 = FoodItems.Chicken;
                        break;
                    case FoodStation.FoodStationType.Pasta:
                        TrayArea1 = FoodItems.Pasta;
                        break;
                    case FoodStation.FoodStationType.Milkshake:
                        TrayArea1 = FoodItems.Milkshake;
                        break;
                }
                PS.FS.FSS.GetFoodItem();
                PS.RemoveFS();
                return;
            }

            if (TrayArea2 == FoodItems.None)
            {
                switch (PS.FS.CurrentStationType)
                {
                    case FoodStation.FoodStationType.Drink:
                        TrayArea2 = FoodItems.Drink;
                        break;
                    case FoodStation.FoodStationType.FrenchFries:
                        TrayArea2 = FoodItems.FrenchFries;
                        break;
                    case FoodStation.FoodStationType.Rice:
                        TrayArea2 = FoodItems.Rice;
                        break;
                    case FoodStation.FoodStationType.Burger:
                        TrayArea2 = FoodItems.Burger;
                        break;
                    case FoodStation.FoodStationType.Chicken:
                        TrayArea2 = FoodItems.Chicken;
                        break;
                    case FoodStation.FoodStationType.Pasta:
                        TrayArea2 = FoodItems.Pasta;
                        break;
                    case FoodStation.FoodStationType.Milkshake:
                        TrayArea2 = FoodItems.Milkshake;
                        break;
                }
                PS.FS.FSS.GetFoodItem();
                PS.RemoveFS();
                return;
            }

            if (TrayArea3 == FoodItems.None)
            {
                switch (PS.FS.CurrentStationType)
                {
                    case FoodStation.FoodStationType.Drink:
                        TrayArea3 = FoodItems.Drink;
                        break;
                    case FoodStation.FoodStationType.FrenchFries:
                        TrayArea3 = FoodItems.FrenchFries;
                        break;
                    case FoodStation.FoodStationType.Rice:
                        TrayArea3 = FoodItems.Rice;
                        break;
                    case FoodStation.FoodStationType.Burger:
                        TrayArea3 = FoodItems.Burger;
                        break;
                    case FoodStation.FoodStationType.Chicken:
                        TrayArea3 = FoodItems.Chicken;
                        break;
                    case FoodStation.FoodStationType.Pasta:
                        TrayArea3 = FoodItems.Pasta;
                        break;
                    case FoodStation.FoodStationType.Milkshake:
                        TrayArea3 = FoodItems.Milkshake;
                        break;
                }
                PS.FS.FSS.GetFoodItem();
                PS.RemoveFS();
                return;
            }
        }
    }

    public void Send()
    {
        if(PS.ClaimStationHasWorker())
        {
            if (TrayArea1 == FoodItems.None &&
           TrayArea2 == FoodItems.None &&
           TrayArea3 == FoodItems.None)

            {
                Debug.Log("NONE ON TRAY");
            }
            else
            {
                PS.SendToClaimArea(ReturnMenuItem());
                GarbageBin();
                PS.ClosePrepMenu();
            }
        }
        else
        {
            Debug.Log("No worker");
        }
        

        
    }

    int ReturnMenuItem()
    {
        if(ChickenMeal())
        {
            Debug.Log("Chicken Meal");
            return 0;            
        }

        else if(BurgerMeal())
        {
            Debug.Log("Burger Meal");
            return 1;
        }

        else if (Milkshake())
        {
            Debug.Log("Milkshake");
            return 2;
        }

        else if (SpaghettiMeal())
        {
            Debug.Log("Spag Meal");
            return 3;
        }       

        else 
            return 9;
    }

    bool ChickenMeal()
    {
        bool HasDrink = false;
        bool HasChicken = false;
        bool HasRice = false;

        if (TrayArea1 == FoodItems.Drink ||
           TrayArea2 == FoodItems.Drink ||
           TrayArea3 == FoodItems.Drink)
        {
            if(!HasDrink)
            {
                HasDrink = true;
            }            
        }

        if (TrayArea1 == FoodItems.Chicken ||
           TrayArea2 == FoodItems.Chicken ||
           TrayArea3 == FoodItems.Chicken)
        {
            if (!HasChicken)
            {
                HasChicken = true;
            }
        }

        if (TrayArea1 == FoodItems.Rice ||
           TrayArea2 == FoodItems.Rice ||
           TrayArea3 == FoodItems.Rice)
        {
            if (!HasRice)
            {
                HasRice = true;
            }
        }

        if (HasRice == true && 
            HasChicken == true && 
            HasDrink == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool BurgerMeal()
    {
        bool HasDrink = false;
        bool HasBurger = false;
        bool HasFries = false;

        if (TrayArea1 == FoodItems.Drink ||
           TrayArea2 == FoodItems.Drink ||
           TrayArea3 == FoodItems.Drink)
        {
            if (!HasDrink)
            {
                HasDrink = true;
            }
        }

        if (TrayArea1 == FoodItems.Burger ||
           TrayArea2 == FoodItems.Burger ||
           TrayArea3 == FoodItems.Burger)
        {
            if (!HasBurger)
            {
                HasBurger = true;
            }
        }

        if (TrayArea1 == FoodItems.FrenchFries ||
           TrayArea2 == FoodItems.FrenchFries ||
           TrayArea3 == FoodItems.FrenchFries)
        {
            if (!HasFries)
            {
                HasFries = true;
            }
        }

        if (HasBurger == true &&
            HasFries == true &&
            HasDrink == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool SpaghettiMeal()
    {
        bool HasDrink = false;
        bool HasPasta = false;
        bool HasFries = false;

        if (TrayArea1 == FoodItems.Drink ||
           TrayArea2 == FoodItems.Drink ||
           TrayArea3 == FoodItems.Drink)
        {
            if (!HasDrink)
            {
                HasDrink = true;
            }
        }

        if (TrayArea1 == FoodItems.Pasta ||
           TrayArea2 == FoodItems.Pasta ||
           TrayArea3 == FoodItems.Pasta)
        {
            if (!HasPasta)
            {
                HasPasta = true;
            }
        }

        if (TrayArea1 == FoodItems.FrenchFries ||
           TrayArea2 == FoodItems.FrenchFries ||
           TrayArea3 == FoodItems.FrenchFries)
        {
            if (!HasFries)
            {
                HasFries = true;
            }
        }

        if (HasPasta == true &&
            HasFries == true &&
            HasDrink == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Milkshake()
    {
        bool HasMilkshake = false;
       
        if (TrayArea1 == FoodItems.Milkshake ||
          TrayArea2 == FoodItems.Milkshake ||
          TrayArea3 == FoodItems.Milkshake)
        {
            if(!HasMilkshake)
                HasMilkshake = true;
           
            
        }

        if (HasMilkshake == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
