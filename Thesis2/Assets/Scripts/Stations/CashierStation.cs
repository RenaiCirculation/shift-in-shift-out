﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashierStation : MonoBehaviour
{
    WorkStation WS;
    QueueHandler QH;
    Menu menu;

    public float TimeToCheck;
    float interTimer;

    public Transform CounterArea;
    bool HasCustomerOrdering;
    bool HasTakenOrder;

    Customer currentCustomer;

    float interOrderTimer;

    // Start is called before the first frame update
    void Start()
    {
        HasCustomerOrdering = false;
        HasTakenOrder = false;

        currentCustomer = null;
        WS = GetComponent<WorkStation>();
        QH = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<GameManager>().QH;
        menu = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<Menu>();

        interOrderTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(HasWorker())
        {
            UpdateLine();
        }

        else
        {

            //do nothing
        }

        
    }

    void UpdateLine()
    {            
        if(QH.CanAddCustomerWaiting())
        {
            if (QH.HasCustomers())
            {
                if (!HasCustomerOrdering)
                {
                    WS.DisableWorker();
                    interTimer += Time.deltaTime;
                    if (interTimer >= TimeToCheck)
                    {
                        currentCustomer = QH.GetFirstInLine();
                        MoveNextCustomer(currentCustomer);  
                    }
                }
            }           
        }   


        if(HasCustomerOrdering && !HasTakenOrder)
        {
            float dis = Vector3.Distance(currentCustomer.transform.position, CounterArea.position);
            //Debug.Log(currentCustomer.name + " == " + dis);
            if(dis <= .8f)
            {
                if (interOrderTimer >= currentCustomer.ReturnOrderTime())
                {
                    TakeOrder();

                }
                else
                {
                    interOrderTimer += Time.deltaTime;
                }
            }                  
        }
        
    }

    void TakeOrder()
    {
        menu.AddNewCustomerOrder(currentCustomer.GetOrder());
        HasTakenOrder = true;
        Debug.Log("Took order " + currentCustomer.name);
        MoveCustomerExit(currentCustomer);
        WS.DepleteStaminaCurrentWorker(2);
    }

    void MoveCustomerExit(Customer x)
    {
        interTimer = 0;
        QH.DoneOrdering(x);
        HasTakenOrder = false;
        HasCustomerOrdering = false;
        currentCustomer = null;
        WS.EnableWorker();
    }

    void MoveNextCustomer(Customer x)
    {
        interOrderTimer = 0;
        HasCustomerOrdering = true;
        x.MoveTo(CounterArea);
        Debug.Log("Moved to counter " + x.name);
       
    }

    public bool HasWorker()
    {
        if(GetComponent<WorkStation>().ReturnAvailable() == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
