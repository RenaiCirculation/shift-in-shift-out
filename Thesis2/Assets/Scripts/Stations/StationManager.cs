﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationManager : MonoBehaviour
{
    GameManager GM;

    public GameObject BurgerMachine, PastaMachine, ChickenMachine,
        RiceMachine, FriesMachine, MilkShakeMachine, DrinkMachine;


    // Start is called before the first frame update
    void Start()
    {
        GM = GetComponent<GameManager>();
       
    }

    public void AssignMachines()
    {
        
        foreach (GameObject x in GM.CurrentStations)
        {
            
            if (x.GetComponent<FoodStation>() != null)
            {
                switch(x.GetComponent<FoodStation>().CurrentStationType)
                {
                    case FoodStation.FoodStationType.Burger:
                        BurgerMachine = x;
                        break;

                    case FoodStation.FoodStationType.Chicken:
                        ChickenMachine = x;
                        break;

                    case FoodStation.FoodStationType.Drink:
                        DrinkMachine = x;
                        break;

                    case FoodStation.FoodStationType.FrenchFries:
                        FriesMachine = x;
                        break;

                    case FoodStation.FoodStationType.Milkshake:
                        MilkShakeMachine = x;
                        break;

                    case FoodStation.FoodStationType.Pasta:
                        PastaMachine = x;
                        break;

                    case FoodStation.FoodStationType.Rice:
                        RiceMachine = x;
                        break;

                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
