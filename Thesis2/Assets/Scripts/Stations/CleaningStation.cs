﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CleaningStation : MonoBehaviour
{
    WorkStation WS;
    GameManager GM;

    public float CleanTimer;
    float cTimer;

    public GameObject TableToClean;

    bool startedCleanRoutine, finishedCleanRoutine;

    float cleaningTimer;

    // Start is called before the first frame update
    void Start()
    {
        WS = GetComponent<WorkStation>();
        cTimer = 0;
        cleaningTimer = 0;
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        TableToClean = null;
        startedCleanRoutine = false;
        finishedCleanRoutine = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!WS.ReturnAvailable())
        {
            if (TableToClean == null)
            {
                if (cTimer > CleanTimer)
                {
                    //ToFindDirtyArea();
                    TableToClean = GM.FindDirtyArea();
                    cTimer = 0;
                }
                else
                {
                    cTimer += Time.deltaTime;
                }
            }
            else
            {
                if (!finishedCleanRoutine)
                {
                    if (!startedCleanRoutine)
                    {
                        WS.DisableWorker();
                        StartCleanRoutine();
                    }
                    else
                    {
                        float des = Vector3.Distance(WS.ReturnCurrentWorker().transform.position, TableToClean.transform.position);
                        if (des < 1.8)
                        {
                            StartCleaning();
                        }
                        Debug.Log(des);
                    }
                }

                else
                {
                    WS.ReturnCurrentWorker().GetComponent<NavMeshAgent>().SetDestination(WS.WorkerStationPos.position);
                    float des = Vector3.Distance(WS.ReturnCurrentWorker().transform.position, WS.WorkerStationPos.position);
                    if(des < .3)
                    {
                        TableToClean = null;
                        finishedCleanRoutine = false;
                        startedCleanRoutine = false;
                        WS.ReturnCurrentWorker().GetComponent<WorkerMood>().DepleteStamina(5, 2);
                        WS.EnableWorker();
                    }
                }
            }
        }
        
        
    }   

    void StartCleanRoutine()
    {
        startedCleanRoutine = true;
        WS.AssignMovement = true;
        WS.ReturnCurrentWorker().GetComponent<NavMeshAgent>().SetDestination(TableToClean.transform.position);
    }

    void StartCleaning()
    {       
        
        if (cleaningTimer > 2)
        {

            cleaningTimer = 0;
            TableToClean.GetComponent<Table>().CleanTable();
            finishedCleanRoutine = true;
            startedCleanRoutine = false;
            WS.ReturnCurrentWorker().GetComponent<NavMeshAgent>().isStopped = false;
        }

        else
        {
            WS.ReturnCurrentWorker().GetComponent<NavMeshAgent>().isStopped = true;
            cleaningTimer += Time.deltaTime;
        }
    }

}
