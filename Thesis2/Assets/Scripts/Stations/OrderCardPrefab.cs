﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderCardPrefab : MonoBehaviour
{
    public Image OrderImage;
    public Image OrderTimer;

    public Image IngreReq1, IngreReq2, IngreReq3;

    public Sprite ChickenSprite, BurgerSprite, SpaghettiSprite, MilkshakeSprite;

    public Sprite BlankSprite, DrinkSprite, ShakeSprite, FriesSprite, RiceSprite,
        BurgerIngSprite, ChickenIngSprite, PastaSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignCard(int i)
    {
        switch(i)
        {
            case 1:
                OrderImage.sprite = ChickenSprite;
                IngreReq1.sprite = DrinkSprite;
                IngreReq2.sprite = RiceSprite;
                IngreReq3.sprite = ChickenIngSprite;

                break;

            case 2:
                OrderImage.sprite = BurgerSprite;
                IngreReq1.sprite = DrinkSprite;
                IngreReq2.sprite = FriesSprite;
                IngreReq3.sprite = BurgerIngSprite;
                break;

            case 3:
                OrderImage.sprite = MilkshakeSprite;
                IngreReq1.sprite = DrinkSprite;
                IngreReq2.sprite = BlankSprite;
                IngreReq3.sprite = BlankSprite;
                break;

            case 4:
                OrderImage.sprite = SpaghettiSprite;
                IngreReq1.sprite = DrinkSprite;
                IngreReq2.sprite = FriesSprite;
                IngreReq3.sprite = SpaghettiSprite;
                break;
        }
    }
}
