﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimStation : MonoBehaviour
{

    WorkStation WS;
    QueueHandler QH;
    Menu menu;
    StatsHandler SH;

    GameManager GM;

    Customer currentCustomer;

    public Transform ClaimPos;

    bool HasOrder;
    bool CCOrderCorrect;

    int OrderNumber;

    public bool HasWorker;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();
        WS = GetComponent<WorkStation>();
        QH = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<GameManager>().QH;
        menu = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<Menu>();

        GM = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<GameManager>();
        OrderNumber = 0;

        HasOrder = false;
        CCOrderCorrect = false;

        HasWorker = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!WS.ReturnAvailable())
        {
            HasWorker = true;
            if (currentCustomer == null)
            {                
                if(QH.HasWaitingCustomers())
                {
                    if (HasOrder)
                    {
                        
                        //GetNextCustomer();
                    }
                }                
            }
            else
            {
                float des = Vector3.Distance(currentCustomer.transform.position, ClaimPos.position);
                //Debug.Log(currentCustomer.name + " --- " + des);
                if (des <= .7f)
                {
                    if (CCOrderCorrect)
                    {
                        HasOrder = false;
                        //currentCustomer.MoveToExit(QH.RestoExitPos);
                        currentCustomer.RecievedOrder();
                        CCOrderCorrect = false;
                        currentCustomer = null;
                    }
                }
            }
                      
        }
        else
        {
            HasWorker = false;
        }
    }  

    void GetNextCustomer()
    {

        // 0 = Chicken meal
        // 1 = Burger Meal
        // 2 = Milkshake
        // 3 = Spaghetti
        // 9 = none of the above

        currentCustomer = QH.GetFirstClaimQueue();
        currentCustomer.MoveTo(ClaimPos);
        //Debug.Log("CUSTOMER ORDERED --" + currentCustomer.ReturnOrderNumber());

        if(IsOrderCorrect())
        {
            PayOrder();
            currentCustomer.GetComponent<CustomerMood>().EndedWaiting();
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().
                CanvasUI.GetComponent<OrderDisplay>().RemoveOrder();         

        }
        else
        {
            Debug.Log("wrong");
        }

    }

    bool IsOrderCorrect()
    {
        Debug.Log(currentCustomer.ReturnOrderNumber() + " ---- " + OrderNumber);
        if (currentCustomer.ReturnOrderNumber() == OrderNumber)
        {
            return true;
        }
        else
        {
            return false;
        }               
    }

    void PayOrder()
    {
        CCOrderCorrect = true;
        switch (currentCustomer.ReturnOrderNumber())
        {
            case 0:
                GM.PayOrder(menu.ChickenMealPrice);
                SH.PayChicken(menu.ChickenMealPrice);
                break;

            case 1:
                GM.PayOrder(menu.BurgerMealPrice);
                SH.PayBurger(menu.BurgerMealPrice);
                break;

            case 2:
                GM.PayOrder(menu.MilkshakePrice);
                SH.PayShake(menu.MilkshakePrice);
                break;

            case 3:
                GM.PayOrder(menu.SpaghettiPrice);
                SH.PaySpag(menu.SpaghettiPrice);
                break;
        }
    }

    public void RecieveOrder(int x)
    {
        OrderNumber = x;
        if(currentCustomer == null)
        {
            currentCustomer = QH.GetFirstClaimQueue();
            currentCustomer.MoveTo(ClaimPos);
        }
        
        GiveOrder();     
    }

    void GiveOrder()
    {
        if(currentCustomer !=null)
        {
            HasOrder = true;
            if (IsOrderCorrect())
            {
                PayOrder();
                currentCustomer.GetComponent<CustomerMood>().EndedWaiting();

                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().
                    CanvasUI.GetComponent<OrderDisplay>().RemoveOrder();


            }
            else
            {
                Debug.Log("wrong");
            }
        }
        else
        {
            Debug.Log("no customer");
        }
    }
}
