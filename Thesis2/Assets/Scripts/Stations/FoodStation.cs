﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodStation : MonoBehaviour
{
    public bool HeldDown;

    public GameObject MousePrefab;
    GameObject MouseObj;

    PrepStation PS;

    WorkStation WS;

    public FoodStationDragging FSD;
    public FoodStationStorage FSS;

    public enum FoodStationType
    {
        Drink,
        FrenchFries,
        Rice,
        Burger,
        Chicken,
        Pasta,
        Milkshake
    }

    public FoodStationType CurrentStationType;

    GameObject CanvasUI;

    // Start is called before the first frame update
    void Start()
    {
        HeldDown = false;
        MouseObj = null;

        CanvasUI = GameObject.Find("CanvasUI");
        PS = GameObject.Find("PrepArea").GetComponent<PrepStation>();
        WS = GetComponent<WorkStation>();
        FSD.gameObject.SetActive(false);
        FSS = GetComponent<FoodStationStorage>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!WS.ReturnAvailable())
        {
            if (FSS.HasReserve())
            {
                FSD.gameObject.SetActive(true);
                if (FSD.HeldDown)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        FSD.HeldDown = false;
                        PS.RemoveFS();
                    }
                    PS.GetFS(this);
                    //ImageOnMouse.SetActive(true);
                    //Debug.Log(Input.mousePosition);
                    if(MouseObj == null)
                    {
                        MouseObj = Instantiate(MousePrefab, transform.position, Quaternion.identity);
                        MouseObj.transform.parent = CanvasUI.transform;
                    }
                    else
                    {
                        MouseObj.transform.position =
                        new Vector3(Input.mousePosition.x + 30.0f,
                        Input.mousePosition.y - 15.0f, 0);
                    }

                    
                }
                else
                {
                    Destroy(MouseObj);
                }
            }
            else
            {
                FSD.gameObject.SetActive(false);
            }

        }


    }

    
}
