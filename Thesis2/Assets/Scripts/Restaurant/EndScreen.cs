﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EndScreen : MonoBehaviour
{
    public Text DayUI;

    public Text CustomersReqUI;
    public Text CustomersServedUI;
    public Text CustomersLeftUI;

    public GameManager GM;

    public Button RetryButton, NextButton;

    // Start is called before the first frame update
    void Start()
    {
        //todo day display
        //DayUI.text = 
        RetryButton.interactable = false;
        NextButton.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        CustomersReqUI.text = "Customers Required: " + GM.MinimumReqCustomers.ToString();
        CustomersServedUI.text ="Customers Served: "+ GM.currentDoneCustomers.ToString();
        CustomersLeftUI.text ="Customers Left: "+  GM.WalkOutCustomers.ToString();

        if(GM.MinimumReqCustomers <= GM.currentDoneCustomers)
        {
            RetryButton.interactable = true;
            NextButton.interactable = true;
        }
        else
        {
            RetryButton.interactable = true;
        }
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
