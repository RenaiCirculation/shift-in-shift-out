﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    public enum TableType
    {
        Seats_1,
        Seats_2,
        Seats_4

    }

    public TableType ThisTableType;

    public GameObject Seat1, Seat2, Seat3, Seat4;

    GameObject Seat1Customer, Seat2Customer, 
        Seat3Customer, Seat4Customer;

    bool Seat1Empty, Seat2Empty, 
        Seat3Empty, Seat4Empty;

    public float CustomerTime;
    float S1Timer, S2Timer, S3Timer, S4Timer;

    public bool S1Dirty, S2Dirty, S3Dirty, S4Dirty;

    public bool HasDirt;

    public GameObject S1Dirt, S2Dirt, S3Dirt, S4Dirt;

    QueueHandler QH;

    // Start is called before the first frame update
    void Start()
    {

        QH = GameObject.Find("LineManager").GetComponent<QueueHandler>();
        Seat1Empty = true;
        Seat2Empty = true;
        Seat3Empty = true;
        Seat4Empty = true;

        Seat1Customer = null;
        Seat2Customer = null;
        Seat3Customer = null;
        Seat4Customer = null;

        S1Dirty = false;
        S2Dirty = false;
        S3Dirty = false;
        S4Dirty = false;

        HasDirt = false;

        S1Timer = 0;
        S2Timer = 0;
        S3Timer = 0;
        S4Timer = 0;

    }

    // Update is called once per frame
    void Update()
    {
        CheckEmptySeats();
        CheckDirt();

        CustomerTimer();

    }

    void CheckEmptySeats()
    {
        if(Seat1Customer !=null)
        {
            Seat1Empty = false;
        }
        else
        {
            Seat1Empty = true;
        }

        if (Seat2Customer != null)
        {
            Seat2Empty = false;
        }
        else
        {
            Seat2Empty = true;
        }

        if (Seat3Customer != null)
        {
            Seat3Empty = false;
        }
        else
        {
            Seat3Empty = true;
        }

        if (Seat4Customer != null)
        {
            Seat4Empty = false;
        }
        else
        {
            Seat4Empty = true;
        }
    }

    void CheckDirt()
    {
        switch(ThisTableType)
        {
            case TableType.Seats_1:
                if (S1Dirty)
                {
                    S1Dirt.SetActive(true);
                }
                else
                {
                    S1Dirt.SetActive(false);
                }
                break;

            case TableType.Seats_2:
                if (S2Dirty)
                {
                    S2Dirt.SetActive(true);
                }
                else
                {
                    S2Dirt.SetActive(false);
                }
                goto case TableType.Seats_1;

            case TableType.Seats_4:
                if (S3Dirty)
                {
                    S3Dirt.SetActive(true);
                }
                else
                {
                    S3Dirt.SetActive(false);
                }

                if (S4Dirty)
                {
                    S4Dirt.SetActive(true);
                }
                else
                {
                    S4Dirt.SetActive(false);
                }
                goto case TableType.Seats_2;
        }

        if(S1Dirty == false && S2Dirty == false && S3Dirty == false && S4Dirty == false)
        {
            HasDirt = false;
        }
        else
        {
            HasDirt = true;
        }

    }

    public void CleanTable()
    {
        S1Dirty = false;
        S2Dirty = false;
        S3Dirty = false;
        S4Dirty = false;

        HasDirt = false;
    }

    void CustomerTimer()
    {
        if(!Seat1Empty)
        {
            if(S1Timer < CustomerTime)
            {
                S1Timer += Time.deltaTime;
            }
            else
            {
                RemoveCustomer(1);
                S1Dirty = true;
                S1Timer = 0;
            }
        }

        if (!Seat2Empty)
        {
            if (S2Timer < CustomerTime)
            {
                S2Timer += Time.deltaTime;
            }
            else
            {
                RemoveCustomer(2);
                S2Dirty = true;
                S2Timer = 0;
            }
        }

        if (!Seat3Empty)
        {
            if (S3Timer < CustomerTime)
            {
                S3Timer += Time.deltaTime;
            }
            else
            {
                RemoveCustomer(3);
                S3Dirty = true;
                S3Timer = 0;
            }
        }

        if (!Seat4Empty)
        {
            if (S4Timer < CustomerTime)
            {
                S4Timer += Time.deltaTime;
            }
            else
            {
                RemoveCustomer(4);
                S4Dirty = true;
                S4Timer = 0;
            }   
        }
    }

    void RemoveCustomer(int pos)
    {
        switch (pos)
        {
            case 1:
                Seat1Customer.GetComponent<Customer>().MoveToExit(QH.RestoExitPos);
                Seat1Customer = null;
                break;

            case 2:
                Seat2Customer.GetComponent<Customer>().MoveToExit(QH.RestoExitPos);
                Seat2Customer = null;
                break;

            case 3:
                Seat3Customer.GetComponent<Customer>().MoveToExit(QH.RestoExitPos);
                Seat3Customer = null;
                break;

            case 4:
                Seat4Customer.GetComponent<Customer>().MoveToExit(QH.RestoExitPos);
                Seat4Customer = null;
                break;

        }

        
    }

    public bool HasEmptySeat()
    {
        switch(ThisTableType)
        {
            case TableType.Seats_1:
                if(Seat1Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }               

            case TableType.Seats_2:
                if (Seat1Empty)
                {
                    return true;
                }
                else
                {
                    if(Seat2Empty)
                    {
                        return true;
                    }
                    else
                        return false;
                }

            case TableType.Seats_4:
                if (Seat1Empty)
                {
                    return true;
                }
                else
                {
                    if (Seat2Empty)
                    {
                        return true;
                    }
                    else
                    {
                        if(Seat3Empty)
                        {
                            return true;
                        }
                        else
                        {
                            if(Seat4Empty)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                       
                }
        }
        return false;
    }

    public GameObject ReturnEmptySeat(GameObject x)
    {
        switch(ThisTableType)
        {
            case TableType.Seats_1:
                Seat1Customer = x;
                return Seat1;

            case TableType.Seats_2:
                if(Seat1Empty)
                {
                    Seat1Customer = x;
                    return Seat1;
                }
                else
                {
                    Seat2Customer = x;
                    return Seat2;
                }
            case TableType.Seats_4:
                if (Seat1Empty)
                {
                    Seat1Customer = x;
                    return Seat1;
                }
                else if(Seat2Empty)
                {
                    Seat2Customer = x;
                    return Seat2;
                }
                else if(Seat3Empty)
                {
                    Seat3Customer = x;
                    return Seat3;
                }
                else
                {
                    Seat4Customer = x;
                    return Seat4;
                }
        }
        return null;
    }
}
