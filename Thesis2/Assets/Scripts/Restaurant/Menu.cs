﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public int NumberOfItems;

    OrderDisplay OD;

    public enum FoodItem
    {
        ChickenMeal,
        BurgerMeal, 
        Milkshake,
        Spaghetti
            
    }

    public float ChickenMealPrice, BurgerMealPrice, 
        MilkshakePrice, SpaghettiPrice;


    // Start is called before the first frame update
    void Start()
    {
        NumberOfItems = Enum.GetNames(typeof(FoodItem)).Length;
        OD = GameObject.Find("CanvasUI").GetComponent<OrderDisplay>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddNewCustomerOrder(int x)
    {
        Debug.Log("ADDED NEW ORDER -- " + x);
        switch(x)
        {
            case 0:
                OD.AddNewOrder(FoodItem.ChickenMeal);
                break;

            case 1:
                OD.AddNewOrder(FoodItem.BurgerMeal);
                break;

            case 2:
                OD.AddNewOrder(FoodItem.Milkshake);
                break;

            case 3:
                OD.AddNewOrder(FoodItem.Spaghetti);
                break;
        }
    }
}
