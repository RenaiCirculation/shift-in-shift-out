﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderDisplay : MonoBehaviour
{

    Menu menu;

    public List<Menu.FoodItem> CustomerOrder = new List<Menu.FoodItem>();

    public List<GameObject> CustomerList = new List<GameObject>();
    public GameObject ContentUI;
    public GameObject OrderPrefab;

    // Start is called before the first frame update
    void Start()
    {
        menu = GameObject.FindGameObjectWithTag("GameController").GetComponent<Menu>();
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void AddNewOrder(Menu.FoodItem x)
    {
        CustomerOrder.Add(x);
        Debug.Log("Added - " + x);
        GameObject z = Instantiate(OrderPrefab, ContentUI.transform.position, Quaternion.identity, ContentUI.transform);
        switch(x)
        {
            case Menu.FoodItem.ChickenMeal:
                z.GetComponent<OrderCardPrefab>().AssignCard(1);
                break;

            case Menu.FoodItem.BurgerMeal:
                z.GetComponent<OrderCardPrefab>().AssignCard(2);
                break;

            case Menu.FoodItem.Milkshake:
                z.GetComponent<OrderCardPrefab>().AssignCard(3);
                break;

            case Menu.FoodItem.Spaghetti:
                z.GetComponent<OrderCardPrefab>().AssignCard(4);
                break;
        }
        CustomerList.Add(z);


    }    

    public void RemoveOrder()
    {
        CustomerOrder.RemoveAt(0);
        GameObject x = CustomerList[0];
        CustomerList.RemoveAt(0);
        Destroy(x);         
    }

   
}
