﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineHandler : MonoBehaviour
{

    public int NumOfCounters;
    public int NumOfDrinkMachines;

    public int NumOfShakeMachines;
    [Range(1, 3)] public int ShakeMachineLvl;

    public int NumOfRiceMachines;
    [Range(1, 3)] public int RiceMachineLvl;

    public int NumOfFriesMachines;
    [Range(1, 3)] public int FriesMachineLvl;

    public int NumOfBurgerMachines;
    [Range(1, 3)] public int BurgerMachineLvl;

    public int NumOfChickenMachines;
    [Range(1, 3)] public int ChickenMachineLvl;

    public int NumOfPastaMachines;
    [Range(1, 3)] public int PastaMachineLvl;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
