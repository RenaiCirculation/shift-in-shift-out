﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialResto : MonoBehaviour
{
    public WorkStation Counter;

    int curIteration;

    public GameObject Tutorial1_1, Tutorial1_2;
    public GameObject Tutorial2_1;
    public GameObject Tutorial3_1, Tutorial3_2;
    public GameObject Tutorial4_1, Tutorial4_2, Tutorial4_3;
    public GameObject Tutorial5_1, Tutorial5_2;

    // Start is called before the first frame update
    void Start()
    {
        curIteration = 1;

        Tutorial1_1.SetActive(false);
        Tutorial1_2.SetActive(false);

        Tutorial2_1.SetActive(false);

        Tutorial3_1.SetActive(false);
        Tutorial3_2.SetActive(false);

        Tutorial4_1.SetActive(false);
        Tutorial4_2.SetActive(false);
        Tutorial4_3.SetActive(false);

        Tutorial5_1.SetActive(false);
        Tutorial5_2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        CheckIteration();

        if(curIteration == 2)
        {
            if (Counter.ReturnAvailable() == false)
            {
                curIteration = 3;
            }
            
        }
    }
    void CheckIteration()
    {
        switch(curIteration)
        {
            case 1:
                Tutorial1_1.SetActive(true);
                Tutorial1_2.SetActive(true);
                break;

            case 2:
                Tutorial1_1.SetActive(false);
                Tutorial1_2.SetActive(false);

                Tutorial2_1.SetActive(true);
                break;

            case 3:
                Tutorial2_1.SetActive(false);

                Tutorial3_1.SetActive(true);
                Tutorial3_2.SetActive(true);
                break;

            case 4:               
                Tutorial3_1.SetActive(false);
                Tutorial3_2.SetActive(false);

                Tutorial4_1.SetActive(true);
                Tutorial4_2.SetActive(true);
                Tutorial4_3.SetActive(true);
                break;

            case 5:
                Tutorial4_1.SetActive(false);
                Tutorial4_2.SetActive(false);
                Tutorial4_3.SetActive(false);

                Tutorial5_1.SetActive(true);
                Tutorial5_2.SetActive(true);
                break;


        }
    }

    public void NextIteration()
    {
        curIteration++;

    }

    public void OpenPrepMenu()
    {
        if (curIteration == 3)
        {
            curIteration = 4;
        }
    }

    public void Serve()
    {
        if (curIteration == 4)
        {
            curIteration = 5;
        }
    }

}
