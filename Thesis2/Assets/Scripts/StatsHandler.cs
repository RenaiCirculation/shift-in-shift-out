﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsHandler : MonoBehaviour
{
    public static StatsHandler instance;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public float CurrentMoney;

    public int CurrentDay;

    public float ChickenEarnings;
    public float BurgerEarnings;
    public float ShakeEarnings;
    public float SpagEarnings;

    public List<GameObject> CurrentWorkers = new List<GameObject>();

    public void WorkersDisableMood()
    {
        GameObject[] W = GameObject.FindGameObjectsWithTag("Worker");
        for (int i = 0; i > W.Length; i++)
        {
            W[i].GetComponent<WorkerMood>().enabled = false;
        }
    }

    public void TransferAllWorkers()
    {
        GameObject[] W = GameObject.FindGameObjectsWithTag("Worker");
        for(int i = 0; i > W.Length; i++)
        {
            W[i].transform.SetParent(this.gameObject.transform);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TransferCurrentEarnings()
    {

    }

    public void PayChicken(float x)
    {
        ChickenEarnings += x;
    }

    public void PayBurger(float x)
    {
        BurgerEarnings += x;
    }

    public void PayShake(float x)
    {
        ShakeEarnings += x;
    }

    public void PaySpag(float x)
    {
        SpagEarnings += x;
    }
}