﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiringWorkerBar : MonoBehaviour
{
    public Sprite Port1, Port2;

    public Image Portrait;

    public Text WorkerName;

    public Image Stamina, Cooking, Interaction;

    public Text PPDay;

    public Sprite Skill1, Skill2, Skill3, Skill4, Skill5;

    public Text HireCost;

    public void AssignValues(int PortraitNum, string WName,
        int Sta, int Cook, int Inter, float PPD, float HRcost)
    {
        switch (PortraitNum)
        {
            case 1:
                Portrait.sprite = Port1;
                break;

            case 2:
                Portrait.sprite = Port2;
                break;
        }
        WorkerName.text = WName;
        HireCost.text = "P: " + HRcost.ToString("F2");
        switch (Sta)
        {
            case 1:
                Stamina.sprite = Skill1;
                break;

            case 2:
                Stamina.sprite = Skill2;
                break;

            case 3:
                Stamina.sprite = Skill3;
                break;

            case 4:
                Stamina.sprite = Skill4;
                break;

            case 5:
                Stamina.sprite = Skill5;
                break;
        }

        switch (Cook)
        {
            case 1:
                Cooking.sprite = Skill1;
                break;

            case 2:
                Cooking.sprite = Skill2;
                break;

            case 3:
                Cooking.sprite = Skill3;
                break;

            case 4:
                Cooking.sprite = Skill4;
                break;

            case 5:
                Cooking.sprite = Skill5;
                break;
        }

        switch (Inter)
        {
            case 1:
                Interaction.sprite = Skill1;
                break;

            case 2:
                Interaction.sprite = Skill2;
                break;

            case 3:
                Interaction.sprite = Skill3;
                break;

            case 4:
                Interaction.sprite = Skill4;
                break;

            case 5:
                Interaction.sprite = Skill5;
                break;
        }

        PPDay.text = "P " + PPD.ToString() + "/Day";
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
