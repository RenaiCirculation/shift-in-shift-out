﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiringMenu : MonoBehaviour
{
    public GameObject WorkerBarPrefab, HireNewPrefab;

    StatsHandler SH;
    WorkerHandler WH;
    
    [SerializeField] GameObject ContentArea;

    public List<GameObject> Workers = new List<GameObject>();
    public List<GameObject> WorkerDisplay = new List<GameObject>();

    GameObject HiringButton;
    bool HiringMenuOpen;
    public GameObject HireMenu;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();
        WH = GameObject.FindGameObjectWithTag("Stats").GetComponent<WorkerHandler>();
        Workers = SH.CurrentWorkers;

        HiringButton = null;
        SpawnWorkerBars();

        HireMenu.SetActive(false);
        HiringMenuOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
        HireMenu.SetActive(HiringMenuOpen);
    }

    void SpawnWorkerBars()
    {
        //foreach(GameObject G in Workers)
        //{
        //    GameObject Bar = Instantiate(WorkerBarPrefab, ContentArea.transform.position,
        //            Quaternion.identity, ContentArea.transform);

        //    Bar.GetComponent<WorkerBar>().AssignValues(G.GetComponent<WorkerStats>().PortraitUsed,
        //        G.GetComponent<WorkerStats>().Name, G.GetComponent<WorkerStats>().MaxStamina, G.GetComponent<WorkerStats>().CookingSkill,
        //        G.GetComponent<WorkerStats>().InteractSkill, G.GetComponent<WorkerStats>().SalaryPerDay);
        //    WorkerDisplay.Add(Bar);

        //}

        foreach(WorkerHandler.WorkerInfo G in WH.CurrentWorkers)
        {
            GameObject Bar = Instantiate(WorkerBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);

            Bar.GetComponent<WorkerBar>().AssignValues(G.PortraitUsed,
                G.Name, G.MaxStamina, G.CookingSkill,
                G.InteractSkill, G.SalaryPerDay);

            WorkerDisplay.Add(Bar);
        }
        
        SpawnHiringBar();
    }

    public void RecheckWorkers()
    {
        Destroy(HiringButton);
        foreach (GameObject g in WorkerDisplay)
        {
            WorkerDisplay.Remove(g);
            Destroy(g);
        }

        foreach (WorkerHandler.WorkerInfo G in WH.CurrentWorkers)
        {
            GameObject Bar = Instantiate(WorkerBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);

            Bar.GetComponent<WorkerBar>().AssignValues(G.PortraitUsed,
                G.Name, G.MaxStamina, G.CookingSkill,
                G.InteractSkill, G.SalaryPerDay);

            WorkerDisplay.Add(Bar);
        }
        SpawnHiringBar();
    }

    void SpawnHiringBar()
    {
        HiringButton = Instantiate(HireNewPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
    }

    public void StartHiringMenu()
    {
        HiringMenuOpen = true;
    }

    public void ExitHiringMenu()
    {
        HiringMenuOpen = false;
    }
}
