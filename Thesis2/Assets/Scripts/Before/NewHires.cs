﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHires : MonoBehaviour
{
    public HiringWorkerBar Bar1, Bar2;

    WorkerHandler WH;

    GameObject Worker1,Worker2;
    
    // Start is called before the first frame update
    void Start()
    {
        WH = GameObject.FindGameObjectWithTag("Stats").GetComponent<WorkerHandler>();
        Worker1 = null;
        Worker2 = null;
        GetRandomWorkers();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GetRandomWorkers()
    {
        GameObject x = WH.WorkersToHire[Random.Range(0, WH.WorkersToHire.Count)];
        Bar1.AssignValues(x.GetComponent<WorkerStats>().PortraitUsed,
            x.GetComponent<WorkerStats>().Name, x.GetComponent<WorkerStats>().MaxStamina,
            x.GetComponent<WorkerStats>().CookingSkill, x.GetComponent<WorkerStats>().InteractSkill,
            x.GetComponent<WorkerStats>().SalaryPerDay, x.GetComponent<WorkerStats>().HiringCost);

        GameObject y = WH.WorkersToHire[Random.Range(0, WH.WorkersToHire.Count)];
        Bar2.AssignValues(y.GetComponent<WorkerStats>().PortraitUsed,
            y.GetComponent<WorkerStats>().Name, y.GetComponent<WorkerStats>().MaxStamina,
            y.GetComponent<WorkerStats>().CookingSkill, y.GetComponent<WorkerStats>().InteractSkill,
            y.GetComponent<WorkerStats>().SalaryPerDay, y.GetComponent<WorkerStats>().HiringCost);

        Worker1 = x;
        Worker2 = y;
    }

    public void HireContract(int BarNum)
    {
        switch(BarNum)
        {
            case 1:
                Worker1.GetComponent<WorkerStats>().AssignContractType(1);
                WH.AddNewWorker(Worker1);
                break;

            case 2:
                Worker2.GetComponent<WorkerStats>().AssignContractType(1);
                WH.AddNewWorker(Worker2);
                break;
        }

        GameObject.Find("HiringTab").GetComponent<HiringMenu>().RecheckWorkers();
        
       
    }

    public void HireRegular(int BarNum)
    {
        switch (BarNum)
        {
            case 1:
                Worker1.GetComponent<WorkerStats>().AssignContractType(2);
                WH.AddNewWorker(Worker1);
                break;

            case 2:
                Worker2.GetComponent<WorkerStats>().AssignContractType(2);
                WH.AddNewWorker(Worker2);
                break;
        }
        GameObject.Find("HiringTab").GetComponent<HiringMenu>().RecheckWorkers();
    }
}
