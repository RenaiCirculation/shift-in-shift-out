﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnClickedObject : MonoBehaviour
{
    public bool IsClicked;

    void Start()
    {
        IsClicked = false;
        StartCoroutine(Reset());
    }

   IEnumerator Reset()
   {
        while (true)
        {
            IsClicked = false;
            yield return new WaitForSeconds(.1f);
        }
   }

    private void OnMouseDown()
    {
        IsClicked = true;
    }
}
