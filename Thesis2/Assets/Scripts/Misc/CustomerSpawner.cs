﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{

    public GameObject CustomerPrefab;

    public int CustomerAmtToSpawn;
    int spawned;
    bool startSpawning;

    public float TimeBetweenSpawn;
    float internalTimer;

    QueueHandler QH;

    // Start is called before the first frame update
    void Start()
    {
        startSpawning = false;
        internalTimer = 0;
        spawned = 0;

        QH = GameObject.Find("LineManager").GetComponent<QueueHandler>();
    }

    // Update is called once per frame
    void Update()
    {      
        
        //todo check if working after loading scene 

        if (startSpawning)
        {
            if (spawned < CustomerAmtToSpawn)
            {
                if (internalTimer >= TimeBetweenSpawn)
                {
                    SpawnCustomer();
                    internalTimer = 0;
                }
                else
                {
                    internalTimer += Time.deltaTime;
                }
            }
        }
    }

    void SpawnCustomer()
    {
        GameObject G = Instantiate(CustomerPrefab, 
            QH.RestoEntrancePos.transform.position, 
            Quaternion.identity);

        spawned++;
    }

    public void StartSpawnCustomer()
    {
        startSpawning = true;
    }


}
