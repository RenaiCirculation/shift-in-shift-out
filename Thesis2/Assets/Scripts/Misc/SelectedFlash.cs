﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedFlash : MonoBehaviour
{    
    public int red, green, blue;

    public bool mouseoverObject = false;
    public bool flashingIn = true;
    public bool startedFlashing = false;

    float oldRed, oldGreen, oldBlue;

    // Start is called before the first frame update
    void Start()
    {
        oldRed = GetComponent<Renderer>().material.color.r;
        oldGreen = GetComponent<Renderer>().material.color.g;
        oldBlue = GetComponent<Renderer>().material.color.b;

    }

    // Update is called once per frame
    void Update()
    {
        if(mouseoverObject == true)
        {
            GetComponent<Renderer>().material.color = new Color32((byte)red, (byte)green, (byte)blue, 255);
        }
        else
        {
            GetComponent<Renderer>().material.color = new Color(oldRed, oldGreen, oldBlue, 255);
        }
    }

    private void OnMouseOver()
    {
        //SelectedObj = GameObject.Find(HighlightObject.selectedObj);
        mouseoverObject = true;
        if(!startedFlashing)
        {
            startedFlashing = true;
            StartCoroutine(FlashObject());
        }

    }

    private void OnMouseExit()
    {
        //Debug.Log(oldRed + "/" + oldGreen + "/" + oldBlue);

        GetComponent<Renderer>().material.color = new Color32((byte)oldRed, (byte)oldGreen, (byte)oldBlue, 255);
        startedFlashing = false;
        mouseoverObject = false;
        StopCoroutine(FlashObject());

        
    }

    IEnumerator FlashObject()
    {
        while(mouseoverObject == true)
        {
            yield return new WaitForSeconds(0.05f);
            if(flashingIn == true)
            {
                if(blue <= 10)
                {
                    flashingIn = false;
                }
                else
                {
                    blue -= 10;
                    green -= 10;
                    red -= 10;
                }
            }

            if(flashingIn == false)
            {
                if (blue >= 250)
                {
                    flashingIn = true;
                }

                else
                {
                    blue += 10;
                    green += 10;
                    red += 10;
                }
            }
        }
    }
}
