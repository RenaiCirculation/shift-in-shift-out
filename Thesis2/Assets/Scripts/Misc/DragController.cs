﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class DragController : MonoBehaviour
{
    public bool IsDragged;

    public Shader NormalShader;
    public Shader HighlightedShader;

    Renderer rend;
    bool IsMouseOver;

    Collider col;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        IsDragged = false;
        IsMouseOver = false;
        rend.material.shader = NormalShader;

        col = GetComponent<Collider>();
        col.enabled = true;
    }

    private void Update()
    {
        if(IsMouseOver)
        {
            rend.material.shader = HighlightedShader;
        }
        else
        {
            rend.material.shader = NormalShader;
        }

        
        
    }

    public void DisableCollider()
    {
        col.enabled = false;
    }

    public void EnableCollider()
    {
        col.enabled = true;
    }

    void OnMouseDrag()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        IsDragged = true;
        //Debug.Log(IsDragged);
        
        Plane plane = new Plane(Vector3.up, new Vector3(0, 1.5f, 0));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (plane.Raycast(ray, out distance))
        {
            transform.position = ray.GetPoint(distance);
        }

        GetComponent<NavMeshAgent>().enabled = false;
    }

    private void OnMouseUp()
    {
        IsDragged = false;
        GetComponent<NavMeshAgent>().enabled = true;
    }

    private void OnMouseOver()
    {
        //Debug.Log("Over");
        if (EventSystem.current.IsPointerOverGameObject())
            return;



        IsMouseOver = true;
    }

    private void OnMouseExit()
    {
        IsMouseOver = false;
    }
}
