﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public List<GameObject> CurrentWorkers;
    public List<GameObject> CurrentStations;
    public List<GameObject> CurrentCustomers;

    public List<GameObject> Tables;

    GameObject curSelectedWorker;

    bool foundDragging;

    public QueueHandler QH;

    public float RecheckTimer;
    public GameObject CanvasUI;

    public GameObject StartMenu;
    public GameObject EndMenu;    

    float queueTimer;
    float waitTimer;

    public Text TimeRemainingText;
    public Image TimeRemainingIndicator;
    public float GameTimer;
    float currentTimer;
    bool startedGame;

    public float TodaysEarning;
    public Text TEDisplay;

    public Text AmtRequiredTxt;
    public int MinimumReqCustomers;//todo
    public int currentDoneCustomers;
    public int WalkOutCustomers;

    

    // Start is called before the first frame update
    void Start()
    {
        TodaysEarning = 0;
        QH = GameObject.Find("LineManager").GetComponent<QueueHandler>();

        CurrentWorkers = new List<GameObject>(GameObject.
            FindGameObjectsWithTag("Worker"));

        CurrentStations = new List<GameObject>(GameObject.FindGameObjectsWithTag("WorkStation"));
        GetComponent<StationManager>().AssignMachines();
        //CurrentCustomers = new List<GameObject>(GameObject.FindGameObjectsWithTag("Customer"));

        //Tables = new List<GameObject>(GameObject.
        //   FindGameObjectsWithTag("Table"));

        CanvasUI = GameObject.Find("CanvasUI");

        foundDragging = false;
        curSelectedWorker = null;
        queueTimer = 0;

        currentDoneCustomers = 0;
        WalkOutCustomers = 0;

        currentTimer = GameTimer;
        startedGame = false;

        StartMenu.SetActive(true);
        EndMenu.SetActive(false);

        Tables = new List<GameObject>(GameObject.FindGameObjectsWithTag("Table"));
        Debug.Log(Tables.Count);
    }
    // Update is called once per frame
    void Update()
    {
        AmtRequiredTxt.text = (currentDoneCustomers + "/" + 
            MinimumReqCustomers);

        TEDisplay.text = TodaysEarning.ToString();

        Highlighting();
        Queuing();
        ReheckCustomers();
        //WaitingQueue();

        if (startedGame)
        {
            Timer();

            if(Input.GetKeyDown(KeyCode.F))
            {
                currentTimer = 5;
            }
        }
        else
        {

        }
        var ts = TimeSpan.FromSeconds(currentTimer);
        TimeRemainingText.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);


        
    }

    void Highlighting()
    {
        if (!foundDragging)
        {
            CheckIfAnyDragging();
        }
        else
        {
            if (curSelectedWorker != null)
            {
                if (curSelectedWorker.GetComponent<DragController>().IsDragged)
                {
                    HighlightAvailableStations();
                }
                else
                {
                    ReturnHighlightAvailableStations();
                    curSelectedWorker = null;
                    foundDragging = false;
                }
            }
        }
    }

    void CheckIfAnyDragging()
    {
        foreach (GameObject g in CurrentWorkers)
        {
            //Debug.Log(g.name);
            if (g.GetComponent<DragController>().IsDragged)
            {
                curSelectedWorker = g;
                foundDragging = true;
                break;
            }
        }
    }

    void HighlightAvailableStations()
    {
        foreach (GameObject g in CurrentStations)
        {
            if (g.GetComponent<WorkStation>().ReturnAvailable() == true)
            {
                g.GetComponent<WorkStation>().ChangeShader(2);
            }
        }
    }

    void ReturnHighlightAvailableStations()
    {
        foreach (GameObject g in CurrentStations)
        {
            g.GetComponent<WorkStation>().ChangeShader(1);
        }
    }

    void ReheckCustomers()
    {
        CurrentCustomers = new List<GameObject>(GameObject.
           FindGameObjectsWithTag("Customer"));
    }

    void Queuing()
    {
        queueTimer += Time.deltaTime;
        if (queueTimer >= RecheckTimer)
        {
            if (QH.Customers.Count < 6)
            {

                foreach (GameObject c in CurrentCustomers)
                {
                    if (c.GetComponent<Customer>().InQueue == false)
                    {
                        if (QH.CanAddCustomer())
                        {
                            Debug.Log("Moved " + c.gameObject.name);
                            QH.AddCustomer(c.GetComponent<Customer>());
                            break;
                        }
                    }
                }
            }

            queueTimer = 0;
        }

    }

    void WaitingQueue()
    {
        if (QH.HasWaitingCustomers())
        {
            waitTimer += Time.deltaTime;
            if (waitTimer >= RecheckTimer)
            {
                Debug.Log("Waiting Queue");

            }
        }
        else
        {
            waitTimer = 0;
        }


    }

    void Timer()
    {
        if (currentTimer > 0)
        {
            currentTimer -= Time.deltaTime;
        }
        else
        {
            currentTimer = 0;
            startedGame = false;
            DayEnd();
        }
    }

    void DayEnd()
    {
        EndMenu.SetActive(true);
    }

    public void StartTimer()
    {
        startedGame = true;
        StartMenu.SetActive(false);
    }

    public void PayOrder(float x)
    {
        TodaysEarning += x;
        currentDoneCustomers += 1;
    }

    public void DepleteWorkerStamina(float x)
    {
        GameObject[] Workers = GameObject.FindGameObjectsWithTag("Worker");
        foreach(GameObject W in Workers)
        {
            W.GetComponent<WorkerMood>().DepleteStamina(x,1);
        }
    }

    public GameObject FindEmptyTable()
    {
        foreach(GameObject G in Tables)
        {
            if (G.GetComponent<Table>().HasEmptySeat())
            {
                return G;
            }
        }
        return null;
    }

    public GameObject FindDirtyArea()
    {
        Debug.Log("Finding tables");
        foreach (GameObject G in Tables)
        {
            if(G.GetComponent<Table>().HasDirt)
            {
                return G;
            }
        }
        
        return null;
    }

}
