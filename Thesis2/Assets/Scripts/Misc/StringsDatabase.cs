﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringsDatabase : MonoBehaviour
{
    public List<string> QueueComplaints = new List<string>();
    public List<string> WaitingComplaints = new List<string>();
    public List<string> ExitComplaints = new List<string>();

    public List<string> WorkerNegative = new List<string>();
    public List<string> WorkerPostive = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string ReturnQueueComplaint()
    {        
        int x = Random.Range(0, QueueComplaints.Count);   
        return QueueComplaints[x];
    }

    public string ReturnWaitingComplaint()
    {
        int x = Random.Range(0, WaitingComplaints.Count);
        return WaitingComplaints[x];
    }

    public string ReturnExitComplaint()
    {
        int x = Random.Range(0, WaitingComplaints.Count);
        return ExitComplaints[x];
    }

    public string ReturnWorkerNegative()
    {
        int x = Random.Range(0, WorkerNegative.Count);
        return WorkerNegative[x];
    }
    public string ReturnWorkerPostive()
    {
        int x = Random.Range(0, WorkerPostive.Count);
        return WorkerPostive[x];
    }
}
