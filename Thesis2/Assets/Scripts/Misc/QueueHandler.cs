﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueHandler : MonoBehaviour
{
    public List<GameObject> QueuePositions = new List<GameObject>();
    public List<Customer> Customers = new List<Customer>();

    public List<GameObject> WaitingPositions = new List<GameObject>();
    public List<Customer> WaitingCustomers = new List<Customer>();

    public GameObject EntrancePosition;
    public GameObject ExitPosition;
    public GameObject RestoEntrancePos;
    public GameObject RestoExitPos;

    bool IsMovingToWaitQueue;
    Customer CToMove;

    public GameObject IdleCenter;
    public GameObject Idle1, Idle2, Idle3, Idle4;

    // Start is called before the first frame update
    void Start()
    {
        IsMovingToWaitQueue = false;
        CToMove = null;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Customers.Count);

        if (IsMovingToWaitQueue)
        {           
            if(CToMove != null)
            {
                float dis = Vector3.Distance(CToMove.transform.position, 
                    ExitPosition.transform.position);
                if (dis <= .9)
                {
                    CToMove.MoveTo(WaitingPositions[WaitingCustomers.IndexOf(CToMove)].transform);
                    IsMovingToWaitQueue = false;
                    CToMove = null;
                }
            }
            
        }
        else
        {

        }
    }
   
    public bool CanAddCustomer()
    {
        return Customers.Count < QueuePositions.Count;
    }

    public bool CanAddCustomerWaiting()
    {
        return WaitingCustomers.Count < WaitingPositions.Count;
    }

    public bool HasWaitingCustomers()
    {
        if (WaitingCustomers.Count <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool HasCustomers()
    {
        if(Customers.Count <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }   
    }

    public void AddCustomer(Customer c)
    {
        Customers.Add(c);
        c.InQueue = true;
        c.CM.StartedQueuing();
        c.MoveTo(EntrancePosition.transform);
        c.MoveTo(QueuePositions[Customers.IndexOf(c)].transform);
    }   

    public Customer GetFirstInLine()
    {
        if(Customers.Count == 0)
        {
            return null;
        }
        else
        {
            Customer c = Customers[0];
            Customers.RemoveAt(0);
            RelocateCustomerQueue();
            return c;
        }   
    }

    public Customer GetFirstClaimQueue()
    {
        if(WaitingCustomers.Count == 0)
        {
            return null;
        }
        else
        {
            Customer c = WaitingCustomers[0];
            WaitingCustomers.RemoveAt(0);
            RelocateWaitingQueue();
            return c;
        }
    }

    void RelocateWaitingQueue()
    {
        for(int i = 0; i < WaitingCustomers.Count; i++)
        {
            WaitingCustomers[i].MoveTo(WaitingPositions[i].transform);
        }
    }

    void RelocateCustomerQueue()
    {
        for(int i =0; i < Customers.Count; i++)
        {
            Customers[i].MoveTo(QueuePositions[i].transform);
        }
    }

    public void DoneOrdering(Customer x)
    {
        Debug.Log("done" + x.name);
        x.MoveTo(ExitPosition.transform);
        x.gameObject.GetComponent<CustomerMood>().EndedQueue();
        AddCustomerWaitingQueue(x);
    }

    void AddCustomerWaitingQueue(Customer x)
    {
        Debug.Log("Added to waiting queue" + x.name);
        x.gameObject.GetComponent<CustomerMood>().StartedWaiting();
        WaitingCustomers.Add(x);
        CToMove = x;
        IsMovingToWaitQueue = true;
    }

    public void FindCustomerInQueue(Customer x)
    {
        foreach(Customer cus in Customers)
        {
            if(cus.gameObject.name == x.gameObject.name)
            {
                Debug.Log("Found");
                Customers.Remove(cus);
                break;
            }
        }
    }

    public void FindCustomerInWaitingQueue(Customer x)
    {
        foreach (Customer cus in WaitingCustomers)
        {
            if (cus.gameObject.name == x.gameObject.name)
            {
                Debug.Log("Found");
                WaitingCustomers.Remove(cus);
                break;
            }
        }
    }
}
