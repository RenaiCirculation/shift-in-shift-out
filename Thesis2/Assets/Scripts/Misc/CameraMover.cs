﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    // Start is called before the first frame update

    public bool LockCamera;
    

    void Start()
    {
        LockCamera = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!LockCamera)
        {
            float xAxisValue = Input.GetAxis("Horizontal");
            float zAxisValue = Input.GetAxis("Vertical");
            if (Camera.current != null)
            {
                
                if(Camera.current.orthographic)
                {
                    Camera.current.transform.Translate(new Vector3(xAxisValue, zAxisValue, 0.0f));

                    //Debug.Log(Camera.current.transform.position.x + " // " + Camera.current.transform.position.y);
                }

                else
                {
                    Camera.current.transform.Translate(new Vector3(xAxisValue, 0.0f, zAxisValue));
                    //Debug.Log(Camera.current.transform.position.x + " // " + Camera.current.transform.position.z);

                }

            }
            
        }        
    }

    public void LockCamToLoc(GameObject x)
    {
        LockCamera = true;
        transform.position = new Vector3(x.transform.position.x,
           transform.position.y, transform.position.z);
    }
}
