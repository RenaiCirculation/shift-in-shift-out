﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerStats : MonoBehaviour
{
    public Sprite Portrait1, Portrait2;

    public int PortraitUsed;
    public string Name;
    public float SalaryPerDay;
    public float HiringCost;

    [Range(1, 5)] public int MaxStamina;
    [Range(1, 5)] public int CookingSkill;
    [Range(1, 5)] public int InteractSkill;

    public int ContractType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignContractType(int x)
    {
        ContractType = x;
    }
}
