﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerMood : MonoBehaviour
{
    GameObject camera;
    StringsDatabase SD;

    bool enableCanvas;

    public GameObject UIDisplay;
    public GameObject TabToDisplay;

    public GameObject LargeTxtBox, MedTxtBox, SmallTxtBox;

    public Image MoodMeter;
    public float CurrentStamina;

    string toDisplay;

    bool hasDisplayText;
    public float DisplayTimer;
    float dTimer;

    public void ResetAll()
    {
        camera = GameObject.Find("Main Camera");
        enableCanvas = false;
        MoodMeter.fillAmount = 1;
        SD = GameObject.FindGameObjectWithTag("GameController").GetComponent<StringsDatabase>();
        hasDisplayText = false;

        dTimer = 0;
        toDisplay = null;
        CurrentStamina = 100;

        LargeTxtBox.SetActive(false);
        MedTxtBox.SetActive(false);
        SmallTxtBox.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        camera = GameObject.Find("Main Camera");
        enableCanvas = false;
        MoodMeter.fillAmount = 1;
        SD = GameObject.FindGameObjectWithTag("GameController").GetComponent<StringsDatabase>();
        hasDisplayText = false;

        dTimer = 0;
        toDisplay = null;
        CurrentStamina = 100;

        LargeTxtBox.SetActive(false);
        MedTxtBox.SetActive(false);
        SmallTxtBox.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UIDisplay.transform.rotation = Quaternion.LookRotation(transform.position - camera.transform.position);
        //UIDisplay.transform.LookAt(camera.transform);
        if (Input.GetKey(KeyCode.Tab))
        {
            TabToDisplay.SetActive(true);
        }
        else
        {
            TabToDisplay.SetActive(false);
        }
        
        //UIDisplay.SetActive(enableCanvas);
        DisplayMeter();

        if(hasDisplayText)
        {
            
            if(dTimer >= DisplayTimer)
            {
                SmallTxtBox.SetActive(false);
                MedTxtBox.SetActive(false);
                LargeTxtBox.SetActive(false);

                toDisplay = null;
                hasDisplayText = false;
                dTimer = 0;
            }
            else
            {
                dTimer += Time.deltaTime;
            }
        }
        
        if(Input.GetKeyDown(KeyCode.X))
        {
            DepleteStamina(5, 2);
        }
       
    }

    public float ReturnStaminaPercentage()
    {
        if(CurrentStamina > 0)
        {
            return CurrentStamina / 100;
        }
        else
        {
            return .1f;
        }
    }

    void DisplayMeter()
    {
        MoodMeter.fillAmount = CurrentStamina / 100;
    }

    public void DepleteStamina(float x, int y)
    {
        if(CurrentStamina > 0)
        {
            CurrentStamina -= x;
            //  Debug.Log(name + " -- Stamina " + CurrentStamina);
            switch (y)
            {
                case 1:
                    DisplayNegative();
                    break;

                case 2:
                    break;
            }
        }
        else
        {
            CurrentStamina = 0;
        }
        

       
    }
    void DisplayNegative()
    {
        int x = Random.Range(0, 100);
        Debug.Log(x);
        if(x < 51)
        {
            Debug.Log("displaying nega");
            toDisplay = SD.ReturnWorkerNegative();
            if (toDisplay.Length < 20)
            {
                SmallTxtBox.SetActive(true);
                SmallTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = toDisplay;
                
            }
            else if (toDisplay.Length >= 21 && toDisplay.Length < 50)
            {
                MedTxtBox.SetActive(true);
                MedTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = toDisplay;
               
            }
            else
            {
                LargeTxtBox.SetActive(true);
                LargeTxtBox.transform.GetChild(0).gameObject.GetComponent<Text>().text = toDisplay;
                
            }
            hasDisplayText = true;
        }
        else
        {

        }
    }

    

}
