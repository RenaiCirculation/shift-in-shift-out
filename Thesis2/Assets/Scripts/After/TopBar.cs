﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopBar : MonoBehaviour
{
    public StatsHandler SH;

    public Text CurrentMoney;
    public Text CurrentDay;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if(SH.CurrentMoney > 0)
        {
            CurrentMoney.color = Color.green;
        }
        else
        {
            CurrentMoney.color = Color.red;
        }

        CurrentMoney.text = SH.CurrentMoney.ToString();
        CurrentDay.text = "Day: " + SH.CurrentDay.ToString();
    }
}   
