﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodEarnings : MonoBehaviour
{
    public GameObject EarningBarPrefab;

    public Text TotalEarningsUI;
    public float TotalEarnings;
    StatsHandler SH;

    [SerializeField] GameObject ContentArea;

    GameObject ChickenEarningBar;
    GameObject BurgerEarningBar;
    GameObject ShakeEarningBar;
    GameObject SpagEarningBar;

    float CEarnings, BEarnings, ShEarnings, SpEarnings;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();


        ChickenEarningBar = null;
        BurgerEarningBar = null;
        ShakeEarningBar = null;
        SpagEarningBar = null;

        CEarnings = 0;
        BEarnings = 0;
        ShEarnings = 0;
        SpEarnings = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheckDisplayEarnings();
        UpdateTotalAmt();
    }

    void CheckDisplayEarnings()
    {
        if(ChickenEarningBar == null)
        {
            if (SH.ChickenEarnings > 0)
            {
                ChickenEarningBar = Instantiate(EarningBarPrefab, ContentArea.transform.position, 
                    Quaternion.identity, ContentArea.transform);
            }
            
        }
        else
        {
            ChickenEarningBar.transform.GetChild(0).
                GetComponent<Text>().text = "Chicken Meal";

            ChickenEarningBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + SH.ChickenEarnings.ToString("F2");

            CEarnings = SH.ChickenEarnings;

            Debug.Log("Has");
        }

        if (BurgerEarningBar == null)
        {
            if (SH.BurgerEarnings > 0)
            {
                BurgerEarningBar = Instantiate(EarningBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
            }

        }
        else
        {
            BurgerEarningBar.transform.GetChild(0).
                GetComponent<Text>().text = "Burger Meal";

            BurgerEarningBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + SH.BurgerEarnings.ToString("F2");

            BEarnings = SH.BurgerEarnings;

            Debug.Log("Has");
        }

        if (ShakeEarningBar == null)
        {
            if (SH.ShakeEarnings > 0)
            {
                ShakeEarningBar = Instantiate(EarningBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
            }

        }
        else
        {
            ShakeEarningBar.transform.GetChild(0).
                GetComponent<Text>().text = "Milkshake";

            ShakeEarningBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + SH.ShakeEarnings.ToString("F2");

            ShEarnings = SH.ShakeEarnings;

            Debug.Log("Has");
        }

        if (SpagEarningBar == null)
        {
            if (SH.SpagEarnings > 0)
            {
                SpagEarningBar = Instantiate(EarningBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
            }

        }
        else
        {
            SpagEarningBar.transform.GetChild(0).
                GetComponent<Text>().text = "Spaghetti Meal";

            SpagEarningBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + SH.SpagEarnings.ToString("F2");

            SpEarnings = SH.SpagEarnings;

            Debug.Log("Has");
        }
        

    }

    void UpdateTotalAmt()
    {
        
        TotalEarnings = CEarnings + BEarnings + ShEarnings + SpEarnings;
       

        TotalEarningsUI.text = "P: " + TotalEarnings.ToString("F2");
    }
}
