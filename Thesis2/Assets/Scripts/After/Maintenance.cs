﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Maintenance : MonoBehaviour
{

    public GameObject BarPrefab;

    public Text TotalMaintUI;
    public float TotalMaint;

    MachineHandler MH;

    [SerializeField] GameObject ContentArea;

    GameObject CounterBar;
    GameObject DrinksBar;
    GameObject ShakesBar;
    GameObject RiceBar;
    GameObject FriesBar;
    GameObject BurgerBar;
    GameObject ChickenBar;
    GameObject PastaBar;

    // Start is called before the first frame update
    void Start()
    {
        MH = GameObject.FindGameObjectWithTag("Stats").GetComponent<MachineHandler>();
        CheckAvailable();
    }

    // Update is called once per frame
    void Update()
    {
        TotalMaint = CalculateCounter() + CalculateDrinks() + CalculateMShakes()
            + CalculateRice() + CalculateFries() + CalculateBurger()
            + CalculateChicken() + CalculatePasta();

        TotalMaintUI.text = "P: -" + TotalMaint.ToString("F2");
        CheckEnabled();
    }

    void CheckAvailable()
    {
        if(MH.NumOfCounters > 0)
        {
            CounterBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if(MH.NumOfDrinkMachines > 0)
        {
            DrinksBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfShakeMachines > 0)
        {
            ShakesBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfRiceMachines > 0)
        {
            RiceBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfFriesMachines > 0)
        {
            FriesBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfBurgerMachines > 0)
        {
            BurgerBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfChickenMachines > 0)
        {
            ChickenBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }

        if (MH.NumOfPastaMachines > 0)
        {
            PastaBar = Instantiate(BarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);
        }
    }

    void CheckEnabled()
    {
        if(CounterBar != null)
        {
            CounterBar.transform.GetChild(0).
                GetComponent<Text>().text = "Counters";

            CounterBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateCounter().ToString("F2");
        }

        if(DrinksBar != null)
        {
            DrinksBar.transform.GetChild(0).
                GetComponent<Text>().text = "Drinks";

            DrinksBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateDrinks().ToString("F2");
        }

        if (ShakesBar != null)
        {
            ShakesBar.transform.GetChild(0).
                GetComponent<Text>().text = "Milkshakes";

            ShakesBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateMShakes().ToString("F2");
        }

        if (RiceBar != null)
        {
            RiceBar.transform.GetChild(0).
                GetComponent<Text>().text = "Rice";

            RiceBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateRice().ToString("F2");
        }

        if (FriesBar != null)
        {
            FriesBar.transform.GetChild(0).
                GetComponent<Text>().text = "Fries";

            FriesBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateFries().ToString("F2");
        }

        if (BurgerBar != null)
        {
            BurgerBar.transform.GetChild(0).
                GetComponent<Text>().text = "Burger";

            BurgerBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateBurger().ToString("F2");
        }

        if (ChickenBar != null)
        {
            ChickenBar.transform.GetChild(0).
                GetComponent<Text>().text = "Chicken";

            ChickenBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculateChicken().ToString("F2");
        }

        if (PastaBar != null)
        {
            PastaBar.transform.GetChild(0).
                GetComponent<Text>().text = "Pasta";

            PastaBar.transform.GetChild(1).
                GetComponent<Text>().text = "P: " + CalculatePasta().ToString("F2");
        }
    }

    float CalculateCounter()
    {
        float x = 10 * MH.NumOfCounters;
        return x;
    }

    float CalculateDrinks()
    {
        float x = 5 * MH.NumOfDrinkMachines;
        return x;
    }

    float CalculateMShakes()
    {
        float x = 10 * MH.NumOfShakeMachines * MH.ShakeMachineLvl;
        return x;
    }

    float CalculateRice()
    {
        float x = 4 * MH.NumOfRiceMachines * MH.RiceMachineLvl;
        return x;
    }

    float CalculateFries()
    {
        float x = 3 * MH.NumOfFriesMachines * MH.FriesMachineLvl;
        return x;
    }

    float CalculateBurger()
    {
        float x = 3 * MH.NumOfBurgerMachines * MH.BurgerMachineLvl;
        return x;
    }

    float CalculateChicken()
    {
        float x = 5 * MH.NumOfChickenMachines * MH.ChickenMachineLvl;
        return x;
    }

    float CalculatePasta()
    {
        float x = 3 * MH.NumOfPastaMachines * MH.PastaMachineLvl;
        return x;
    }
}

