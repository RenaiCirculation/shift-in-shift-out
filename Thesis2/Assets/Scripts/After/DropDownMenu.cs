﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropDownMenu : MonoBehaviour
{

    public bool OpenDropDown;
    public GameObject ViewPort;
    // Start is called before the first frame update
    void Start()
    {
        OpenDropDown = false;
    }

    // Update is called once per frame
    void Update()
    {
        ViewPort.SetActive(OpenDropDown);
    }

    public void Clicked()
    {
        OpenDropDown = !OpenDropDown;
    }
}
