﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerWages : MonoBehaviour
{
    public GameObject EarningBarPrefab;

    public Text TotalWagesUI;
    public float TotalWages;

    StatsHandler SH;    

    [SerializeField] GameObject ContentArea;

    public List<GameObject> Workers = new List<GameObject>();
    public List<GameObject> WorkerDisplay = new List<GameObject>();

    public List<float> WorkersSalary = new List<float>();

    bool startedDisplay;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();
        startedDisplay = false;
        Workers = SH.CurrentWorkers;


        DisplayWorkers();
        CalculateTotalWage();
    }
    
    // Update is called once per frame
    void Update()
    {


        TotalWagesUI.text = "P: -" + TotalWages.ToString("F2");
    }

    void CalculateTotalWage()
    {
        foreach(float F in WorkersSalary)
        {
            TotalWages += F;
        }
    }
    

    void DisplayWorkers()
    {
        foreach(GameObject G in Workers)
        {
            GameObject Bar = Instantiate(EarningBarPrefab, ContentArea.transform.position,
                    Quaternion.identity, ContentArea.transform);

            Bar.transform.GetChild(0).GetComponent<Text>().text = G.GetComponent<WorkerStats>().Name;
            Bar.transform.GetChild(1).GetComponent<Text>().text = "P: " + G.GetComponent<WorkerStats>().SalaryPerDay.ToString("F2");

            WorkerDisplay.Add(Bar);
            WorkersSalary.Add(G.GetComponent<WorkerStats>().SalaryPerDay);
        }
    }
}
