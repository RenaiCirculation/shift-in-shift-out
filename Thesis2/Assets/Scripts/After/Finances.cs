﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Finances : MonoBehaviour
{
    public Text TotalAmtUI;
    float TotalAmt;

    public FoodEarnings FE;
    public WorkerWages WW;
    public Maintenance Maint;

    bool Added;

    StatsHandler SH;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("Stats").GetComponent<StatsHandler>();
        Added = false;
    }    

    // Update is called once per frame
    void Update()
    {
        ComputeTotal();
    }
    void ComputeTotal()
    {
        TotalAmt = FE.TotalEarnings - (WW.TotalWages + Maint.TotalMaint);

        TotalAmtUI.text = "P " + TotalAmt.ToString("F2");
        if(!Added)
        {
            SH.CurrentMoney += TotalAmt;
            Added = true;
        }
    }
}
