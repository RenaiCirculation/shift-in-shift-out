﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartTestScene()
    {
        SceneManager.LoadScene("TestHire");
    }

    public void StartScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
    