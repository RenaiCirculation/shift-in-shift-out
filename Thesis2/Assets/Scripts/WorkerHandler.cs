﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerHandler : MonoBehaviour
{
    public class WorkerInfo
    {
        public int PortraitUsed;
        public string Name;
        public float SalaryPerDay;
        public float HiringCost;

        [Range(1, 5)] public int MaxStamina;
        [Range(1, 5)] public int CookingSkill;
        [Range(1, 5)] public int InteractSkill;

        public int ContractType;
        public WorkerInfo(int PortraitNum, string WName,
        int Sta, int Cook, int Inter, float PPD, float HCost, int CT)
        {
            PortraitUsed = PortraitNum;
            Name = WName;
            SalaryPerDay = PPD;
            HiringCost = HCost;

            MaxStamina = Sta;
            CookingSkill = Cook;
            InteractSkill = Inter;
            ContractType = CT;
        }

    }

    public List<WorkerInfo> CurrentWorkers = new List<WorkerInfo>();
    public List<GameObject> WorkersToHire = new List<GameObject>();


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        Debug.Log(CurrentWorkers.Count);
                
       
    }

    public void AddNewWorker(GameObject x)
    {
        WorkerInfo WI = new WorkerInfo(x.GetComponent<WorkerStats>().PortraitUsed, x.GetComponent<WorkerStats>().Name,
            x.GetComponent<WorkerStats>().MaxStamina, x.GetComponent<WorkerStats>().CookingSkill,
            x.GetComponent<WorkerStats>().InteractSkill, x.GetComponent<WorkerStats>().SalaryPerDay,
            x.GetComponent<WorkerStats>().HiringCost, x.GetComponent<WorkerStats>().ContractType);

        CurrentWorkers.Add(WI);
    }
}
