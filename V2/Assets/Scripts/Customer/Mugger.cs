﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mugger : MonoBehaviour
{
    NavMeshAgent nav;
    GameObject RobPoint;
    OrderHandler OH;
    MessageController MC;

    GameObject ExitPos;

    bool arrived;
    // Start is called before the first frame update
    void Start()
    {
        MC = GameObject.FindGameObjectWithTag("MController").GetComponent<MessageController>();
        ExitPos = GameObject.FindGameObjectWithTag("ExitPos");
        arrived = false;
        OH = GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>();
        nav = GetComponent<NavMeshAgent>();
        RobPoint = GameObject.FindGameObjectWithTag("RobPoint");

        nav.SetDestination(RobPoint.transform.position  );
    }

    // Update is called once per frame
    void Update()
    {
        float dis = Vector3.Distance(gameObject.transform.position, RobPoint.transform.position);
        float dis2 = Vector3.Distance(gameObject.transform.position, ExitPos.transform.position);

        //Debug.Log(dis);
        if (!arrived)            
        {
            if (dis < .5)
            {
                arrived = true;
                OH.RemoveAllCurCustomers();
                MC.ActivateMuggerMessage();
            }   
        }

        else
        {
            if(dis2 < 1)
            {
                Destroy(gameObject);
            }
        }
    }

    public void Escape()
    {
        nav.SetDestination(ExitPos.transform.position);
    }
}
