﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerMood : MonoBehaviour
{
    bool timerStarted;

    public float MaxTimer;
    [SerializeField]
    float internalTimer;

    float messageResetTimer;

    GameManager GM;
    GlobalStats GS;
    MessageController MC;
    public enum CustomerAttitude
    {
        Positive,
        Neutral,
        Negative
    }

    public CustomerAttitude curAttitude;

    public Sprite PositiveSprite, NeutralSprite, NegativeSprite;
    public Image AttitudeDisplay;

    public GameObject NegativeHead, NeutralHead, PositiveHead;

    public bool CanDisplayEndMessage;
    public bool SureDisplayEndMessage;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        GM = GameObject.FindGameObjectWithTag("GameController").
            GetComponent<GameManager>();

        MC = GameObject.FindGameObjectWithTag("MController").GetComponent<MessageController>();
        //MaxTimer = GM.WaitingTime;
        switch (curAttitude)
        {
            case CustomerAttitude.Negative:
                MaxTimer = GM.NegativeWaitTime;
                break;

            case CustomerAttitude.Neutral:
                MaxTimer = GM.NeutralWaitTime;
                break;

            case CustomerAttitude.Positive:
                MaxTimer = GM.PositiveWaitTime;
                break;
        }


        internalTimer = MaxTimer;

        NegativeHead.SetActive(false);
        NeutralHead.SetActive(false);
        PositiveHead.SetActive(false);

        AssignSprite();

        messageResetTimer = 0;
        AttitudeDisplay.gameObject.SetActive(false);

        timerStarted = false;
        CanDisplayEndMessage = true;
        SureDisplayEndMessage = false;
    }

    void AssignSprite()
    {
        switch(curAttitude)
        {
            case CustomerAttitude.Negative:
                AttitudeDisplay.sprite = NegativeSprite;
                NegativeHead.SetActive(true);
                break;

            case CustomerAttitude.Neutral:
                AttitudeDisplay.sprite = NeutralSprite;
                NeutralHead.SetActive(true);
                break;

            case CustomerAttitude.Positive:
                AttitudeDisplay.sprite = PositiveSprite;
                PositiveHead.SetActive(true);
                break;
        }
    }

    public void DisplayAttitude()
    {
        AttitudeDisplay.gameObject.SetActive(true);
    }

    public void StartCTimer()
    {
        timerStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        //if(GetComponent<Customer>().CurCusState ==
        //    Customer.CustomerState.InQueue)
        //{
        //    if(!timerStarted)
        //    {
        //        Debug.Log("Started Timer");
        //        timerStarted = true;
        //    }
            
        //}
       
        if(timerStarted)
        {
            if (internalTimer > 0)
            {
                internalTimer -= Time.deltaTime;
            }
            else
            {
                
                timerStarted = false;
                Debug.Log(timerStarted);
                internalTimer = 0;
                DisplayEndMessage();
            }
        }

        if(internalTimer <= MaxTimer /2 && internalTimer >= 5)
        {
            if(messageResetTimer < 7)
            {
                messageResetTimer += Time.deltaTime;
            }
            else
            {
                MC.DisplayHalfwayWaitMessage(GetComponent<Customer>().ThisSlot);
                messageResetTimer = 0;
            }
        }
    }
    public void DisplayEndMessage() 
    {
        //Timer Ran Out
         GS.TotalAmtCustoLost++;
        Debug.Log("Display End");
        switch (curAttitude)
        {
            case CustomerAttitude.Positive:
                MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 1, true, GetComponent<Customer>().CustomerName);
                break;

            case CustomerAttitude.Neutral:
                MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 2, true, GetComponent<Customer>().CustomerName);
                break;

            case CustomerAttitude.Negative:
                MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 3, true, GetComponent<Customer>().CustomerName);
                break;

        }
    }

    public void DisplayOrderCorrectMenu()
    {
        GetComponent<Customer>().CurCusState = Customer.CustomerState.Exit;

        int x = Random.Range(0, 100);
        //Debug.Log("Display Order correct == " + x);
        if (!SureDisplayEndMessage)
        {
            if (CanDisplayEndMessage)
            {
                switch (curAttitude)
                {
                    case CustomerAttitude.Positive:
                        if (x > 50)
                        {
                            MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 1, false, GetComponent<Customer>().CustomerName);
                        }
                        break;
                    case CustomerAttitude.Neutral:
                        if (x > 75)
                        {
                            MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 2, false, GetComponent<Customer>().CustomerName);
                        }
                        break;

                    case CustomerAttitude.Negative:
                        if (x > 75)
                        {
                            MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 3, false, GetComponent<Customer>().CustomerName);
                        }
                        break;
                }
            }
        }
        else
        {
            switch (curAttitude)
            {
                case CustomerAttitude.Positive:
                    
                        MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 1, false, GetComponent<Customer>().CustomerName);
                    
                    break;
                case CustomerAttitude.Neutral:
                    
                        MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 2, false, GetComponent<Customer>().CustomerName);
                    
                    break;

                case CustomerAttitude.Negative:
                    
                        MC.DisplayEndMessage(GetComponent<Customer>().ThisSlot, 3, false, GetComponent<Customer>().CustomerName);
                    
                    break;
            }
        }
    }

    public void SubtractTimer()
    {
        internalTimer -= 10;
    }
}
