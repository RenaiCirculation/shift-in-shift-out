﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Customer : MonoBehaviour
{
    [Range(1,4)]public int ThisSlot;

    public string CustomerName;
    public NavMeshAgent agent;

    TableHandler TH;
    GameManager GM;

    public enum CustomerState
    {
        None,
        InQueue,
        Waiting,
        Exit
    }
    public CustomerState CurCusState;

    [Range(1, 3)] public int Order;

    GameObject Exitpos;
    bool isExiting;

    Vector3 NextIdlePos;
    bool IsMoving;

    AudioSource AS;
    public void AssignSlotNum(int x)
    {
        ThisSlot = x;
    }

    public List<GameObject> Variants = new List<GameObject>();


    // Start is called before the first frame update
    void Start()
    {
        ChooseVariant();

        CurCusState = CustomerState.None;
        agent = GetComponent<NavMeshAgent>();
        AS = GetComponent<AudioSource>();

        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        TH = GameObject.FindGameObjectWithTag("TableHandler").GetComponent<TableHandler>();

        IsMoving = false;

        isExiting = false;
        Exitpos = GameObject.FindGameObjectWithTag("ExitPos");
        AssignOrder();
        AS.Play();
    }
    void ChooseVariant()
    {
        int x = Random.Range(0, Variants.Count);
        foreach(GameObject y in Variants)
        {
            y.SetActive(false);
        }
        Variants[x].SetActive(true);
    }
    void AssignOrder()
    {
        int x = Random.Range(0, 100);

        if(x <= 33)
        {
            Order = 1;
        }
        else if (x > 33 && x <= 66)
        {
            Order = 2;
        }

        else
        {
            Order = 3;
        }      
    }
    // Update is called once per frame
    void Update()
    {
        if(isExiting)
        {
            float s = Vector3.Distance(transform.position, Exitpos.transform.position);
            if (s < 1)
            {
                Destroy(gameObject);
            }
        }

        if(CurCusState == CustomerState.None)
        {
            TH.AddNewCustomer(this);
            Idle();
        }

        //Debug.Log(AS.isPlaying);
        
    }
    void Idle()
    {
        float dis = 0;
        if (NextIdlePos != null)
        {
            dis = Vector3.Distance(transform.position, NextIdlePos);
        }
        
        if(!IsMoving)
        {
            NextIdlePos = GM.GetRandomIdlePos();
            IsMoving = true;
        }
        else
        {
            agent.SetDestination(NextIdlePos);
            if(dis < .5)
            {
                IsMoving = false;
            }
        }
    }
    
    public void MoveTo(Transform x)
    {
        agent.SetDestination(x.position);
    }

    public void MoveToExit(Transform x)
    {
        isExiting = true;
        agent.SetDestination(x.position);
    }

    public int GetOrder()
    {
        return Order;
    }

    public void OrderCorrect()
    {
        GetComponent<CustomerMood>().DisplayOrderCorrectMenu();
        switch (Order)
        {
            case 1:
                GM.AddProfits(30);
                break;

            case 2:
                GM.AddProfits(45);
                break;

            case 3:
                GM.AddProfits(50);
                break;
        }
        
    }
}
