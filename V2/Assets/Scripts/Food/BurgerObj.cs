﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurgerObj : MonoBehaviour
{
    [Range(1,4)]public int BurgerNum;

    public bool hasBurger;
    public bool isSelected;  

    public GameObject Bun, Patty, Egg, BunTop, KetchMayo,Cheese;

    public bool HasBun, HasEgg, HasCheese, HasKM;

    float cookTimer;
    float serveTimer;

    OrderHandler OH;

    AudioSource AS;
    public AudioClip CookingSFX, PrepSFX, BurntSFX;
    bool IsPlayingSFX;

    public GameObject CookingParticles, ServingParticles, BurntParticles;

    public AudioClip QBreadSFX, WCheeseSFX, EEggSFX, RSauce;

    public AudioClip CancelSFX;

    public AudioClip WrongOrderSFX;

    public bool CanInput;

    public enum BurgerState
    {
        None,
        Cooking,
        Serving,
        Burnt
    }
    public BurgerState curBurState;

    // Start is called before the first frame update
    void Start()
    {
        OH = GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>();
        AS = GetComponent<AudioSource>();
        hasBurger = false;
        isSelected = false;

        serveTimer = 20;
        cookTimer = 0;
        Bun.SetActive(false);
        Patty.SetActive(false);
        Egg.SetActive(false);
        BunTop.SetActive(false);
        KetchMayo.SetActive(false);
        Cheese.SetActive(false);

        HasBun = false;
        HasEgg = false;
        HasCheese = false;
        HasKM = false;

        curBurState = BurgerState.None;
        IsPlayingSFX = false;

        CanInput = true;
    }

    public void ResetObj()
    {

        hasBurger = false;
        isSelected = false;
        serveTimer = 20;
        cookTimer = 0;
        Bun.SetActive(false);
        Patty.SetActive(false);
        Egg.SetActive(false);
        BunTop.SetActive(false);
        KetchMayo.SetActive(false);
        Cheese.SetActive(false);

        HasBun = false;
        HasEgg = false;
        HasCheese = false;
        HasKM = false;

        curBurState = BurgerState.None;
    }

    // Update is called once per frame
    void Update()
    {
        CheckState();
        //Debug.Log(curBurState);
        if (isSelected && hasBurger)
        {
            if(curBurState == BurgerState.Serving)
                CheckInputs();
        }

        if(!hasBurger && isSelected)
        {
            if(curBurState == BurgerState.None)
            {
                if (Input.GetKeyDown(KeyCode.F))
                {
                    hasBurger = true;
                    curBurState = BurgerState.Cooking;
                    
                }
            }            
        }

        CheckSFX();

        Patty.SetActive(hasBurger);
        Bun.SetActive(HasBun);
        Egg.SetActive(HasEgg);
        Cheese.SetActive(HasCheese);
        KetchMayo.SetActive(HasKM);           
       
    }

    void CheckSFX()
    {
        switch (curBurState)
        {
            case BurgerState.None:
                CookingParticles.SetActive(false);
                ServingParticles.SetActive(false);
                BurntParticles.SetActive(false);

                if (IsPlayingSFX)
                {
                    if(AS.isPlaying)
                    {
                        AS.Stop();
                        
                    }
                    IsPlayingSFX = false;
                }
                break;

            case BurgerState.Cooking:
                CookingParticles.SetActive(true);
                ServingParticles.SetActive(false);
                BurntParticles.SetActive(false);


                if (!IsPlayingSFX)
                {
                    AS.clip = CookingSFX;
                    AS.Play();
                    IsPlayingSFX = true;                   
                }
                break;

            case BurgerState.Burnt:
                CookingParticles.SetActive(false);
                ServingParticles.SetActive(false);
                BurntParticles.SetActive(true);

                if (!IsPlayingSFX)
                {
                    AS.clip = BurntSFX;
                    AS.Play();
                    IsPlayingSFX = true;
                }
                break;

            case BurgerState.Serving:
                CookingParticles.SetActive(false);
                ServingParticles.SetActive(true);
                BurntParticles.SetActive(false);

                if (!IsPlayingSFX)
                {
                    AS.clip = PrepSFX;
                    AS.Play();
                    IsPlayingSFX = true;
                }
                break;
        }
    }

    void CheckState()
    {
        switch (curBurState)
        {
            case BurgerState.None:
                //do nothing
               
                break;

            case BurgerState.Cooking:
                Cooking();
               
                break;

            case BurgerState.Serving:
                Serving();
                
                break;

            case BurgerState.Burnt:
                Burnt();                
                break;
        }
    }

    void Cooking()
    {
      
        //Debug.Log(cookTimer);
        if (cookTimer >= OH.TimeToCook)
        {
            
            curBurState = BurgerState.Serving;
            IsPlayingSFX = false;

        }
        else
        {
            cookTimer += Time.deltaTime;
        }
    }

    void Serving()
    {
        //Debug.Log(serveTimer);
        if (serveTimer >= 0)
        {
            serveTimer -= Time.deltaTime;
        }
        else
        {
            IsPlayingSFX = false;
            curBurState = BurgerState.Burnt;
            
        }

        if(Input.GetKeyDown(KeyCode.F))
        {
            if(isSelected)
                SendOrder();
        }

    }

    void SendOrder()
    {
       
        //1 regular 
        //2 cheese borgar
        //3 egg borgar

        if(HasBun && HasKM &&!HasCheese && !HasEgg)
        {
            OH.ReceiveOrder(BurgerNum, 1);
        }

        else if(HasBun && HasKM && HasCheese && !HasEgg)
        {
            OH.ReceiveOrder(BurgerNum, 2);
        }

        else if(HasBun && HasKM && !HasCheese && HasEgg)
        {
            OH.ReceiveOrder(BurgerNum, 3);
        }

        else
        {
            AS.PlayOneShot(WrongOrderSFX);
            Debug.Log("Wrong Order");
        }
    }

    void Burnt()
    {
        

        if (Input.GetKeyDown(KeyCode.F))
        {
            serveTimer = 20;
            cookTimer = 0;

            Bun.SetActive(false);
            Patty.SetActive(false);
            Egg.SetActive(false);
            BunTop.SetActive(false);
            KetchMayo.SetActive(false);
            Cheese.SetActive(false);

            HasBun = false;
            HasEgg = false;
            HasCheese = false;
            HasKM = false;
            hasBurger = false;
            IsPlayingSFX = false;
            curBurState = BurgerState.None;
           
        }
    }

    void CheckInputs()
    {
        if (CanInput)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                //if (!HasBun)
                if (HasBun == false)
                {
                    AS.PlayOneShot(QBreadSFX);
                }
                else
                {
                    AS.PlayOneShot(CancelSFX);
                }
                HasBun = !HasBun;

            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                //if (!HasCheese)
                if (HasCheese == false)
                {
                    AS.PlayOneShot(WCheeseSFX);
                }
                else
                {
                    AS.PlayOneShot(CancelSFX);
                }
                HasCheese = !HasCheese;

            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                //if (!HasEgg)
                if (HasEgg == false)
                {
                    AS.PlayOneShot(EEggSFX);
                }
                else
                {
                    AS.PlayOneShot(CancelSFX);
                }
                HasEgg = !HasEgg;
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (HasKM == false)
                {
                    AS.PlayOneShot(RSauce);
                }
                else
                {
                    AS.PlayOneShot(CancelSFX);
                }
                HasKM = !HasKM;

            }
        }
    }

    public void ButtonQ()
    {
        if (CanInput)
        {
            if (isSelected && hasBurger)
            {
                if (curBurState == BurgerState.Serving)
                {
                    if (HasBun == false)
                    {
                        AS.PlayOneShot(QBreadSFX);
                    }
                    else
                    {
                        AS.PlayOneShot(CancelSFX);
                    }
                    HasBun = !HasBun;
                }
            }
        }
    }

    public void ButtonW()
    {
        if (CanInput)
        {
            if (isSelected && hasBurger)
            {
                if (curBurState == BurgerState.Serving)
                {
                    if (HasCheese == false)
                    {
                        AS.PlayOneShot(WCheeseSFX);
                    }
                    else
                    {
                        AS.PlayOneShot(CancelSFX);
                    }
                    HasCheese = !HasCheese;
                }
            }
        }
    }

    public void ButtonE()
    {
        if (CanInput)
        {
            if (isSelected && hasBurger)
            {
                if (curBurState == BurgerState.Serving)
                {
                    if (HasEgg == false)
                    {
                        AS.PlayOneShot(EEggSFX);
                    }
                    else
                    {
                        AS.PlayOneShot(CancelSFX);
                    }
                    HasEgg = !HasEgg;
                }
            }
        }
    }

    public void ButtonR()
    {
        if (CanInput)
        {
            if (isSelected && hasBurger)
            {
                if (curBurState == BurgerState.Serving)
                {
                    if (HasKM == false)
                    {
                        AS.PlayOneShot(RSauce);
                    }
                    else
                    {
                        AS.PlayOneShot(CancelSFX);
                    }
                    HasKM = !HasKM;
                }
            }
        }
    }
}
