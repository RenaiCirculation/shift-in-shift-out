﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grill : MonoBehaviour
{
    OrderHandler OH;

    public BurgerObj BurgerObj1, BurgerObj2, BurgerObj3, BurgerObj4;

    public GameObject CookButton;
    public GameObject cookingText, serveText, burntText;

    public GameObject Q_Bun, W_Cheese, E_Egg, R_KMayo;
    public GameObject HasBun, HasCheese, HasEgg, HasKM;
    

    // Start is called before the first frame update
    void Start()
    {
        OH = GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>();

        //cookingText = CookButton.transform.GetChild(1).gameObject;
        //serveText = CookButton.transform.GetChild(2).gameObject;
        //burntText = CookButton.transform.GetChild(3).gameObject;

        Q_Bun.SetActive(false);
        W_Cheese.SetActive(false);
        E_Egg.SetActive(false);
        R_KMayo.SetActive(false);

        HasBun.SetActive(false);
        HasCheese.SetActive(false);
        HasEgg.SetActive(false);
        HasKM.SetActive(false);

        cookingText.SetActive(false);
        serveText.SetActive(false);
        burntText.SetActive(false);

        CookButton.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {        
        CheckBurger();
    }

    void CheckBurgerIngredients(BurgerObj x)
    {
        HasBun.SetActive(x.HasBun);
        HasCheese.SetActive(x.HasCheese);
        HasEgg.SetActive(x.HasEgg);
        HasKM.SetActive(x.HasKM);
    }

    void CheckBurger()
    {
        switch(OH.CurOrderSelected)
        {
            case OrderHandler.SelectedOrder.Order1:
                BurgerObj1.isSelected = true;
                BurgerObj2.isSelected = false;
                BurgerObj3.isSelected = false;
                BurgerObj4.isSelected = false;

                if (!BurgerObj1.hasBurger)
                {
                    CookButton.SetActive(true);
                    serveText.SetActive(false);
                    cookingText.SetActive(true);
                    burntText.SetActive(false);
                    
                    
                }
                else
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(false);
                    serveText.SetActive(true);

                    if(BurgerObj1.curBurState == BurgerObj.BurgerState.Serving)
                    {
                        Q_Bun.SetActive(true);
                        W_Cheese.SetActive(true);
                        E_Egg.SetActive(true);
                        R_KMayo.SetActive(true);

                        CheckBurgerIngredients(BurgerObj1);


                    }
                    else
                    {
                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);

                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }

                    if(BurgerObj1.curBurState == BurgerObj.BurgerState.Burnt)
                    {
                        burntText.SetActive(true);
                        cookingText.SetActive(false);
                        serveText.SetActive(false);

                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);

                    }                    
                }
                break;

            case OrderHandler.SelectedOrder.Order2:
                BurgerObj1.isSelected = false;
                BurgerObj2.isSelected = true;
                BurgerObj3.isSelected = false;
                BurgerObj4.isSelected = false;

                if (!BurgerObj2.hasBurger)
                {
                    CookButton.SetActive(true);
                    serveText.SetActive(false);
                    cookingText.SetActive(true);
                    

                }
                else
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(false);
                    serveText.SetActive(true);

                    if (BurgerObj2.curBurState == BurgerObj.BurgerState.Serving)
                    {
                        Q_Bun.SetActive(true);
                        W_Cheese.SetActive(true);
                        E_Egg.SetActive(true);
                        R_KMayo.SetActive(true);

                        CheckBurgerIngredients(BurgerObj2);
                    }
                    else
                    {
                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }

                    if (BurgerObj2.curBurState == BurgerObj.BurgerState.Burnt)
                    {
                        burntText.SetActive(true);
                        cookingText.SetActive(false);
                        serveText.SetActive(false);

                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);

                    }
                }
                break;

            case OrderHandler.SelectedOrder.Order3:
                BurgerObj1.isSelected = false;
                BurgerObj2.isSelected = false;
                BurgerObj3.isSelected = true;
                BurgerObj4.isSelected = false;

                if (!BurgerObj3.hasBurger)
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(true);
                    serveText.SetActive(false);

                    
                }
                else
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(false);
                    serveText.SetActive(true);

                    if (BurgerObj3.curBurState == BurgerObj.BurgerState.Serving)
                    {
                        Q_Bun.SetActive(true);
                        W_Cheese.SetActive(true);
                        E_Egg.SetActive(true);
                        R_KMayo.SetActive(true);

                        CheckBurgerIngredients(BurgerObj3);
                    }
                    else
                    {
                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }

                    if (BurgerObj3.curBurState == BurgerObj.BurgerState.Burnt)
                    {
                        burntText.SetActive(true);
                        cookingText.SetActive(false);
                        serveText.SetActive(false);

                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }
                }
                break;

            case OrderHandler.SelectedOrder.Order4:
                BurgerObj1.isSelected = false;
                BurgerObj2.isSelected = false;
                BurgerObj3.isSelected = false;
                BurgerObj4.isSelected = true;

                if (!BurgerObj4.hasBurger)
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(true);
                    serveText.SetActive(false);
                }
                else
                {
                    CookButton.SetActive(true);
                    cookingText.SetActive(false);
                    serveText.SetActive(true);

                    if (BurgerObj4.curBurState == BurgerObj.BurgerState.Serving)
                    {
                        Q_Bun.SetActive(true);
                        W_Cheese.SetActive(true);
                        E_Egg.SetActive(true);
                        R_KMayo.SetActive(true);

                        CheckBurgerIngredients(BurgerObj4);
                    }
                    else
                    {
                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }

                    if (BurgerObj4.curBurState == BurgerObj.BurgerState.Burnt)
                    {
                        burntText.SetActive(true);
                        cookingText.SetActive(false);
                        serveText.SetActive(false);

                        Q_Bun.SetActive(false);
                        W_Cheese.SetActive(false);
                        E_Egg.SetActive(false);
                        R_KMayo.SetActive(false);


                        HasBun.SetActive(false);
                        HasCheese.SetActive(false);
                        HasEgg.SetActive(false);
                        HasKM.SetActive(false);
                    }
                }
                break;

            case OrderHandler.SelectedOrder.None:
                BurgerObj1.isSelected = false;
                BurgerObj2.isSelected = false;
                BurgerObj3.isSelected = false;
                BurgerObj4.isSelected = false;

                CookButton.SetActive(false);
                serveText.SetActive(false);
                Q_Bun.SetActive(false);
                W_Cheese.SetActive(false);
                E_Egg.SetActive(false);
                R_KMayo.SetActive(false);


                HasBun.SetActive(false);
                HasCheese.SetActive(false);
                HasEgg.SetActive(false);
                HasKM.SetActive(false);

                break;
        }
    }
}
