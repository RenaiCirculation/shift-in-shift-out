﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject Menu;
    bool isOpen;

    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;

    }

    // Update is called once per frame
    void Update()
    {
        Menu.SetActive(isOpen);
    }

    public void OpenMenu()
    {
        isOpen = !isOpen;
    }
}
