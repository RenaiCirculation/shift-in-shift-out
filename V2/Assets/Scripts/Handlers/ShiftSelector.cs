﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShiftSelector : MonoBehaviour
{
    public int Hours;
    public Text HoursText;

    public float Salary;
    public Text SalaryText;

    public float Mood;
    public Text MoodText;

    public bool selected;

    ShiftHandler SH;

    // Start is called before the first frame update
    void Start()
    {
        selected = false;
        SH = GameObject.FindGameObjectWithTag("GameController").GetComponent<ShiftHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        HoursText.text = Hours.ToString();
        SalaryText.text = "P " + Salary.ToString();
        MoodText.text = "-" + Mood.ToString();

        if(selected)
        {
            this.GetComponent<Button>().interactable = false;
        }
        else
        {
            this.GetComponent<Button>().interactable = true;
        }

    }

    public void SelectedThis()
    {        
        SH.RecieveShift(Hours,this.gameObject, Salary, Mood);        
    }
}
