﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableHandler : MonoBehaviour
{

    public GameObject Chair1, Chair2, Chair3, Chair4;

    public List<GameObject> Chair1Line = new List<GameObject>();
    public List<GameObject> Chair2Line = new List<GameObject>();
    public List<GameObject> Chair3Line = new List<GameObject>();
    public List<GameObject> Chair4Line = new List<GameObject>();

    public List<Customer> Line1Customers = new List<Customer>();
    public List<Customer> Line2Customers = new List<Customer>();
    public List<Customer> Line3Customers = new List<Customer>();
    public List<Customer> Line4Customers = new List<Customer>();

    public Customer CInSeat1, CInSeat2, CInSeat3, CInSeat4;
    OrderHandler OH;

    public Transform Exitpos;

    GameObject MainCam;

    SoundHandler SH;   

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();

        OH = GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>();
        MainCam = GameObject.FindGameObjectWithTag("MainCamera");
        CInSeat1 = null;
        CInSeat2 = null;
        CInSeat3 = null;
        CInSeat4 = null;        

    }

    // Update is called once per frame
    void Update()
    {
        CheckIfCanSeat();
        //DisplayCustomerAttitude();       
        FacePlayer();
    }

    void FacePlayer()
    {
        if (CInSeat1 != null)
        {
            var lookPos = MainCam.transform.position - CInSeat1.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);

            CInSeat1.transform.rotation = Quaternion.Slerp(CInSeat1.transform.rotation, rotation, Time.deltaTime * 3);
        }

        if (CInSeat2 != null)
        {
            var lookPos = MainCam.transform.position - CInSeat2.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);

            CInSeat2.transform.rotation = Quaternion.Slerp(CInSeat2.transform.rotation, rotation, Time.deltaTime * 3);
        }

        if (CInSeat3 != null)
        {
            var lookPos = MainCam.transform.position - CInSeat3.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);

            CInSeat3.transform.rotation = Quaternion.Slerp(CInSeat3.transform.rotation, rotation, Time.deltaTime * 3);
        }

        if (CInSeat4 != null)
        {
            var lookPos = MainCam.transform.position - CInSeat4.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);

            CInSeat4.transform.rotation = Quaternion.Slerp(CInSeat4.transform.rotation, rotation, Time.deltaTime * 3);
        }
    }

    void DisplayCustomerAttitude()
    {
        if(CInSeat1)
        {
            if(CInSeat1.CurCusState == Customer.CustomerState.InQueue)
            {
                CInSeat1.GetComponent<CustomerMood>().DisplayAttitude();
                CInSeat1.CurCusState = Customer.CustomerState.Waiting;
            }
            
        }

        if(CInSeat2)
        {
            if (CInSeat2.CurCusState == Customer.CustomerState.InQueue)
            {
                CInSeat2.GetComponent<CustomerMood>().DisplayAttitude();
                CInSeat2.CurCusState = Customer.CustomerState.Waiting;
            }

        }

        if (CInSeat3)
        {
            if (CInSeat3.CurCusState == Customer.CustomerState.InQueue)
            {
                CInSeat3.GetComponent<CustomerMood>().DisplayAttitude();
                CInSeat3.CurCusState = Customer.CustomerState.Waiting;
            }
        }

        if (CInSeat4)
        {
            if (CInSeat4.CurCusState == Customer.CustomerState.InQueue)
            {
                CInSeat4.GetComponent<CustomerMood>().DisplayAttitude();
                CInSeat4.CurCusState = Customer.CustomerState.Waiting;
            }
        }
    }

    public void RemoveCustomer(int x)
    {
        //Debug.Log("Removed @ " + x);
        switch (x)
        {
            case 1:
                CInSeat1 = null;
                break;

            case 2:
                CInSeat2 = null;
                break;

            case 3:
                CInSeat3 = null;
                break;

            case 4:
                CInSeat4 = null;
                break;

        }        
    }

    public void RemoveAllCustomers()
    {
        foreach(Customer c in Line1Customers)
        {
            c.MoveToExit(Exitpos);
            //Line1Customers.Remove(c);
        }
        Line1Customers.Clear();

        foreach (Customer c in Line2Customers)
        {
            c.MoveToExit(Exitpos);
            //Line2Customers.Remove(c);
        }
        Line2Customers.Clear();

        foreach (Customer c in Line3Customers)
        {
            c.MoveToExit(Exitpos);
            //Line3Customers.Remove(c);
        }
        Line3Customers.Clear();

        foreach (Customer c in Line4Customers)
        {
            c.MoveToExit(Exitpos);
            //Line4Customers.Remove(c);
        }
        Line4Customers.Clear();

        foreach (GameObject s in GameObject.FindGameObjectsWithTag("Customer"))
        {
            s.GetComponent<Customer>().MoveToExit(Exitpos);
        }

    }

    bool CheckIfCustomerNear(int Line)
    {
        switch(Line)
        {
            case 1:
                float dis1 = Vector3.Distance(Line1Customers[0].transform.position, Chair1Line[0].transform.position);
                if (dis1 < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            case 2:
                float dis2 = Vector3.Distance(Line2Customers[0].transform.position, Chair2Line[0].transform.position);
                if (dis2 < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            case 3:
                float dis3 = Vector3.Distance(Line3Customers[0].transform.position, Chair3Line[0].transform.position);
                if (dis3 < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            case 4:
                float dis4 = Vector3.Distance(Line4Customers[0].transform.position, Chair4Line[0].transform.position);
                if (dis4 < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            default: return false;

        }
    }

    void CheckIfCanSeat()
    {
       
        if(Line1HasCustomers())
        {           
            if (CheckIfCustomerNear(1))
            {
                //Debug.Log("Checking 1");
                if (CInSeat1 == null)
                {
                    SH.CustomerArrived();
                    //Debug.Log("1 Started");
                    CInSeat1 = GetNextCustomer(1);
                    CInSeat1.MoveTo(Chair1.transform);
                    CInSeat1.GetComponent<CustomerMood>().StartCTimer();
                    OH.PlaceOrder(1, CInSeat1.GetOrder(), CInSeat1.GetComponent<CustomerMood>().MaxTimer);
                }
            }
        }       

        if (Line2HasCustomers())
        {
           
            if (CheckIfCustomerNear(2))
            {
                //Debug.Log("Checking 2");
                if (CInSeat2 == null)
                {
                    SH.CustomerArrived();
                    //Debug.Log("2 Started");
                    CInSeat2 = GetNextCustomer(2);
                    CInSeat2.MoveTo(Chair2.transform);
                    CInSeat2.GetComponent<CustomerMood>().StartCTimer();
                    OH.PlaceOrder(2, CInSeat2.GetOrder(), CInSeat2.GetComponent<CustomerMood>().MaxTimer);
                }
            }
        }
        

        if (Line3HasCustomers())
        {
            
            if (CheckIfCustomerNear(3))
            {
                //Debug.Log("Checking 3");
                if (CInSeat3 == null)
                {
                    SH.CustomerArrived();
                    //Debug.Log("3 Started");
                    CInSeat3 = GetNextCustomer(3);
                    CInSeat3.MoveTo(Chair3.transform);
                    CInSeat3.GetComponent<CustomerMood>().StartCTimer();
                    OH.PlaceOrder(3, CInSeat3.GetOrder(),CInSeat3.GetComponent<CustomerMood>().MaxTimer);
                }
            }
        }
       

        if (Line4HasCustomers())
        {
            
            if (CheckIfCustomerNear(4))
            {
                //Debug.Log("Checking 4");
                if (CInSeat4 == null)
                {
                    SH.CustomerArrived();
                    
                    //Debug.Log("4 Started");
                    CInSeat4 = GetNextCustomer(4);
                    CInSeat4.MoveTo(Chair4.transform);
                    CInSeat4.GetComponent<CustomerMood>().StartCTimer();
                    OH.PlaceOrder(4, CInSeat4.GetOrder(),CInSeat4.GetComponent<CustomerMood>().MaxTimer);
                }
            }
        }
       
    }

    public Customer GetNextCustomer(int lineNum)
    {
        Customer C = null;
        switch(lineNum)
        {
            case 1:
                if(Line1Customers.Count == 0)
                {
                    C = null;
                }
                else
                {
                    C = Line1Customers[0];
                    Line1Customers.RemoveAt(0);
                }
                break;

            case 2:
                if (Line2Customers.Count == 0)
                {
                    C = null;
                }
                else
                {
                    C = Line2Customers[0];
                    Line2Customers.RemoveAt(0);
                }
                break;

            case 3:
                if (Line3Customers.Count == 0)
                {
                    C = null;
                }
                else
                {
                    C = Line3Customers[0];
                    Line3Customers.RemoveAt(0);
                }
                break;

            case 4:
                if (Line4Customers.Count == 0)
                {
                    C = null;
                }
                else
                {
                    C = Line4Customers[0];
                    Line4Customers.RemoveAt(0);
                }
                break;
        }
        RelocateCustomersInList(lineNum);
        
        return C;
    }

    void RelocateCustomersInList(int lineNum)
    {
        switch(lineNum)
        {
            case 1:
                for (int i = 0; i < Line1Customers.Count; i++)
                {
                    Line1Customers[i].MoveTo(Chair1Line[i].transform);
                }
                break;

            case 2:
                for (int i = 0; i < Line2Customers.Count; i++)
                {
                    Line2Customers[i].MoveTo(Chair2Line[i].transform);
                }
                break;

            case 3:
                for (int i = 0; i < Line3Customers.Count; i++)
                {
                    Line3Customers[i].MoveTo(Chair3Line[i].transform);
                }
                break;

            case 4:
                for (int i = 0; i < Line4Customers.Count; i++)
                {
                    Line4Customers[i].MoveTo(Chair4Line[i].transform);
                }
                break;
        }
    }

    bool Line1HasCustomers()
    {
        if(Line1Customers.Count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool Line2HasCustomers()
    {
        if (Line2Customers.Count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool Line3HasCustomers()
    {
        if (Line3Customers.Count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool Line4HasCustomers()
    {
        if (Line4Customers.Count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void AddNewCustomer(Customer x)
    {
        if(Line1Customers.Count == 0)
        {            
            Line1Customers.Add(x);
            MoveAddedCustomer(1, x);
            x.AssignSlotNum(1);
            x.CurCusState = Customer.CustomerState.InQueue;
        }
        else if(Line2Customers.Count == 0)
        {
            
            Line2Customers.Add(x);
            MoveAddedCustomer(2, x);
            x.AssignSlotNum(2);
            x.CurCusState = Customer.CustomerState.InQueue;
        }
        else if (Line3Customers.Count == 0)
        {
            
            Line3Customers.Add(x);
            MoveAddedCustomer(3, x);
            x.AssignSlotNum(3);
            x.CurCusState = Customer.CustomerState.InQueue;
        }
        else if (Line4Customers.Count == 0)
        {
            
            Line4Customers.Add(x);
            MoveAddedCustomer(4, x);
            x.AssignSlotNum(4);
            x.CurCusState = Customer.CustomerState.InQueue;
        }

        else
        {
            if(CanAddLine1())
            {
                
                Line1Customers.Add(x);
                MoveAddedCustomer(1, x);
                x.AssignSlotNum(1);
                x.CurCusState = Customer.CustomerState.InQueue;
            }
            else if (CanAddLine2())
            {
                
                Line2Customers.Add(x);
                MoveAddedCustomer(2, x);
                x.AssignSlotNum(2);
                x.CurCusState = Customer.CustomerState.InQueue;
            }
            else if (CanAddLine3())
            {
                
                Line3Customers.Add(x);
                MoveAddedCustomer(3, x);
                x.AssignSlotNum(3);
                x.CurCusState = Customer.CustomerState.InQueue;
            }
            else if (CanAddLine4())
            {
                
                Line4Customers.Add(x);
                MoveAddedCustomer(4, x);
                x.AssignSlotNum(4);
                x.CurCusState = Customer.CustomerState.InQueue;
            }

            else
            {
                Debug.Log("broke - " + x.name );
                x.CurCusState = Customer.CustomerState.None;
            }
        }



    }

    void MoveAddedCustomer(int lineNum, Customer x)
    {
        switch(lineNum)
        {
            case 1:
                x.MoveTo(Chair1Line[Line1Customers.IndexOf(x)].transform);
                break;

            case 2:
                x.MoveTo(Chair2Line[Line2Customers.IndexOf(x)].transform);
                break;

            case 3:
                x.MoveTo(Chair3Line[Line3Customers.IndexOf(x)].transform);
                break;

            case 4:
                x.MoveTo(Chair4Line[Line4Customers.IndexOf(x)].transform);
                break;
        }

    }
    
    public bool CanAddLine1()
    {
        return Line1Customers.Count < Chair1Line.Count;
    }

    public bool CanAddLine2()
    {
        return Line2Customers.Count < Chair2Line.Count;
    }

    public bool CanAddLine3()
    {
        return Line3Customers.Count < Chair3Line.Count;
    }

    public bool CanAddLine4()
    {
        return Line4Customers.Count < Chair4Line.Count;
    }
}
