﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float WaitingTime;

    public float PositiveWaitTime, NeutralWaitTime, NegativeWaitTime;

    public GameObject EndScrn;

    public Text BurgersServedText;
    public Text SalaryEarnedText;
    public Text CompanyProfitsText;

    public int BurgersServed;
    public float SalaryThisDay;
    public float CompanyProfit;

    ShiftHandler SH;
    bool ended;

    GlobalStats GS;
    PlayerStats PS;

    public Text CompanyProfitsUI;
    public Text BurgersServedUI;

    public GameObject IdleBoundary1, IdleBoundary2, IdleBoundary3, IdleBoundary4;

    public Text CurSavings;
    public Text CurSalary;
    public Text CurExpenses;
    public float ExpensesAmt;

    public Text HappinessHit;

    public Text CurTotal;
    float totalAmt;


    public Vector3 GetRandomIdlePos()
    {
        float x = Random.Range(IdleBoundary1.transform.position.x, IdleBoundary4.transform.position.x);
        float z = Random.Range(IdleBoundary1.transform.position.z, IdleBoundary2.transform.position.z);

        return new Vector3(x, 0, z);
        //todo
    }

    // Start is called before the first frame update
    void Start()
    {
        SH = GetComponent<ShiftHandler>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();

        ended = false;

        EndScrn.SetActive(false);

        BurgersServed = 0;
        SalaryThisDay = 0;
        CompanyProfit = 0;
    }

    // Update is called once per frame
    void Update()
    {        
        if (ended)
        {
            int x = GameObject.FindGameObjectsWithTag("Customer").Length;
            //Debug.Log(x);
            if (x <=0)
            {
                Debug.Log("None");
                OpenEndScreen();
            }
        }
        BurgersServedText.text = BurgersServed.ToString();
        SalaryEarnedText.text = "₱ " + SalaryThisDay.ToString();
        CompanyProfitsText.text = "₱ " + CompanyProfit.ToString();

        BurgersServedUI.text = "x " + BurgersServed.ToString();
        CompanyProfitsUI.text = "₱ " + CompanyProfit.ToString();

        HappinessHit.text = "-" + SH.MoodToMinus.ToString() + " mood";
    }

    public void EndedShift()
    {
        ended = true;
    }

    public void AddBurger()
    {
        BurgersServed += 1;
        GS.TotalAmtOfBurgers++;

    }

    public void AddProfits(float x)
    {
        CompanyProfit += x;
    }

    public void OpenEndScreen()
    {
        EndScrn.SetActive(true);
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayShiftDone();
        CurSavings.text = "P " + GS.CurrentEarnings;
        CurSalary.text = "P " + SalaryThisDay;
        CurExpenses.text = "P " + ExpensesAmt;
        totalAmt = (GS.CurrentEarnings + SalaryThisDay) - ExpensesAmt;
        CurTotal.text = "P " + totalAmt;
        GS.CurrentEarnings = totalAmt;

        ended = false;
    }

    public void EndShift(string Scenename)
    {
        
        GS.thisDayEarnings = SalaryThisDay;
        GS.TotalSalaryEarned += SalaryThisDay;
        //GS.thisDayBonus = (BurgersServed / 3) + 10;
        PS.HappinessLevel -= SH.MoodToMinus; 

        SceneManager.LoadScene(Scenename);
    }
}
