﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{
    public GameObject CustomerPrefab;

    public List<GameObject> CustomersToday;

    public int CustomersToSpawn;
    int customersSpawned;

    public float TimeBetweenSpawn;
    float timer;

    public Transform EnterPos1, EnterPos2;

    ShiftHandler SH;

    GlobalStats GS;
    PlayerStats PS;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();

        SH = GetComponent<ShiftHandler>();
        customersSpawned = 0;
        AssignSpawnRate();
        timer = TimeBetweenSpawn - 1;

    }

    void AssignSpawnRate()
    {
        switch(GS.CurrentShiftTime)
        {
            case GlobalStats.ShiftTime.Morning:
                TimeBetweenSpawn = TimeBetweenSpawn * .9f;
                break;

            case GlobalStats.ShiftTime.Noon:
                TimeBetweenSpawn = TimeBetweenSpawn * 1f;
                break;

            case GlobalStats.ShiftTime.Night:
                TimeBetweenSpawn = TimeBetweenSpawn * 1.45f;
                break;
        }

        switch (GS.CurrentWeather)
        {
            case GlobalStats.Weather.NoModifier:
                TimeBetweenSpawn = TimeBetweenSpawn * 1f;
                break;

            case GlobalStats.Weather.Overcast:
                TimeBetweenSpawn = TimeBetweenSpawn * 0.5f;
                break;

            case GlobalStats.Weather.Rain:
                TimeBetweenSpawn = TimeBetweenSpawn * 1.5f;
                break;
                    
            case GlobalStats.Weather.Storm:
                TimeBetweenSpawn = TimeBetweenSpawn * 2f;
                break;
        }
        Debug.Log(TimeBetweenSpawn);
    }

    public GameObject GetRandomCustomerFromList()
    {
        int x = Random.Range(0, CustomersToday.Count);
        return CustomersToday[x];
    }

    // Update is called once per frame
    void Update()
    {
        if (SH.startedShift)
        {
            SpawnRepeat();
        }
        else
        {

        }
            
    }

    void SpawnRepeat()
    {
        if (timer <= TimeBetweenSpawn)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0;
            int x = Random.Range(0, 100);
            if( x > 50)
            {
                Instantiate(GetRandomCustomerFromList(), EnterPos1.position, Quaternion.identity);
            }
            else
            {
                Instantiate(GetRandomCustomerFromList(), EnterPos2.position, Quaternion.identity);
            }
            
        }
    }

    void StartSpawning()
    {
        if(customersSpawned < CustomersToSpawn)
        {
            if (timer <= TimeBetweenSpawn)
            {
                timer += Time.deltaTime;
            }

            else
            {
                timer = 0;
                Instantiate(CustomerPrefab, EnterPos1.position, Quaternion.identity);
                customersSpawned++;
            }

        }
        
    }
}
