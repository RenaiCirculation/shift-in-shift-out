﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShiftHandler : MonoBehaviour
{
    public float ShiftTime;
    public float internalTimer;

    public Image ShiftTimerImg;
    public Text ShiftTimeText;

    public bool startedShift;
    bool endedShift;

    public GameObject ShiftMenu;

    GameManager GM;
    GlobalStats GS;

    public Text TimeOfDayUI;
    public Image WeatherImage;
    public Sprite Day, Night, Overcast, Rain, Storm;

    public List<GameObject> ShiftButtons = new List<GameObject>();

    public GameObject MainCamera;
    public Transform BeforeShiftPos, ShiftPos;

    public GameObject MainUI, MoodUI, SelectShiftUI;

    public Text CurSelectedShiftUI;
    public GameObject NextButton;
    int selectedShift;

    public float MoodToMinus;
    public GameObject Spotlight;

    bool playShiftEndSFX;

    public GameObject CongratsMsg;
    public Text CongratsMsgTxt;

    // Start is called before the first frame update
    void Start()
    {
        GM = GetComponent<GameManager>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        selectedShift = 0;
        NextButton.GetComponent<Button>().interactable = false;
        playShiftEndSFX = false;
        if (GS.IsOnOvertime)
        {
            internalTimer += ShiftTime;
        }
        else
        {
            //nothing
        }
        AssignWeather();
        ShiftMenu.SetActive(true);

        MainCamera.transform.position = BeforeShiftPos.transform.position;
        MainCamera.transform.rotation = BeforeShiftPos.transform.rotation;

        MainUI.SetActive(false);
        MoodUI.SetActive(false);
        SelectShiftUI.SetActive(true);

        Spotlight.SetActive(true);
        CongratsMsg.SetActive(false);

        if(GS.CompletedMQ1 && !GS.ShowCongratsMsg1)
        {
            CongratsMsg.SetActive(true);
            CongratsMsgTxt.text = "Congrats, you completed the goal for week 1!";
            GS.ShowCongratsMsg1 = true;
        }
        
        if (GS.CompletedMQ2 && !GS.ShowCongratsMsg2)
        {
            CongratsMsg.SetActive(true);
            CongratsMsgTxt.text = "Congrats, you completed the goal for week 2!";
            GS.ShowCongratsMsg2 = true;
        }

        if (GS.CompletedMQ3 && !GS.ShowCongratsMsg3)
        {
            CongratsMsg.SetActive(true);
            CongratsMsgTxt.text = "Congrats, you completed the goal for week 3!";
            GS.ShowCongratsMsg2 = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        int min = Mathf.FloorToInt(internalTimer / 60);
        int sec = Mathf.FloorToInt(internalTimer % 60);

        ShiftTimeText.text = min.ToString("00") +":"+ sec.ToString("00");
        ShiftTimerImg.fillAmount = internalTimer / ShiftTime;

        if (startedShift)
        {
            Started();
            ShiftMenu.SetActive(false);
        }

        CurSelectedShiftUI.text = "Current shift: " + selectedShift.ToString() + " hours";

        
    }

    IEnumerator LerpFromTo(Transform pos2, float duration)
    {        
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            MainCamera.transform.position = Vector3.Lerp(MainCamera.transform.position, pos2.position, t / duration);
            MainCamera.transform.rotation = Quaternion.Lerp(MainCamera.transform.rotation, pos2.rotation, t / duration);
            yield return 0;
        }
        MainCamera.transform.position = pos2.transform.position;
    }
    void AssignWeather()
    {
        TimeOfDayUI.text = GS.CurrentShiftTime.ToString();

        switch(GS.CurrentWeather)
        {
            case GlobalStats.Weather.NoModifier:
                if(GS.CurrentShiftTime == GlobalStats.ShiftTime.Night)
                {
                    WeatherImage.sprite = Night;
                }
                else
                {
                    WeatherImage.sprite = Day;
                }
                break;

            case GlobalStats.Weather.Overcast:
                WeatherImage.sprite = Overcast;
                break;

            case GlobalStats.Weather.Rain:
                WeatherImage.sprite = Rain;
                break;

            case GlobalStats.Weather.Storm:
                WeatherImage.sprite = Storm;
                break;
        }

    }

    void Started()
    {
        if(internalTimer >= 0 )
        {
            internalTimer -= Time.deltaTime;
        }
        else
        {
            internalTimer = 0;
            startedShift = false;            
            GM.EndedShift();
        }
    }

    public void StartShiftButton()
    {
        startedShift = true;
    }

    public void RecieveShift(int shift, GameObject G, float Salary, float Mood)
    {
        selectedShift = shift;
        ShiftTime = shift * 10;

        NextButton.GetComponent<Button>().interactable = true;

        foreach (GameObject x in ShiftButtons)
        {
            x.GetComponent<ShiftSelector>().selected = false;
        }
        GetComponent<GameManager>().SalaryThisDay = Salary;
        G.GetComponent<ShiftSelector>().selected = true;
        MoodToMinus = Mood;
    }

    public void AdvanceStage()
    {
        StartCoroutine(LerpFromTo(ShiftPos, 5));

        MainUI.SetActive(true);
        MoodUI.SetActive(true);
        SelectShiftUI.SetActive(false);

        Spotlight.SetActive(false);
        internalTimer = ShiftTime;
        StartShiftButton();
    }
}
