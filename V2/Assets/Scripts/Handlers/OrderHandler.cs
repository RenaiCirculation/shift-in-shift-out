﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderHandler : MonoBehaviour
{
    public GameObject ExitPos;

    public bool PlacedOrder1, PlacedOrder2, 
        PlacedOrder3, PlacedOrder4;

    public GameObject Order1Obj, Order2Obj, Order3Obj, Order4Obj;

    public GameObject SelectionObj;

    public BurgerObj B1, B2, B3, B4;

    public float TimeToCook;

    TableHandler TH;
    GameManager GM;
    SoundHandler SH;

    public AudioClip Selection;

    public enum SelectedOrder
    {
        None,
        Order1,
        Order2,
        Order3,
        Order4
    }

    public SelectedOrder CurOrderSelected;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();

        Order1Obj.SetActive(false);
        Order2Obj.SetActive(false);
        Order3Obj.SetActive(false);
        Order4Obj.SetActive(false);

        PlacedOrder1 = false;
        PlacedOrder2 = false;
        PlacedOrder3 = false;
        PlacedOrder4 = false;


        CurOrderSelected = SelectedOrder.None;
        SelectionObj.SetActive(false);

        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        TH = GameObject.FindGameObjectWithTag("TableHandler").GetComponent<TableHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckCurOrder();
        SelectionController();
        CheckBurgers();
    }

    void CheckBurgers()
    {
        if(PlacedOrder1)
        {
            switch(B1.curBurState)
            {
                case BurgerObj.BurgerState.Cooking:
                    Order1Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Cooking;
                    break;

                case BurgerObj.BurgerState.Serving:
                    Order1Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Serving;
                    break;

                case BurgerObj.BurgerState.Burnt:
                    Order1Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Burnt;
                    break;

                case BurgerObj.BurgerState.None:
                    Order1Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.None;
                    break;

            }
        }

        if (PlacedOrder2)
        {
            switch (B2.curBurState)
            {
                case BurgerObj.BurgerState.Cooking:
                    Order2Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Cooking;
                    break;

                case BurgerObj.BurgerState.Serving:
                    Order2Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Serving;
                    break;

                case BurgerObj.BurgerState.Burnt:
                    Order2Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Burnt;
                    break;

                case BurgerObj.BurgerState.None:
                    Order2Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.None;
                    break;

            }
        }

        if (PlacedOrder3)
        {
            switch (B3.curBurState)
            {
                case BurgerObj.BurgerState.Cooking:
                    Order3Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Cooking;
                    break;

                case BurgerObj.BurgerState.Serving:
                    Order3Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Serving;
                    break;

                case BurgerObj.BurgerState.Burnt:
                    Order3Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Burnt;
                    break;

                case BurgerObj.BurgerState.None:
                    Order3Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.None;
                    break;

            }
        }

        if (PlacedOrder4)
        {
            switch (B4.curBurState)
            {
                case BurgerObj.BurgerState.Cooking:
                    Order4Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Cooking;
                    break;

                case BurgerObj.BurgerState.Serving:
                    Order4Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Serving;
                    break;

                case BurgerObj.BurgerState.Burnt:
                    Order4Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.Burnt;
                    break;

                case BurgerObj.BurgerState.None:
                    Order4Obj.GetComponent<OrderObj>().CurState = OrderObj.CurrentState.None;
                    break;

            }
        }
    }

    void SelectionController()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            
            if (PlacedOrder1)
            {
                CurOrderSelected = SelectedOrder.Order1;
                SH.PlayAudioClip(Selection);
            }
                
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {

            if (PlacedOrder2)
            {
                SH.PlayAudioClip(Selection);
                CurOrderSelected = SelectedOrder.Order2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {

            if (PlacedOrder3)
            {
                SH.PlayAudioClip(Selection);
                CurOrderSelected = SelectedOrder.Order3;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {

            if (PlacedOrder4)
            {
                SH.PlayAudioClip(Selection);
                CurOrderSelected = SelectedOrder.Order4;
            }
        }

        

    }

    void CheckCurOrder()
    {
       
        switch(CurOrderSelected)
        {
            case SelectedOrder.None:
                SelectionObj.SetActive(false);
                break;

            case SelectedOrder.Order1:
                SelectionObj.SetActive(true);
                SelectionObj.transform.position =
                    new Vector3(SelectionObj.transform.position.x, Order1Obj.transform.position.y, 0);
                break;

            case SelectedOrder.Order2:
                SelectionObj.SetActive(true);
                SelectionObj.transform.position =
                    new Vector3(SelectionObj.transform.position.x, Order2Obj.transform.position.y, 0);
                break;

            case SelectedOrder.Order3:
                SelectionObj.SetActive(true);
                SelectionObj.transform.position =
                    new Vector3(SelectionObj.transform.position.x, Order3Obj.transform.position.y, 0);
                break;

            case SelectedOrder.Order4:
                SelectionObj.SetActive(true);
                SelectionObj.transform.position =
                    new Vector3(SelectionObj.transform.position.x, Order4Obj.transform.position.y, 0);
                break;


        }
    }

    public void PlaceOrder(int tableNum, int orderNum, float MaxTimer)
    {
        switch(tableNum)
        {
            case 1:
                PlacedOrder1 = true;
                Order1Obj.SetActive(true);
                Order1Obj.GetComponent<OrderObj>().AssignValue(orderNum, MaxTimer);
                break;

            case 2:
                PlacedOrder2 = true;
                Order2Obj.SetActive(true);
                Order2Obj.GetComponent<OrderObj>().AssignValue(orderNum, MaxTimer);
                break;

            case 3:
                PlacedOrder3 = true;
                Order3Obj.SetActive(true);
                Order3Obj.GetComponent<OrderObj>().AssignValue(orderNum, MaxTimer);
                break;

            case 4:
                PlacedOrder4 = true;
                Order4Obj.SetActive(true);
                Order4Obj.GetComponent<OrderObj>().AssignValue(orderNum, MaxTimer);
                break;
        }
    }

    public void ReceiveOrder(int Slot, int orderNum)
    {
        //Debug.Log(Slot + " == " + orderNum);
        switch (Slot)
        {
            case 1:
                Order1Obj.GetComponent<OrderObj>().GetOrder(orderNum, Slot);
                break;

            case 2:
                Order2Obj.GetComponent<OrderObj>().GetOrder(orderNum, Slot);
                break;

            case 3:
                Order3Obj.GetComponent<OrderObj>().GetOrder(orderNum, Slot);
                break;

            case 4:
                Order4Obj.GetComponent<OrderObj>().GetOrder(orderNum, Slot);
                break;
        }

    }

    public void CorrectOrder(int Slot)
    {
        GM.AddBurger();        
        switch(Slot)
        {
            case 1:
                TH.CInSeat1.OrderCorrect();
                Order1Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B1.ResetObj();
                PlacedOrder1 = false;               
                TH.CInSeat1.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(Slot);
                break;

            case 2:
                TH.CInSeat2.OrderCorrect();
                Order2Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B2.ResetObj();
                PlacedOrder2 = false;
                TH.CInSeat2.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(Slot);
                break;

            case 3:
                TH.CInSeat3.OrderCorrect();
                Order3Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B3.ResetObj();
                PlacedOrder3 = false;
                TH.CInSeat3.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(Slot);
                break;

            case 4:
                TH.CInSeat4.OrderCorrect();
                Order4Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B4.ResetObj();
                PlacedOrder4 = false;
                TH.CInSeat4.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(Slot);
                break;

        }
        
    }

    public void CustomerWrongOrder(int slot)
    {
        switch(slot)
        {
            case 1:
                TH.CInSeat1.gameObject.GetComponent<CustomerMood>().SubtractTimer();
                break;
            case 2:
                TH.CInSeat2.gameObject.GetComponent<CustomerMood>().SubtractTimer();
                break;
            case 3:
                TH.CInSeat3.gameObject.GetComponent<CustomerMood>().SubtractTimer();
                break;
            case 4:
                TH.CInSeat4.gameObject.GetComponent<CustomerMood>().SubtractTimer();
                break;
        }
    }

    public void CustomerLeft(int slot)
    {
        switch (slot)
        {
            case 1:
                Order1Obj.GetComponent<OrderObj>().ResetObj();
                Order1Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B1.ResetObj();
                PlacedOrder1 = false;
                TH.CInSeat1.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(slot);
                break;

            case 2:
                Order2Obj.GetComponent<OrderObj>().ResetObj();
                Order2Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B2.ResetObj();
                PlacedOrder2 = false;
                TH.CInSeat2.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(slot);
                break;

            case 3:
                Order3Obj.GetComponent<OrderObj>().ResetObj();
                Order3Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B3.ResetObj();
                PlacedOrder3 = false;
                TH.CInSeat3.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(slot);
                break;

            case 4:
                Order4Obj.GetComponent<OrderObj>().ResetObj();
                Order4Obj.SetActive(false);
                CurOrderSelected = SelectedOrder.None;
                B4.ResetObj();
                PlacedOrder4 = false;
                TH.CInSeat4.MoveToExit(ExitPos.transform);
                TH.RemoveCustomer(slot);
                break;
        }
    }

    public void RemoveAllCurCustomers()
    {
        TH.RemoveAllCustomers();       
        if(TH.CInSeat1)
            CustomerLeft(1);

        if (TH.CInSeat2)
            CustomerLeft(2);

        if (TH.CInSeat3)
            CustomerLeft(3);

        if (TH.CInSeat4)
            CustomerLeft(4);
    }
}
