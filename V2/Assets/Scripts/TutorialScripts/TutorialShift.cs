﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShift : MonoBehaviour
{
    public int currentSlide;

    public GameObject Slide1, Slide2, Slide3, Slide4,Slide5;
    public GameObject Slide6, Slide7, Slide8, Slide9, Slide10;
    public GameObject Slide11, Slide12, Slide13, Slide14, Slide15;
    public GameObject Slide16;

    public GameObject Slide11_1;

    public bool hasSelectedShift;

    bool spawnedCustomer;
    public GameObject CustomerToSpawn1;
    public GameObject CustomerToSpawn2;

    public Transform EnterPos1;
    public TableHandler TH;

    public OrderObj curOrder;
    public BurgerObj curBurger;
    public MessageController MC;
    public ShiftHandler SH;

    bool HoldShiftTime;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<EventsController>().curEvent = EventsController.EffectToday.None;
        HoldShiftTime = false;
        currentSlide = 1;
        Slide1.SetActive(false);
        Slide2.SetActive(false);
        Slide3.SetActive(false);
        Slide4.SetActive(false);
        Slide5.SetActive(false);
        Slide6.SetActive(false);
        Slide7.SetActive(false);
        Slide8.SetActive(false);
        Slide9.SetActive(false);
        Slide10.SetActive(false);
        Slide11.SetActive(false);
        Slide11_1.SetActive(false);
        Slide12.SetActive(false);
        Slide13.SetActive(false);
        Slide14.SetActive(false);
        Slide15.SetActive(false);
        Slide16.SetActive(false);

        hasSelectedShift = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(HoldShiftTime)
        {
            SH.internalTimer = 60;
        }

        switch (currentSlide)
        {
            case 1:
                Slide1.SetActive(true);
                break;

            case 2:
                Slide1.SetActive(false);

                Slide2.SetActive(true);
                break;

            case 3:
                HoldShiftTime = true;
                Slide2.SetActive(false);

                Slide3.SetActive(true);
                break;

            case 4:
                Slide3.SetActive(false);

                Slide4.SetActive(true);

                Time.timeScale = 0;
                break;

            case 5:
                Slide4.SetActive(false);

                Slide5.SetActive(true);
                Time.timeScale = 0;
                break;

            case 6:
                Slide5.SetActive(false);

                Slide6.SetActive(true);
                Time.timeScale = 1;

                if(!spawnedCustomer)
                {
                    Instantiate(CustomerToSpawn1, EnterPos1.position, Quaternion.identity);
                    
                    spawnedCustomer = true;
                }

                CheckIfCustomerInside();
                break;

            case 7:
                Slide6.SetActive(false);
                Slide7.SetActive(true);
                Time.timeScale = 0;
                TH.CInSeat1.GetComponent<CustomerMood>().CanDisplayEndMessage = false;

                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    Next();
                    Time.timeScale = 1;
                }
                break;

            case 8:
                Slide7.SetActive(false);
                Slide8.SetActive(true);
                Time.timeScale = 0;
                if (Input.GetKeyDown(KeyCode.F))
                {
                    Next();
                    Time.timeScale = 1;                    
                }
                break;

            case 9:
                Slide8.SetActive(false);
                Slide9.SetActive(true);

                if(curOrder.CurState == OrderObj.CurrentState.Serving)
                {
                    Time.timeScale = 0;
                    Next();
                }
                break;

            case 10:
                Slide9.SetActive(false);
                Slide10.SetActive(true);
                curBurger.CanInput = false;
                break;

            case 11:
                curBurger.CanInput = true;
                Time.timeScale = 1;
                Slide10.SetActive(false);
                Slide11.SetActive(true);
                if (curOrder.CurState == OrderObj.CurrentState.Burnt)
                {
                    Slide11_1.SetActive(true);
                }
                
                if(TH.CInSeat1 == null)
                {
                    spawnedCustomer = false;
                    Next();
                }
                break;

            case 12:
                Slide11.SetActive(false);
                Slide12.SetActive(true);
                if (!spawnedCustomer)
                {
                    Instantiate(CustomerToSpawn2, EnterPos1.position, Quaternion.identity);
                    
                    spawnedCustomer = true;
                }
                CheckIfCustomerInside();
                break;

            case 13:               

                Slide12.SetActive(false);
                Slide13.SetActive(true);
                spawnedCustomer = false;
                if (TH.CInSeat1 != null)
                {
                    TH.CInSeat1.GetComponent<CustomerMood>().SureDisplayEndMessage = true;
                }
                if(MC.HasEndMessageOpen)
                {
                    Time.timeScale = .2f;
                    Next();
                }
                break;

            case 14:
                Slide13.SetActive(false);
                Slide14.SetActive(true);
                if(!MC.HasEndMessageOpen)
                {
                    Next();
                }
                break;

            case 15:
                Slide14.SetActive(false);
                Slide15.SetActive(true);
                
                if (!spawnedCustomer)
                {
                    Instantiate(CustomerToSpawn1, EnterPos1.position, Quaternion.identity);
                    spawnedCustomer = true;
                }

                if (SH.startedShift == false)
                {
                    Next();
                }

                if(GameObject.FindGameObjectsWithTag("Customer").Length <= 0)
                {
                    if(HoldShiftTime)
                    {
                        HoldShiftTime = false;
                        SH.internalTimer = 5;
                    }
                    
                }
                break;

            case 16:
                Slide15.SetActive(false);
                Slide16.SetActive(true);
                break;



        }
    }


    void CheckIfCustomerInside()
    {
        if(TH.CInSeat1 != null)
        {
            Next();
        }
    }

    public void SelectThisShift()
    {
        if(!hasSelectedShift)
        {
            hasSelectedShift = true;
            currentSlide++;
        }
        
    }


    public void Next()
    {
        if(currentSlide != 2)
        {
            currentSlide++;
        }            
    }
}
