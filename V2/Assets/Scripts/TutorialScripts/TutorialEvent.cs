﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEvent : MonoBehaviour
{
    public int currentSlide;

    public EventHandler EH;
    bool hasOveridden;
    public EventWChoice toOver;


    public GameObject Slide1, Slide2, Slide3;

    // Start is called before the first frame update
    void Start()
    {
        hasOveridden = false;

        currentSlide = 1;
        Slide1.SetActive(false);
        Slide2.SetActive(false);
        Slide3.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentSlide)
        {
            case 1:
                Slide1.SetActive(true);
                if (!hasOveridden)
                {
                    EH.OverrideCurEvent(toOver);
                    hasOveridden = true;
                }
                break;

            case 2:
                Slide1.SetActive(false);
                Slide2.SetActive(true);
                break;

            case 3:
                Slide2.SetActive(false);
                Slide3.SetActive(true);
                break;
        }

    }

    public void Next()
    {
        currentSlide++;
    }
}
