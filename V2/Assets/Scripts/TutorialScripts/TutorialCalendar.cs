﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCalendar : MonoBehaviour
{
    public int currentSlide;

    public GameObject Slide1,Slide2, Slide3, Slide4;


    // Start is called before the first frame update
    void Start()
    {
        currentSlide = 1;
        Slide1.SetActive(false);
        Slide2.SetActive(false);
        Slide3.SetActive(false);
        Slide4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentSlide)
        {
            case 1:
                Slide1.SetActive(true);
                break;

            case 2:
                Slide1.SetActive(false);

                Slide2.SetActive(true);
                break;

            case 3:
                Slide2.SetActive(false);

                Slide3.SetActive(true);
                break;

            case 4:
                Slide3.SetActive(false);

                Slide4.SetActive(true);
                break;
        }
    }

    public void Next()
    {
        currentSlide++;
    }
}
