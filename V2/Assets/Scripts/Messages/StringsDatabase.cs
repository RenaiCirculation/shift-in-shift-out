﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringsDatabase : MonoBehaviour
{
    public List<string> WaitingQuips = new List<string>();

    public List<string> PositiveEndMessages = new List<string>();
    public List<string> NeutralEndMessages = new List<string>();
    public List<string> NegativeEndMessages = new List<string>();

    public List<string> PositiveRanOutMsg = new List<string>();
    public List<string> NeutralRanOutMsg = new List<string>();
    public List<string> NegativeRanOutMsg = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GetRandomWaitingQuip()
    {
        int x = Random.Range(0, WaitingQuips.Count);
        return WaitingQuips[x];
    }

    public string GetRandomPositiveEndMessage()
    {
        int x = Random.Range(0, PositiveEndMessages.Count);
        return PositiveEndMessages[x];
    }

    public string GetRandomNeutralEndMessage()
    {
        int x = Random.Range(0, NeutralEndMessages.Count);
        return NeutralEndMessages[x];
    }

    public string GetRandomNegativeEndMessage()
    {
        int x = Random.Range(0, NegativeEndMessages.Count);
        return NegativeEndMessages[x];
    }

    public string GetRandomPositiveRanOutMessage()
    {
        int x = Random.Range(0, PositiveRanOutMsg.Count);
        return PositiveRanOutMsg[x];
    }
    public string GetRandomNeutralRanOutMessage()
    {
        int x = Random.Range(0, NeutralRanOutMsg.Count);
        return NeutralRanOutMsg[x];
    }
    public string GetRandomNegativeRanOutMessage()
    {
        int x = Random.Range(0, NegativeRanOutMsg.Count);
        return NegativeRanOutMsg[x];
    }
}
