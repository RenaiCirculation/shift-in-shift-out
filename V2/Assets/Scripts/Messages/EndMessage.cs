﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndMessage : MonoBehaviour
{
    [Range(1,4)]public int slot;

    public Text CustomerName;
    public Text Message;

    public Text PositiveReplyText, NegativeReplyText;

    public Image TimerBar;
    public float Timeout;
    float internalTimer;

    MessageController MC;
    [Range(1,3)]int attitude;
    bool ranOut;

    // Start is called before the first frame update
    void Start()
    {
        MC = GameObject.FindGameObjectWithTag("MController").
            GetComponent<MessageController>();
        internalTimer = Timeout;
        //attitude = 0;
        ranOut = false;
    }

    public void AssignValues(string x, int curAtt, bool timer,string Cname)
    {
        internalTimer = Timeout;
        Message.text = x;
        attitude = curAtt;        
        ranOut = timer;
        CustomerName.text = Cname;
        AssignRepliesText();
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayerEndMessage();
    }

    // Update is called once per frame
    void Update()
    {
        TimerBar.fillAmount = internalTimer / Timeout;
        if (internalTimer > 0)
        {
            internalTimer -= Time.deltaTime;
        }
        else
        {
            internalTimer = Timeout;
            MC.EndMessageNoAnswer(slot);
        }

        if(Input.GetKeyDown(KeyCode.F1))
        {
            PositiveReply();
            internalTimer = Timeout;
        }

        else if(Input.GetKeyDown(KeyCode.F2))
        {
            NegativeReply();
            internalTimer = Timeout;
        }
    }

    void AssignRepliesText()
    {
        switch (attitude)
        {
            case 1://positive
                switch (ranOut)
                {
                    case true:
                        PositiveReplyText.text = "Sorry sir";
                        NegativeReplyText.text = "It's my fault, sorry sir";
                        break;

                    case false:
                        PositiveReplyText.text = "Thank you come again";
                        NegativeReplyText.text = "You're welcome..";
                        break;
                }
                break;

            case 2://neutral
                switch (ranOut)
                {
                    case true:
                        PositiveReplyText.text = "Sorry about that..";
                        NegativeReplyText.text = "Yeah, my bad";
                        break;

                    case false:
                        PositiveReplyText.text = "Thank you";
                        NegativeReplyText.text = "...";
                        break;
                }
                break;

            case 3://Negative
                switch (ranOut)
                {
                    case true:
                        PositiveReplyText.text = "Yeah, sorry I guess..";
                        NegativeReplyText.text = "Sorry, this is very easy..";
                        break;

                    case false:
                        PositiveReplyText.text = "Thanks...";
                        NegativeReplyText.text = "...";
                        break;
                }
                break;

        }
    }
    public void PositiveReply()
    {        
        MC.PositiveMessageClicked(attitude,ranOut);
    }

    public void NegativeReply()
    {        
        MC.NegativeMessageClicked(attitude,ranOut);
    }
}
