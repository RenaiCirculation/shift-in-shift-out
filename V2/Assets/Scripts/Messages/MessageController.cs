﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MessageController : MonoBehaviour
{
    public GameObject Display1, Display2, Display3, Display4;
    Text TextDis1, TextDis2, TextDis3, TextDis4;

    //public GameObject EndMessage1, EndMessage2, EndMessage3, EndMessage4;
    public GameObject EndMessage;

    StringsDatabase SD;
    PlayerStats PS;

    float Display1Fadeout, Display2Fadeout, Display3Fadeout, Display4Fadeout;
    public HappinessMeter HM;

    public bool HasEndMessageOpen;

    public GameObject MuggerMessage;
    public bool HasMuggerMessageOpen;
    // Start is called before the first frame update
    void Start()
    {
        Display1Fadeout = 0;
        Display2Fadeout = 0;
        Display3Fadeout = 0;
        Display4Fadeout = 0;

        HM = GameObject.FindGameObjectWithTag("HMeter").GetComponent<HappinessMeter>();
        SD = GameObject.FindGameObjectWithTag("GameController").GetComponent<StringsDatabase>();
        PS = GameObject.FindGameObjectWithTag("GameController").GetComponent<PlayerStats>();

        TextDis1 = Display1.transform.GetChild(1).transform.gameObject.GetComponent<Text>();
        TextDis2 = Display2.transform.GetChild(1).transform.gameObject.GetComponent<Text>();
        TextDis3 = Display3.transform.GetChild(1).transform.gameObject.GetComponent<Text>();
        TextDis4 = Display4.transform.GetChild(1).transform.gameObject.GetComponent<Text>();

        Display1.SetActive(false);
        Display2.SetActive(false);
        Display3.SetActive(false);
        Display4.SetActive(false);

        //EndMessage1.SetActive(false);
        //EndMessage2.SetActive(false);
        //EndMessage3.SetActive(false);
        //EndMessage4.SetActive(false);

        EndMessage.SetActive(false);
        HasEndMessageOpen = false;

        MuggerMessage.SetActive(false);
        HasMuggerMessageOpen = false;


    }

    // Update is called once per frame
    void Update()
    {
        CheckActiveDisplays();  
    }

    public void CheckActiveDisplays()
    {
        if(Display1.activeInHierarchy)
        {
            if(Display1Fadeout <= 3)
            {
                Display1Fadeout += Time.deltaTime;
            }
            else
            {
                Display1.SetActive(false);
                Display1Fadeout = 0;
            }
        }

        if (Display2.activeInHierarchy)
        {
            if (Display2Fadeout <= 3)
            {
                Display2Fadeout += Time.deltaTime;
            }
            else
            {
                Display2.SetActive(false);
                Display2Fadeout = 0;
            }
        }

        if (Display3.activeInHierarchy)
        {
            if (Display3Fadeout <= 3)
            {
                Display3Fadeout += Time.deltaTime;
            }
            else
            {
                Display3.SetActive(false);
                Display3Fadeout = 0;
            }
        }

        if (Display4.activeInHierarchy)
        {
            if (Display4Fadeout <= 3)
            {
                Display4Fadeout += Time.deltaTime;
            }
            else
            {
                Display4.SetActive(false);
                Display4Fadeout = 0;
            }
        }
    }

    public void DisplayEndMessage(int slot, int attitude, bool timerRanOut, string CName)
    {
        //1 = positve , 2 = neutral, 3 = negative
        //RanOut
        if (!HasEndMessageOpen)
        {
            Debug.Log("Called by -- " + slot + "- hasmesopen-" + HasEndMessageOpen);
            Time.timeScale = .5f;
            string f;
            

            switch (timerRanOut)
            {
                case true:
                    switch (attitude)
                    {
                        case 1:
                            f = SD.GetRandomPositiveRanOutMessage();
                            break;

                        case 2:
                            f = SD.GetRandomNeutralRanOutMessage();
                            break;

                        case 3:
                            f = SD.GetRandomNegativeRanOutMessage();
                            break;

                        default: throw new ArgumentOutOfRangeException();
                    }
                    break;

                case false:
                    switch (attitude)
                    {
                        case 1:
                            f = SD.GetRandomPositiveEndMessage();
                            break;

                        case 2:
                            f = SD.GetRandomNeutralEndMessage();
                            break;

                        case 3:
                            f = SD.GetRandomNegativeEndMessage();
                            break;

                        default: throw new ArgumentOutOfRangeException();
                    }
                    break;

                default: throw new ArgumentOutOfRangeException();
            }

            
            EndMessage.SetActive(true);
            EndMessage.GetComponent<EndMessage>().AssignValues(f, attitude, timerRanOut, CName);
            HasEndMessageOpen = true;
            //switch (slot)
            //{
            //    case 1:
            //        EndMessage1.SetActive(true);
            //        EndMessage1.GetComponent<EndMessage>().AssignValues(f, attitude, timerRanOut,1);
            //        break;

            //    case 2:
            //        EndMessage2.SetActive(true);
            //        EndMessage2.GetComponent<EndMessage>().AssignValues(f, attitude, timerRanOut,2);
            //        break;

            //    case 3:
            //        EndMessage3.SetActive(true);
            //        EndMessage3.GetComponent<EndMessage>().AssignValues(f, attitude, timerRanOut,3);
            //        break;

            //    case 4:
            //        EndMessage4.SetActive(true);
            //        EndMessage4.GetComponent<EndMessage>().AssignValues(f, attitude, timerRanOut,4);
            //        break;
            //}
        }
    }

    public void DisplayHalfwayWaitMessage(int slot)
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayWaitingMessage();
        Debug.Log("Called by -- " + slot);
        switch(slot)
        {
            case 1:
                Display1.SetActive(true);                
                TextDis1.text = SD.GetRandomWaitingQuip();
                break;

            case 2:
                Display2.SetActive(true);                
                TextDis2.text = SD.GetRandomWaitingQuip();
                break;

            case 3:
                Display3.SetActive(true);
                TextDis3.text = SD.GetRandomWaitingQuip();
                break;

            case 4:
                Display4.SetActive(true);
                TextDis4.text = SD.GetRandomWaitingQuip();
                break;
        }
    }

    public void EndMessageNoAnswer(int slot)
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayerEndNoAns();
        Time.timeScale = 1;
        HasEndMessageOpen = false;
        EndMessage.SetActive(false);
        HM.DecreaseHappinessBar(5);
    }

    public void PositiveMessageClicked(int att,bool ranOut)
    {
        Debug.Log(att + " Positive;");
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayerEndResp();
        switch (ranOut)
        {
            case true:
                switch (att)
                {
                    case 1:
                        HM.DecreaseHappinessBar(5);
                        break;

                    case 2:
                        HM.DecreaseHappinessBar(3);
                        break;

                    case 3:
                        HM.DecreaseHappinessBar(2);
                        break;

                    default: throw new ArgumentOutOfRangeException();
                }

                break;

            case false:
                switch (att)
                {
                    case 1:
                        HM.IncreaseHappinessBar(5);
                        break;

                    case 2:
                        HM.IncreaseHappinessBar(3);
                        break;

                    case 3:
                        HM.DecreaseHappinessBar(2);
                        break;

                    default: throw new ArgumentOutOfRangeException();
                }
                break;
        }

        Time.timeScale = 1;
        HasEndMessageOpen = false;
        EndMessage.SetActive(false);
    }

    public void NegativeMessageClicked(int att, bool ranOut)
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayerEndResp();
        Debug.Log(att + " Negative;");
        switch (ranOut)
        {
            case true:
                switch (att)
                {
                    case 1:
                        HM.DecreaseHappinessBar(5);
                        break;

                    case 2:
                        HM.DecreaseHappinessBar(3);
                        break;

                    case 3:
                        HM.DecreaseHappinessBar(2);
                        break;

                    default: throw new ArgumentOutOfRangeException();
                }

                break;

            case false:
                switch (att)
                {
                    case 1:
                        HM.DecreaseHappinessBar(7);
                        break;

                    case 2:
                        HM.DecreaseHappinessBar(5);
                        break;

                    case 3:
                        HM.IncreaseHappinessBar(3);
                        break;

                    default: throw new ArgumentOutOfRangeException();
                }
                break;
        }


       
        
        Time.timeScale = 1;
        HasEndMessageOpen = false;
        EndMessage.SetActive(false);    
    }

    public void ActivateMuggerMessage()
    {
        if(!HasMuggerMessageOpen)
        {
            Time.timeScale = .5f;
            MuggerMessage.SetActive(true);
            HasMuggerMessageOpen = true;

            MuggerMessage.GetComponent<MuggerMessage>().AssignRepliesText();
        }
    }

    public void MuggerYesClicked()
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayerEndResp();
        Debug.Log("Yes");
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().CompanyProfit = 0;

        Time.timeScale = 1;
        HasMuggerMessageOpen = false;
        MuggerMessage.SetActive(false);
        HM.DecreaseHappinessBar(20);

        GameObject.FindGameObjectWithTag("Mugger").GetComponent<Mugger>().Escape();
    }

    public void MuggerNoClicked()
    {
        Debug.Log("No");        
        Time.timeScale = 1;
        HasMuggerMessageOpen = false;
        MuggerMessage.SetActive(false);
        GameObject.Find("GameManager").GetComponent<GlobalUI>().LoadScene("DeadScene");
        //SceneManager.LoadScene("DeadScene");
    }

}
