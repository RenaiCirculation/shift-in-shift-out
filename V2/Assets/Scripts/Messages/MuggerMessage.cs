﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuggerMessage : MonoBehaviour
{
    public Text CustomerName;
    public Text Message;

    public Text PositiveReplyText, NegativeReplyText;

    public Image TimerBar;
    public float Timeout;
    float internalTimer;

    bool ranOut;

    MessageController MC;

    AudioSource AS;
    // Start is called before the first frame update
    void Start()
    {
        internalTimer = Timeout;
        ranOut = false;
        AS = GetComponent<AudioSource>();

        MC = GameObject.FindGameObjectWithTag("MController").
             GetComponent<MessageController>();

    }   

    public void AssignRepliesText()
    {
        Message.text = "Akin na ang pera ninyo!";
        ranOut = false;
        CustomerName.text = "Holdaper";

        PositiveReplyText.text = "F1. -Give money-";
        NegativeReplyText.text = "F2. -Refuse-";
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayGasp();
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().GunCock();
        Time.timeScale = .8f;
        //AS.Play();
        
    }
    // Update is called once per frame
    void Update()
    {
        TimerBar.fillAmount = internalTimer / Timeout;
        if (internalTimer > 0)
        {
            internalTimer -= Time.deltaTime;
        }
        else
        {
            internalTimer = 0;
            NegativeReply();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            PositiveReply();
        }

        else if (Input.GetKeyDown(KeyCode.F2))
        {
            NegativeReply();
        }
    }

    public void PositiveReply()
    {
        Time.timeScale = 1;
        AS.Stop();
        MC.MuggerYesClicked();
    }

    public void NegativeReply()
    {
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().GunShot();
        AS.Stop();
       
        MC.MuggerNoClicked();
    }
}
