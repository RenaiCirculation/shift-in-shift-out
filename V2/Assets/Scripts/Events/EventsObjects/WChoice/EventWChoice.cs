﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/With Choice Event", order = 1)]
public class EventWChoice : ScriptableObject
{
    public string Description;

    public string Option1Description;
    public string Option2Description;

    public bool Option1NegativeHappiness;
    public float Option1NegaH;

    public bool Option1PostiveHappiness;
    public float Option1PosiH;

    public bool Option1NegativeCash;
    public float Option1NegaCash;

    public bool Option1PostiveCash;
    public float Option1PosiCash;

    public bool Option1SkipDays;
    public int Option1DaysAmt;

    public bool Option2NegativeHappiness;
    public float Option2NegaH;

    public bool Option2PostiveHappiness;
    public float Option2PosiH;

    public bool Option2NegativeCash;
    public float Option2NegaCash;

    public bool Option2PostiveCash;
    public float Option2PosiCash;

    public bool Option2SkipDays;
    public int Option2DaysAmt;

    public Sprite ThisImage;
}
