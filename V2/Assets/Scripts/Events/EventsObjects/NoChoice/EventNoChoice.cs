﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/No Choice Event", order = 1)]
public class EventNoChoice : ScriptableObject
{
    public string Description;
    public string Effect;

    public bool NegativeHappiness;
    public float NegaH;

    public bool PostiveHappiness;
    public float PosiH;

    public bool NegativeCash;
    public float NegaCash;

    public bool PostiveCash;
    public float PosiCash;

    public bool SkipDays;
    public int DaysAmt;

    public Sprite ThisImage;
}
