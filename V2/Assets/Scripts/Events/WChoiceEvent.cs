﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class WChoiceEvent : MonoBehaviour
{
    public Text Description;
    public Text Option1Des, Option2Des;

    public string NextScene;
    public Image ImgToDisplay;

    EventWChoice EWC;

    PlayerStats PS;
    GlobalStats GS;
    EventHandler EH;
    // Start is called before the first frame update
    void Start()
    {
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        EH = GameObject.Find("Holder").GetComponent<EventHandler>();

    }

    public void AssignValues(EventWChoice x)
    {
        EWC = x;
        Description.text = EWC.Description;
        Option1Des.text = EWC.Option1Description;
        Option2Des.text = EWC.Option2Description;

        ImgToDisplay.sprite = EWC.ThisImage;
    }

    void ApplyEffect(int x)
    {
        switch (x)
        {
            case 1:
                if (EWC.Option1NegativeHappiness)
                {
                    Debug.Log(PS.HappinessLevel);
                    PS.DecreaseHappinessLevel(EWC.Option1NegaH);
                    Debug.Log("decrease happiness");
                    Debug.Log(PS.HappinessLevel);
                }
                else if (EWC.Option1PostiveHappiness)
                {
                    Debug.Log(PS.HappinessLevel);
                    PS.IncreaseHappinessLevel(EWC.Option1PosiH);
                    Debug.Log("increase happiness");
                    Debug.Log(PS.HappinessLevel);
                }

                if (EWC.Option1NegativeCash)
                {
                    Debug.Log(GS.CurrentEarnings);
                    GS.CurrentEarnings -= EWC.Option1NegaCash;
                    Debug.Log("NegativeCash");
                    Debug.Log(GS.CurrentEarnings);
                }

                else if (EWC.Option1PostiveCash)
                {
                    Debug.Log(GS.CurrentEarnings);
                    GS.CurrentEarnings += EWC.Option1PosiCash;
                    Debug.Log("Posicash");
                    Debug.Log(GS.CurrentEarnings);
                }

                if(EWC.Option1SkipDays)
                {
                    GS.SkipDays = true;
                    GS.SkipDaysAmt = EWC.Option1DaysAmt;
                }
                break;

            case 2:
                if (EWC.Option2NegativeHappiness)
                {
                    Debug.Log(PS.HappinessLevel);
                    PS.DecreaseHappinessLevel(EWC.Option2NegaH);
                    Debug.Log("decrease happiness");
                    Debug.Log(PS.HappinessLevel);
                }
                else if (EWC.Option2PostiveHappiness)
                {
                    Debug.Log(PS.HappinessLevel);
                    PS.IncreaseHappinessLevel(EWC.Option2PosiH);
                    Debug.Log("increase happiness");
                    Debug.Log(PS.HappinessLevel);
                }

                if (EWC.Option2NegativeCash)
                {
                    Debug.Log(GS.CurrentEarnings);
                    GS.CurrentEarnings -= EWC.Option2NegaCash;
                    Debug.Log("NegativeCash");
                    Debug.Log(GS.CurrentEarnings);
                }

                else if (EWC.Option2PostiveCash)
                {
                    Debug.Log(GS.CurrentEarnings);
                    GS.CurrentEarnings += EWC.Option2PosiCash;
                    Debug.Log("Posicash");
                    Debug.Log(GS.CurrentEarnings);
                }

                if(EWC.Option2SkipDays)
                {
                    GS.SkipDays = true;
                    GS.SkipDaysAmt = EWC.Option2DaysAmt;
                }
                break;

        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void Option1()
    {
        ApplyEffect(1);
        ContinueScene();
    }

    public void TutOption1(string scene)
    {
        ApplyEffect(1);
        EH.LoadNextScene(scene);    
    }

    public void TutOption2(string scene)
    {
        ApplyEffect(2);
        EH.LoadNextScene(scene);    
    }

    public void Option2()
    {
        ApplyEffect(2);
        ContinueScene();
    }

    void ContinueScene()
    {        
        EH.LoadNextScene(NextScene);
        //SceneManager.LoadScene(NextScene);
    }
}
