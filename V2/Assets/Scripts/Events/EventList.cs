﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventList : MonoBehaviour
{
    public List<EventNoChoice> EventNoChoiceList = new List<EventNoChoice>();
    public List<EventWChoice> EventWChoiceList = new List<EventWChoice>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public EventNoChoice GetRandomEventNoChoice()
    {
        int x = Random.Range(0, EventNoChoiceList.Count);
        return EventNoChoiceList[x];
    }

    public EventWChoice GetRandomEventWChoice()
    {
        int x = Random.Range(0, EventWChoiceList.Count);
        return EventWChoiceList[x];
    }
}
