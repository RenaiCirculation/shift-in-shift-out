﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NoChoiceEvent : MonoBehaviour
{    
    public Text Description, Effect;
    public Image ImageToDisplay;

    EventNoChoice currentEvent;

    PlayerStats PS;
    GlobalStats GS;

    EventHandler EH;

    // Start is called before the first frame update
    void Start()
    {
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        EH = GameObject.Find("Holder").GetComponent<EventHandler>();
    }

    public void AssignValues(EventNoChoice Ev)
    {
        currentEvent = Ev;
        Description.text = currentEvent.Description;
        Effect.text = currentEvent.Effect;

        ImageToDisplay.sprite = currentEvent.ThisImage;
    }

    void ApplyEffect()
    {
        if(currentEvent.NegativeHappiness)
        {
            Debug.Log(PS.HappinessLevel);
            PS.DecreaseHappinessLevel(currentEvent.NegaH);
            Debug.Log("decrease happiness");
            Debug.Log(PS.HappinessLevel);
        }
        else if(currentEvent.PostiveHappiness)
        {
            Debug.Log(PS.HappinessLevel);
            PS.IncreaseHappinessLevel(currentEvent.PosiH);
            Debug.Log("increase happiness");
            Debug.Log(PS.HappinessLevel);
        }

        if(currentEvent.NegativeCash)
        {
            Debug.Log(GS.CurrentEarnings);
            GS.CurrentEarnings -= currentEvent.NegaCash;
            Debug.Log("NegativeCash");
            Debug.Log(GS.CurrentEarnings);
        }

        else if(currentEvent.PostiveCash)
        {
            Debug.Log(GS.CurrentEarnings);
            GS.CurrentEarnings += currentEvent.PosiCash;
            Debug.Log("NegativeCash");
            Debug.Log(GS.CurrentEarnings);
        }

        if (currentEvent.SkipDays)
        {
            GS.SkipDays = true;
            GS.SkipDaysAmt = currentEvent.DaysAmt;
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void ContinueButton(string Scene)
    {
        ApplyEffect();
        EH.LoadNextScene(Scene);
        //SceneManager.LoadScene(Scene);
    }
}
