﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopBarEvent : MonoBehaviour
{
    public Text MoneyUI;

    public Image MoodFace, MoodBar;
    public Sprite Face1, Face2, Face3, Face4, Face5;

    PlayerStats PS;
    GlobalStats GS;
    // Start is called before the first frame update
    void Start()
    {
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
    }

    // Update is called once per frame
    void Update()
    {
        MoodBar.fillAmount = PS.HappinessLevel / 100;
        MoneyUI.text = "₱ :" + GS.CurrentEarnings;

        if (PS.HappinessLevel > 80)
        {
            MoodFace.sprite = Face1;
        }
        else if (PS.HappinessLevel > 60 && PS.HappinessLevel <= 80)
        {
            MoodFace.sprite = Face2;
        }
        else if (PS.HappinessLevel > 40 && PS.HappinessLevel <= 60)
        {
            MoodFace.sprite = Face3;
        }

        else if (PS.HappinessLevel > 20 && PS.HappinessLevel <= 40)
        {
            MoodFace.sprite = Face4;
        }
        else
        {
            MoodFace.sprite = Face5;
        }
    }
}
