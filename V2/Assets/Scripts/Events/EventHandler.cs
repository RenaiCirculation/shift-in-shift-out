﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventHandler : MonoBehaviour
{
    public GameObject NoChoiceEvent,WChoiceEvent,NoEvent;
    EventList EL;

    GameObject GS;

    public GlobalUI Global;

    public EventNoChoice debugOver;

    AudioSource AS;
    public AudioClip NoEventSFX, HasEventSFX;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler");
        AS = GetComponent<AudioSource>();
        EL = GetComponent<EventList>();
        NoChoiceEvent.SetActive(false);
        WChoiceEvent.SetActive(false);
        NoEvent.SetActive(false);
        CheckEvent();

    }

    void CheckEvent()
    {
        int x = Random.Range(0, 100);

        if (x > 66)
        {
            NoChoiceEvent.SetActive(true);
            AS.PlayOneShot(HasEventSFX);
            NoChoiceEvent.GetComponent<NoChoiceEvent>().AssignValues(EL.GetRandomEventNoChoice());
        }
        else if (x > 33 && x <= 66)
        {
            WChoiceEvent.SetActive(true);
            AS.PlayOneShot(HasEventSFX);
            WChoiceEvent.GetComponent<WChoiceEvent>().AssignValues(EL.GetRandomEventWChoice());
        }
        else
        {
            AS.PlayOneShot(NoEventSFX);
            NoEvent.SetActive(true);
        }
    }
    // Update is called once per frame
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.X))
        //{
        //    NoChoiceEvent.SetActive(true);
        //    WChoiceEvent.SetActive(false);
        //    NoEvent.SetActive(false);

        //    NoChoiceEvent.GetComponent<NoChoiceEvent>().AssignValues(debugOver);
        //}
    }

    public void OverrideCurEvent(EventWChoice overrideEvent)
    {
        NoChoiceEvent.SetActive(false);
        WChoiceEvent.SetActive(true);
        NoEvent.SetActive(false);

        WChoiceEvent.GetComponent<WChoiceEvent>().AssignValues(overrideEvent);
    }

    public void LoadNextScene(string scen)
    {
        //if(GS.GetComponent<GlobalStats>().CurrentDay == 1 || 
        //    GS.GetComponent<GlobalStats>().CurrentDay == 7 ||
        //    GS.GetComponent<GlobalStats>().CurrentDay == 14)
        //{
        //    GS.GetComponent<QuestController>().ResetQuests();
        //    GS.GetComponent<ItemController>().ResetItems();
        //}
        //SceneManager.LoadScene(scen);
        Global.LoadScene(scen);
    }
}
