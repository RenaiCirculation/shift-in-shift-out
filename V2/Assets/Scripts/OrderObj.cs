﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderObj : MonoBehaviour
{
    [Range(1,4)]public int Slot;

    public Image CookingBar, ServingBar, BurntBar;
    public Text OrderText;
    public Image TimerImg;
    public GameObject WrongOrderImg;

    OrderHandler OH;
    GameManager GM;
    float cookTime;
    float serveTime;

    public Image BurgerDisplay;
    public Sprite Blank,RegBur, EggBur, CheBur;

    public enum OrderType
    {
        None,
        Regular,
        Cheeseburger,
        EggBurger
    }
    public OrderType CurrentOrder;

    public float MaxTimer;
    [SerializeField]
    float internalTimer;

    bool startTimer;

    bool displayWrongTimer;
    float wrongTimer;

    public enum CurrentState
    {
        None,
        Cooking,
        Serving,
        Burnt
    }
    public CurrentState CurState;

    private void Awake()
    {
        CurrentOrder = OrderType.None;
        startTimer = false;

    }
    // Start is called before the first frame update
    void Start()
    {
        OH = GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>();
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        WrongOrderImg.SetActive(false);
        CurState = CurrentState.None;

        displayWrongTimer = false;
        wrongTimer = 0;
        MaxTimer = GM.WaitingTime;
        internalTimer = MaxTimer;

        cookTime = 0;
        serveTime = 20;
        CookingBar.fillAmount = 0;
        ServingBar.fillAmount = 0;
        BurntBar.fillAmount = 0;
    }

    public void ResetObj()
    {
        WrongOrderImg.SetActive(false);
        CurState = CurrentState.None;

        displayWrongTimer = false;
        wrongTimer = 0;
        MaxTimer = GM.WaitingTime;
        internalTimer = MaxTimer;

        cookTime = 0;
        serveTime = 20;
        CookingBar.fillAmount = 0;
        ServingBar.fillAmount = 0;
        BurntBar.fillAmount = 0;

        switch (CurrentOrder)
        {
            case OrderType.Cheeseburger:
                BurgerDisplay.sprite = CheBur;
                break;

            case OrderType.EggBurger:
                BurgerDisplay.sprite = EggBur;
                break;

            case OrderType.Regular:
                BurgerDisplay.sprite = RegBur;
                break;

            case OrderType.None:
                BurgerDisplay.sprite = Blank;
                break;

        }

    }

    public void AssignValue(int numOrder, float timer)
    {
        Debug.Log("started");
        
        switch (numOrder)
        {
            case 1:
                CurrentOrder = OrderType.Regular;
                break;

            case 2:
                CurrentOrder = OrderType.Cheeseburger;
                break;

            case 3:
                CurrentOrder = OrderType.EggBurger;
                break;
        }
        MaxTimer = timer;
        internalTimer = MaxTimer;
        startTimer = true;
    }

    public void GetOrder(int numOrder, int slot)
    {
        //Debug.Log(numOrder + " - " + name + " - " + CurrentOrder);
        if(numOrder == 1)
        {
            if(CurrentOrder == OrderType.Regular)
            {
                CorrectOrder(slot);
            }
            else
            {
                WrongOrder();
            }
        }
        if (numOrder == 2)
        {
            if(CurrentOrder == OrderType.Cheeseburger)
            {
                CorrectOrder(slot);
            }
            else
            {
                WrongOrder();
            }
        }

        if(numOrder == 3)
        {
            if(CurrentOrder == OrderType.EggBurger)
            {
                CorrectOrder(slot);
            }
            else
            {
                WrongOrder();
            }
        }
    }

    void CorrectOrder(int x)
    {
        OH.CorrectOrder(x);

        CurState = CurrentState.None;

        internalTimer = MaxTimer;

        cookTime = 0;
        serveTime = 20;
        CookingBar.fillAmount = 0;
        ServingBar.fillAmount = 0;
        BurntBar.fillAmount = 0;

        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayCorrectOrder();
    }

    void WrongOrder()
    {
        internalTimer -= 10;
        displayWrongTimer = true;
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayWrongOrder();
        OH.CustomerWrongOrder(Slot);
    }

    // Update is called once per frame
    void Update()
    {        
        if (startTimer == true)
        {            
            StartedTimer();
        }

        if(displayWrongTimer)
        {
            if(wrongTimer <= 2)
            {
                wrongTimer += Time.deltaTime;
                WrongOrderImg.SetActive(true);
            }
            else
            {
                displayWrongTimer = false;
                wrongTimer = 0;
                WrongOrderImg.SetActive(false);
            }
           
        }

        CheckState();

        switch (CurrentOrder)
        {
            case OrderType.None:
                //nothing
                break;

            case OrderType.Regular:
                OrderText.text = "Regular Burger";
                break;

            case OrderType.Cheeseburger:
                OrderText.text = "Cheeseburger";
                break;

            case OrderType.EggBurger:
                OrderText.text = "Egg Burger";
                break;
        }
    }

    void CheckState()
    {
        switch(CurState)
        {
            case CurrentState.None:
                //do nothing
                BurntBar.fillAmount = 0;
                break;

            case CurrentState.Cooking:
                BurntBar.fillAmount = 0;
                Cooking();
                break;

            case CurrentState.Serving:
                BurntBar.fillAmount = 0;
                Serving();
                break;

            case CurrentState.Burnt:
                Burnt();
                break;
        }
    }

    void Burnt()
    {
        cookTime = 0;
        serveTime = 20;
        CookingBar.fillAmount = 0;
        ServingBar.fillAmount = 0;

        BurntBar.fillAmount = 1;
    }

    void Serving()
    {
        if(serveTime <= 20)
        {
            serveTime -= Time.deltaTime;
        }
        else
        {
            CurState = CurrentState.Burnt;
            serveTime = 20;
        }
        ServingBar.fillAmount = serveTime / 20;
    }

    void Cooking()
    {
        if(cookTime <= OH.TimeToCook)
        {
            cookTime += Time.deltaTime;
        }
        else
        {
            CurState = CurrentState.Serving;
            cookTime = 0;
        }
        CookingBar.fillAmount = cookTime / OH.TimeToCook;
    }

    void StartedTimer()
    {
        if(internalTimer <= 0)
        {    
            OH.CustomerLeft(Slot);
            startTimer = false;

            CurState = CurrentState.None;

            internalTimer = MaxTimer;

            cookTime = 0;
            serveTime = 20;
            CookingBar.fillAmount = 0;
            ServingBar.fillAmount = 0;
            BurntBar.fillAmount = 0;
        }
        else
        {
            TimerImg.fillAmount = internalTimer / MaxTimer;
            internalTimer -= Time.deltaTime;
        }
    }

    
}
