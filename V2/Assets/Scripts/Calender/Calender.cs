﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calender : MonoBehaviour
{
    public List<GameObject> CalendarDays = new List<GameObject>();

    GlobalStats GS;
    SoundHandler SH; 

    public GameObject CustomersLostScrn;
    public Text CustoLostTxt;

    public GameObject StatsScrn;
    bool StatsOpen;

    public Text TSalary, TBurgers, TCustomers;

    AudioSource AS;
    public AudioClip PhoneSFX;

    public GameObject DayOffObj;
    public Text DayOffText;

    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();
        StatsOpen = false;
        AS = GetComponent<AudioSource>();

        foreach (GameObject G in CalendarDays)
        {
            G.transform.GetChild(1).gameObject.SetActive(false);
        }

        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        CheckCross();

        if(!GS.ShownCustoScreen)
        {
            if (GS.TotalAmtCustoLost > 6)
            {
                
                CustomersLostScrn.SetActive(true);
                GS.ShownCustoScreen = true;
                CustoLostTxt.text = "You lost a total of " + GS.TotalAmtCustoLost.ToString() + " customers so far";
                AS.PlayOneShot(PhoneSFX);
                SH.Vibrate();
            }
            
        }
        else
        {
            CustomersLostScrn.SetActive(false);
        }
        DayOffObj.SetActive(false);
        if (GS.SkipDays)
        {
            DayOffObj.SetActive(true);
            DayOffText.text = "Took " + (GS.SkipDaysAmt - 1) + " day/s off";
            GS.SkipDaysAmt = 0;
            GS.SkipDays = false;
        }
    }

    public void CheckReset()
    {
        //if (GS.GetComponent<GlobalStats>().CurrentDay == 1 ||
        //    GS.GetComponent<GlobalStats>().CurrentDay == 7 ||
        //    GS.GetComponent<GlobalStats>().CurrentDay == 14)
        //{
        //    GS.GetComponent<QuestController>().ResetQuests();
        //    GS.GetComponent<ItemController>().ResetItems();
        //}

       

        if(GS.GetComponent<GlobalStats>().CurrentDay == 1)
        {
            Debug.Log("Part 1 ");
            GS.GetComponent<QuestController>().ResetQuests();
            GS.GetComponent<ItemController>().ResetItems();
        }
        else
        {
            if (GS.GetComponent<GlobalStats>().CurrentDay > 7 && GS.GetComponent<GlobalStats>().CurrentDay <= 14)
            {
                if (GS.GetComponent<QuestController>().Part == 1)
                {
                    Debug.Log("Part 2 ");
                    GS.GetComponent<QuestController>().ResetQuests();
                    GS.GetComponent<ItemController>().ResetItems();
                }
            }
            else if(GS.GetComponent<GlobalStats>().CurrentDay > 14) 
            {                
               if(GS.GetComponent<QuestController>().Part == 2)
                {
                    Debug.Log("Part 3 ");
                    GS.GetComponent<QuestController>().ResetQuests();
                    GS.GetComponent<ItemController>().ResetItems();
                }
            }
            else
            {
                Debug.Log("bruh");
            }
        }

        
    }

    public void CloseCustoScreen()
    {
        GS.ShownCustoScreen = false;
        CustomersLostScrn.SetActive(false);
    }

    public void StatsClicked()
    {
        StatsOpen = !StatsOpen;
    }

    void CheckCross()
    {

        for(int i = 0; i < GS.CurrentDay-1; i++)
        {
            CalendarDays[i].transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        StatsScrn.SetActive(StatsOpen);

        TSalary.text = "Total Earnings - P" + GS.TotalSalaryEarned.ToString("F2");
        TBurgers.text = "Total Burgers Sold - " + GS.TotalAmtOfBurgers;
        TCustomers.text = "Total Customers Lost - " + GS.TotalAmtCustoLost;
    }
}
