﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalendarDisplayQuest : MonoBehaviour
{
    QuestController QC;
    public GameObject QuestObjPrefab;

    public GameObject QuestLoc1, QuestLoc2, QuestLoc3, QuestLoc4;
    // Start is called before the first frame update
    void Start()
    {
        QC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<QuestController>();
        
        SpawnQuests();
        
    }

    void SpawnQuests()
    {
        switch (QC.QuestsToDisplay.Count)
        {
            case 1:
                GameObject x1 = Instantiate(QuestObjPrefab, QuestLoc1.transform);               
                x1.GetComponent<QuestDisplayObj>().AssignData(QC.QuestsToDisplay[0]);

                if(QC.QuestDone1)
                {
                    x1.GetComponent<QuestDisplayObj>().IsDone = true;
                }
                else
                {
                    //todo doesnt show
                    switch (QC.Part)
                    {
                        case 1:
                            if(QC.QuestsToDisplay[0].GetComponent<QuestObject>().DueDate < GlobalStats.instance.CurrentDay)
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = true;
                            }
                            else
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = false;
                            }
                            break;

                        case 2:
                            if (QC.QuestsToDisplay[0].GetComponent<QuestObject>().DueDate < GlobalStats.instance.CurrentDay)
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = true;
                            }
                            else
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = false;
                            }
                            break;

                        case 3:
                            if (QC.QuestsToDisplay[0].GetComponent<QuestObject>().DueDate < GlobalStats.instance.CurrentDay)
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = true;
                            }
                            else
                            {
                                x1.GetComponent<QuestDisplayObj>().HasFailed = false;
                            }
                            break;
                    }
                }
                Debug.Log("Part " + QC.Part + " //// " + x1.GetComponent<QuestDisplayObj>().HasFailed);
                break;

            case 2:
                GameObject x2 = Instantiate(QuestObjPrefab, QuestLoc2.transform);
                x2.GetComponent<QuestDisplayObj>().AssignData(QC.QuestsToDisplay[1]);
                x2.GetComponent<QuestDisplayObj>().IsDone = QC.QuestDone2;
                goto case 1;

            case 3:
                GameObject x3 = Instantiate(QuestObjPrefab, QuestLoc3.transform);
                x3.GetComponent<QuestDisplayObj>().AssignData(QC.QuestsToDisplay[2]);
                x3.GetComponent<QuestDisplayObj>().IsDone = QC.QuestDone3;
                goto case 2;

            case 4:
                GameObject x4 = Instantiate(QuestObjPrefab, QuestLoc4.transform);
                x4.GetComponent<QuestDisplayObj>().AssignData(QC.QuestsToDisplay[3]);
                x4.GetComponent<QuestDisplayObj>().IsDone = QC.QuestDone4;
                goto case 3;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
