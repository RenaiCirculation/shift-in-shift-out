﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDisplayObj : MonoBehaviour
{
    public Text Description, DueDate;

    public Image BGImage,DoneImg;
    public bool IsDone;

    public GameObject FailedObj;
    public bool HasFailed;

    GlobalStats GS;
    GameObject QuestObject;
    // Start is called before the first frame update  

    public void AssignData(GameObject QObject)
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        QuestObject = QObject;
       
        switch (QuestObject.GetComponent<QuestObject>().ThisQType)
        {
            case global::QuestObject.QType.Main:
                BGImage.color = Color.red;
                break;

            case global::QuestObject.QType.Side:
                BGImage.color = Color.yellow;
                break;
        }

        switch (QuestObject.GetComponent<QuestObject>().ThisReqType)
        {
            case global::QuestObject.ReqType.ReqMoney:
                Description.text = "Money Req: P " + QuestObject.GetComponent<QuestObject>().ReqAmtNeeded.ToString();
                break;

            case global::QuestObject.ReqType.ReqHamburger:
                Description.text = "Burgers Req: " + GS.TotalAmtOfBurgers.ToString() + "/" + QuestObject.GetComponent<QuestObject>().ReqAmtNeeded.ToString();
                break;

            case global::QuestObject.ReqType.ReqSalary:
                Description.text = "Salary Earned Req: " + GS.TotalSalaryEarned.ToString() + "/" + QuestObject.GetComponent<QuestObject>().ReqAmtNeeded.ToString();
                break;
        }

        DueDate.text = "Day: " + QuestObject.GetComponent<QuestObject>().DueDate.ToString();
        

    }

    // Update is called once per frame
    void Update()
    {
        DoneImg.gameObject.SetActive(IsDone);
        FailedObj.SetActive(HasFailed);
    }


}
