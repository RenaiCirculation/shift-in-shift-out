﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bills : MonoBehaviour
{
    GlobalStats GS;
    public Text SavingsUI, SalaryUI, BonusUI, BillsamtUI, CurrentCashUI;
    public float BillAmt;

    float TotalAmt;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        SavingsUI.text = "₱ :" + GS.CurrentEarnings.ToString("F2");
        SalaryUI.text = "₱ :" + GS.thisDayEarnings.ToString("F2");

        BonusUI.text = "₱ :" + GS.thisDayBonus.ToString("F2");
        BillsamtUI.text = "₱ :-" + BillAmt.ToString("F2");

        TotalAmt = 0;
        ComputeTotal();
        CurrentCashUI.text = "₱ :" + TotalAmt.ToString("F2");

    }
    void ComputeTotal()
    {
        TotalAmt = GS.CurrentEarnings + GS.thisDayEarnings + GS.thisDayBonus + (-BillAmt);
        GS.CurrentEarnings = TotalAmt;
    }
    void Update()
    {
        
    }
}
