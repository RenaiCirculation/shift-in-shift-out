﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoodBoard : MonoBehaviour
{
    public Image MoodFace,MoodBar;
    public Sprite Face1, Face2, Face3, Face4, Face5;

    PlayerStats PS;
    GlobalStats GS;

    public Text Days;

    public GameObject Overtime, Break;

    // Start is called before the first frame update
    void Start()
    {
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        Overtime.SetActive(false);
        Break.SetActive(false);

        if(GS.IsOnOvertime)
        {
            Overtime.SetActive(true);
            PS.HappinessLevel -= 15;
        }

        if(GS.IsOnBreak)
        {
            Break.SetActive(true);
            PS.HappinessLevel += 20;
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoodBar.fillAmount = PS.HappinessLevel / 100;
        Days.text = "Day's until promotion :" + (GS.EndGameDay - GS.CurrentDay).ToString();

        if (PS.HappinessLevel > 80)
        {
            MoodFace.sprite = Face1;
        }
        else if (PS.HappinessLevel > 60 && PS.HappinessLevel <= 80)
        {
            MoodFace.sprite = Face2;
        }
        else if (PS.HappinessLevel > 40 && PS.HappinessLevel <= 60)
        {
            MoodFace.sprite = Face3;
        }

        else if (PS.HappinessLevel > 20 && PS.HappinessLevel <= 40)
        {
            MoodFace.sprite = Face4;
        }
        else
        {
            MoodFace.sprite = Face5;
        }
    }
}
