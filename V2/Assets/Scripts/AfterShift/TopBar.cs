﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopBar : MonoBehaviour
{
    public Text DayUI;
    public Text CurrentCashUI;

    GlobalStats SH;
    // Start is called before the first frame update
    void Start()
    {
        SH = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
    }

    // Update is called once per frame
    void Update()
    {
        DayUI.text = "Day :" + SH.CurrentDay;
        if(SH.CurrentEarnings > 0)
        {
            CurrentCashUI.color = Color.green;
        }
        else
        {
            CurrentCashUI.color = Color.red;
        }
        CurrentCashUI.text = "P: " + SH.CurrentEarnings;
    }

}
