﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessMeter : MonoBehaviour
{
    public Image HappinessBar;
    float happinessLevel;

    PlayerStats PS;

    public Image IndicatorUI;
    float IndicatorTimer;
    bool indicatorOpen;   

    public Sprite Blank,Plus, Minus, PlusTwo, MinusTwo;

    public Image FaceImg;
    public Sprite Face1, Face2, Face3, Face4, Face5;
    // Start is called before the first frame update
    void Start()
    {
        IndicatorTimer = 0;
        indicatorOpen = false;
        IndicatorUI.sprite = Blank;
        

        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
       
    }

    // Update is called once per frame
    void Update()
    {
        //HappinessBar.fillAmount = happinessLevel / 100;
        happinessLevel = PS.HappinessLevel;
        HappinessBar.fillAmount = happinessLevel / PS.MaxHappiness;
        UpdateMood();

        if (happinessLevel != PS.HappinessLevel)
        {
            
            HappinessBar.fillAmount = Mathf.Lerp(HappinessBar.fillAmount, PS.HappinessLevel / 100, 4 * Time.deltaTime);
        }
        else
        {
            
            PS.HappinessLevel = happinessLevel;
        } 

        if(indicatorOpen)
        {
            if(IndicatorTimer < 3)
            {
                IndicatorTimer += Time.deltaTime;
            }
            else
            {
                indicatorOpen = false;
                IndicatorTimer = 0;
                IndicatorUI.sprite = Blank;
            }
            
        }        

        
    }

    void UpdateMood()
    {
        if (PS.HappinessLevel > 80)
        {
            FaceImg.sprite = Face1;
        }
        else if (PS.HappinessLevel > 60 && PS.HappinessLevel <= 80)
        {
            FaceImg.sprite = Face2;
        }
        else if (PS.HappinessLevel > 40 && PS.HappinessLevel <= 60)
        {
            FaceImg.sprite = Face3;
        }

        else if (PS.HappinessLevel > 20 && PS.HappinessLevel <= 40)
        {
            FaceImg.sprite = Face4;
        }
        else
        {
            FaceImg.sprite = Face5;
        }
    }

    void DisplayIndicator(Sprite s)
    {
        IndicatorUI.sprite = s;
        indicatorOpen = true;
    }

    public void DecreaseHappinessBar(float amt)
    {
        Debug.Log("Decrease");
        PS.HappinessLevel -= amt;
        DisplayIndicator(Minus);
    }

    public void IncreaseHappinessBar(float amt)
    {
        Debug.Log("Increase");
        PS.HappinessLevel += amt;        
        DisplayIndicator(Plus);
    }

}
