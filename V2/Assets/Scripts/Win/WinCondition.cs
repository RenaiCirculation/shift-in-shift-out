﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinCondition : MonoBehaviour
{
    public Image Medals;
    public Sprite Plat, Gold, Silver, Bronze;

    public Text DecriptionUI;
    public Text BurgersUI, SalaryUI,CustoLostUI;

    GlobalStats GS;

    enum Type
    {
        Platinum,
        Gold,
        Silver,
        Bronze
    }
    Type thisType;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        thisType = Type.Bronze;
        AssignTrophy();
    }

    // Update is called once per frame
    void Update()
    {
        BurgersUI.text = "Total Burgers served: " + GS.TotalAmtOfBurgers.ToString();
        SalaryUI.text = "Total Salary Earned: P" + GS.TotalSalaryEarned.ToString();
        CustoLostUI.text = "Total Customers Lost: " + GS.TotalAmtCustoLost.ToString();

        switch (thisType)
        {
            case Type.Platinum:
                Medals.sprite = Plat;
                DecriptionUI.text = "You got a Platinum trophy";
                break;

            case Type.Gold:
                Medals.sprite = Gold;
                DecriptionUI.text = "You got a Gold trophy";
                break;

            case Type.Silver:
                Medals.sprite = Silver;
                DecriptionUI.text = "You got a Silver trophy";
                break;

            case Type.Bronze:
                Medals.sprite = Bronze;
                DecriptionUI.text = "You got a Bronze trophy";
                break;
        }

    }

    void AssignTrophy()
    {
        if(GS.TotalAmtOfBurgers > 350)
        {
            thisType = Type.Platinum;
        }
        else if (GS.TotalAmtOfBurgers <= 350 && GS.TotalAmtOfBurgers > 300)
        {
            thisType = Type.Gold;
        }
        else if(GS.TotalAmtOfBurgers <= 300 && GS.TotalAmtOfBurgers > 250)
        {
            thisType = Type.Silver;
        }
        else
        {
            thisType = Type.Bronze; ;
        }
    }

    public void Return()
    {
        GetComponent<GlobalUI>().LoadScene("CreditsScene");
        //GetComponent<GlobalUI>().LoadScene("MainMenu");
    }
}
