﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsController : MonoBehaviour
{
    public enum EffectToday
    {
        None,
        Rush,
        Robbery
    }
    public EffectToday curEvent;

    public GameObject WarningSign;

    public GameObject Spawn1, Spawn2;

    public int CustomersToSpawn;
    int customersSpawned;

    ShiftHandler SH;

    bool startedRush;
    public float RushMaxTimer;
    float rushTimer;
    CustomerSpawner CS;

    public GameObject MuggerPrefab;
    bool spawnedMugger;

    public AudioClip AlertSFX;

    // Start is called before the first frame update
    void Start()
    {
        SH = GetComponent<ShiftHandler>();
        CS = GetComponent<CustomerSpawner>();
        WarningSign.SetActive(false);

        customersSpawned = 0;
        startedRush = false;

        spawnedMugger = false;

        AssignRandomEvent();
    }

    void AssignRandomEvent()
    {
        int x = Random.Range(0, 100);

        if(x > 50)
        {
            curEvent = EffectToday.None;
        }

        else if(x > 20 && x <= 50)
        {
            curEvent = EffectToday.Rush;
        }

        else
        {
            curEvent = EffectToday.Robbery;
        }
    }

    void CheckEvent()
    {
        switch (curEvent)
        {
            case EffectToday.None:
                // do nothing
                break;

            case EffectToday.Rush:
                Rush();
                break;

            case EffectToday.Robbery:
                Rob();
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(SH.startedShift)
        {
            CheckEvent();
        }
        
    }

    void Rush()
    {
        if (SH.internalTimer < SH.ShiftTime / 2)
        {            
            WarningSign.SetActive(true);
            if (customersSpawned < CustomersToSpawn)
            {
                if (!startedRush)
                {
                    startedRush = true;
                    GameObject.FindGameObjectWithTag("SoundH").
                        GetComponent<SoundHandler>().PlayAudioClip(AlertSFX);
                }
                else
                {
                    if (rushTimer < RushMaxTimer)
                    {
                        rushTimer += Time.deltaTime;
                    }
                    else
                    {
                        rushTimer = 0;
                        int x = Random.Range(0, 50);
                        if (x > 25)
                        {
                            Instantiate(CS.GetRandomCustomerFromList(), Spawn1.transform.position, Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(CS.GetRandomCustomerFromList(), Spawn2.transform.position, Quaternion.identity);
                        }
                        customersSpawned++;
                    }
                }
            }
        }
    }

    void Rob()
    {
        if (SH.internalTimer < SH.ShiftTime / 2)
        {
            if(!spawnedMugger)
            {
                Debug.Log("Spawned");
                Instantiate(MuggerPrefab, Spawn1.transform.position, Quaternion.identity);
                spawnedMugger = true;
                
            }
        }
    }
}
