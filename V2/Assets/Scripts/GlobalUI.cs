﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadScene(string scene)
    {
        //SceneManager.LoadScene(scene);
        LoadingScreen.Instance.Show(SceneManager.LoadSceneAsync(scene));
    }

    public void CheckWinLose(PlayerStats PS)
    {
        if(PS.HappinessLevel <= 0)
        {
            SceneManager.LoadScene("LoseScene");
        }
        else
        {
            SceneManager.LoadScene("7_Event");
        }
    }

    public void SetShiftScene()
    {
        GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>().AssignShiftTime();
        GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>().AssignWeather();
    }

    public void CalendarCheckFail()
    {
        GlobalStats GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        PlayerStats PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        QuestController QC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<QuestController>();
        SetShiftScene();

        if (GS.TotalAmtCustoLost >= 15)
        {
            LoadScene("FiredScene");
        }
        else
        {
            if (GS.CurrentEarnings < -1500)
            {
                LoadScene("BrokeScene");
                
            }
            else
            {
                if (PS.HappinessLevel <= 0)
                {
                    LoadScene("LoseScene");
                }
                else
                {
                    if (GS.CurrentDay >= GS.EndGameDay)
                    {
                        LoadScene("WinScene");
                    }
                    else
                    {                        
                        if (QC.HasMainFailed())
                        {
                            LoadScene("QuestFailedScene");
                        }
                        else
                        {
                            GameObject.Find("Canvas").GetComponent<Calender>().CheckReset();
                            LoadScene("4_Shift");
                        }
                    }

                }
            }
        }

        Debug.Log("poo poo pee pee");
    }

    public void TutorialNext()
    {
        LoadScene("4_ShiftTutorial");
    }

    public void StartMainGame()
    {
        GlobalStats.instance.StartValues();
        LoadScene("3_Calendar");
    }

    public void StartTutorial()
    {
        GlobalStats.instance.StartTutorialValues();
        LoadScene("3_CalendarTutorial");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
