﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetLight : MonoBehaviour
{
    bool LightOn = false;

    GameObject Bulb;
    float LightOffIntensity = -1f;
    float LightOnIntensity = 3f;

    public List<Material> LightMats = new List<Material>();

    GlobalStats GS;
    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if(LightOn)
        {
            OpenLight();
        }
        else
        {
            CloseLight();
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            LightOn = !LightOn;
        }
    }

    void OpenLight()
    {
        foreach(Material mat in LightMats)
        {
            mat.SetColor("_EmissionColor", new Vector4(mat.color.r, mat.color.g, mat.color.b, 0) * LightOnIntensity);
        }
    }

    void CloseLight()
    {
        foreach (Material mat in LightMats)
        {
            mat.SetColor("_EmissionColor", new Vector4(mat.color.r, mat.color.g, mat.color.b, 0) * LightOffIntensity);
        }
    }
}
