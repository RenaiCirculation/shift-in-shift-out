﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftSound : MonoBehaviour
{
    AudioSource AS;
    ShiftHandler SH;
    GlobalStats GS;

    public AudioClip DayAmbient, NightAmbient, RainAmbient, StormAmbient;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        AS = GetComponent<AudioSource>();
        SH = GameObject.FindGameObjectWithTag("GameController").GetComponent<ShiftHandler>();

        PlayAmbientSounds();
    }

    void PlayAmbientSounds()
    {
        AS.loop = true;
        AS.volume = .5f;
        switch (GS.CurrentWeather)
        {
            case GlobalStats.Weather.NoModifier:
                if (GS.CurrentShiftTime == GlobalStats.ShiftTime.Night)
                {
                    AS.clip = NightAmbient;

                }
                else
                {
                    AS.clip = DayAmbient;
                }
                break;

            case GlobalStats.Weather.Overcast:
                goto case GlobalStats.Weather.NoModifier;

            case GlobalStats.Weather.Rain:
                AS.clip = RainAmbient;
                break;

            case GlobalStats.Weather.Storm:
                AS.clip = StormAmbient;
                break;
        }
        AS.Play();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
