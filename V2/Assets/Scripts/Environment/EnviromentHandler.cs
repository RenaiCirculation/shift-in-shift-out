﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentHandler : MonoBehaviour
{
    public Material DaySkybox, NightSkybox, MorningSkybox;

    public GameObject Sunlight, Moonlight, Morning;
    public GameObject RainParticles;
    Light curLight;

    GlobalStats GS;

    // Start is called before the first frame update
    void Start()
    {
        Sunlight.SetActive(false);
        Moonlight.SetActive(false);
        Morning.SetActive(false);
        RainParticles.SetActive(false);
        curLight = null;

        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();

        switch (GS.CurrentShiftTime)
        {
            case GlobalStats.ShiftTime.Morning:
                RenderSettings.skybox = MorningSkybox;
                Morning.SetActive(true);
                curLight = Morning.GetComponent<Light>();
                break;

            case GlobalStats.ShiftTime.Night:
                RenderSettings.skybox = NightSkybox;
                Moonlight.SetActive(true);
                curLight = Moonlight.GetComponent<Light>();
                break;

            case GlobalStats.ShiftTime.Noon:
                RenderSettings.skybox = DaySkybox;
                Sunlight.SetActive(true);
                curLight = Sunlight.GetComponent<Light>();
                break;
        }

        switch (GS.CurrentWeather)
        {
            case GlobalStats.Weather.NoModifier:
                //do nothing
                break;

            case GlobalStats.Weather.Overcast:
                curLight.intensity = curLight.intensity - .2f;
                break;

            case GlobalStats.Weather.Rain:
                curLight.intensity = curLight.intensity - .45f;
                RainParticles.SetActive(true);
                break;

            case GlobalStats.Weather.Storm:
                curLight.intensity = curLight.intensity - .65f;
                RainParticles.SetActive(true);
                break;
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
