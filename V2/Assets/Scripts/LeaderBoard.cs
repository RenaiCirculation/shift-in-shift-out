﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoard : MonoBehaviour
{
    public GameObject LeaderboardMenu;
    bool IsOpen;

    // Start is called before the first frame update
    void Start()
    {
        IsOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
        LeaderboardMenu.SetActive(IsOpen);
    }

    public void Activate()
    {
        IsOpen = !IsOpen;
    }
}
