﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public AudioClip SelectSFX, ClickedSFX;
    public GameObject PM;
    bool IsOpen;

    public GameObject DebugMenu;
    bool DebugOpen;

    public GameObject MMmenu;
    bool MMOpen;

    public Text MusicName;  
    // Start is called before the first frame update
    void Start()
    {
        IsOpen = false;
        DebugOpen = false;
        MMOpen = false;
    }

    // Update is called once per frame
    void Update()
    {
        PM.SetActive(IsOpen);
        DebugMenu.SetActive(DebugOpen);
        MMmenu.SetActive(MMOpen);

        if (IsOpen)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Activate();
        }

        MusicName.text = MusicController.instance.CurrentPlaying();
    }

    public void NextTrack()
    {
        MusicController.instance.NextTrack();
    }

    public void Activate()
    {
        IsOpen = !IsOpen;
    }

    public void RestartScene()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        //LoadingScreen.Instance.Show(SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex));
    }

    public void Debug()
    {
        DebugOpen = !DebugOpen;
    }

    public void MainMenu()
    {
        MMOpen = !MMOpen;
    }

    public void ReturnToMM()
    {
        LoadingScreen.Instance.Show(SceneManager.LoadSceneAsync("MainMenu"));
    }

    public void PlaySelectSFX()
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayAudioClip(SelectSFX);
    }

    public void PlayClickedSFX()
    {
        GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayAudioClip(ClickedSFX);
    }

}
