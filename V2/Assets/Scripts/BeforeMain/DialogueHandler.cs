﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueHandler : MonoBehaviour
{
    public GameObject Text1, Text2, Text3, Text4;
    public GameObject ApplyButton;

    // Start is called before the first frame update
    void Start()
    {
        Text1.SetActive(true);
        Text1.GetComponent<DialogueController>().StartText();


        Text2.SetActive(false);
        Text3.SetActive(false);
        Text4.SetActive(false);
        ApplyButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Text1.GetComponent<DialogueController>().IsDoneDisplay)
        {
            Text2.SetActive(true);
            Text2.GetComponent<DialogueController>().StartText();
        }

        if (Text2.GetComponent<DialogueController>().IsDoneDisplay)
        {
            Text3.SetActive(true);
            Text3.GetComponent<DialogueController>().StartText();
        }

        if (Text3.GetComponent<DialogueController>().IsDoneDisplay)
        {
            ApplyButton.SetActive(true);
            Text4.SetActive(true);
            Text4.GetComponent<DialogueController>().StartText();
        }
    }
    
}
