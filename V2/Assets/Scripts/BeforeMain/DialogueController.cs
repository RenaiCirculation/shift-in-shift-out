﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    public float TextSpeed;
    public string TextToDisplay;
    string currentText = "";

    public bool IsDoneDisplay;
    bool started;
    IEnumerator showText;

    // Start is called before the first frame update
    void Start()
    {
        showText = ShowText();
        started = false;


    }

    public void StartText()
    {
        if(!started)
        {
            StartCoroutine(ShowText());
            IsDoneDisplay = false;
            started = true; 
        }        
    }

    private void Update()
    {
        if (!IsDoneDisplay)
        {
            if (currentText == TextToDisplay)
            {
                IsDoneDisplay = true;
                StopCoroutine(showText);
                Debug.Log("Done Displaying");
            }
        }
    }
    IEnumerator ShowText()
    {
        for(int i = 0; i <= TextToDisplay.Length; i++)
        {   
            currentText = TextToDisplay.Substring(0, i);
            this.GetComponent<Text>().text = currentText;
            GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>().PlayClick();
            yield return new WaitForSeconds(TextSpeed);
        }
    }

}
