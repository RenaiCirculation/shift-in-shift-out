﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BeforeMainController : MonoBehaviour
{
    int currentScreen;
    public GameObject Screen1,Screen2,Screen3,Screen4, Screen5, Screen6;

    // Start is called before the first frame update
    void Start()
    {
        currentScreen = 1;
        Screen1.SetActive(true);
        Screen2.SetActive(false);
        Screen3.SetActive(false);
        Screen4.SetActive(false);
        Screen5.SetActive(false);
        Screen6.SetActive(false);
        UpdateScreens();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateScreens()
    {
        switch (currentScreen)
        {
            case 1:
                Screen1.SetActive(true);
                break;
            case 2:
                Screen1.SetActive(false);
                Screen2.SetActive(true);
                break;
            case 3:                
                Screen2.SetActive(false);
                Screen3.SetActive(true);
                break;
            case 4:
                Screen3.SetActive(false);
                Screen4.SetActive(true);
                break;
            case 5:
                Screen4.SetActive(false);
                Screen5.SetActive(true);
                break;
            case 6:
                Screen5.SetActive(false);
                Screen6.SetActive(true);
                break;

        }
    }

    public void ApplyButton()
    {
        Debug.Log("clicked");
        currentScreen += 1;
        UpdateScreens();
    }

    public void OpenTutorial()
    {
        SceneManager.LoadScene("JobSceneTutorial");
    }
}
