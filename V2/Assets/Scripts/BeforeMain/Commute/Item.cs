﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public Button BuyButton;
    bool isBought;

    public Text CostUI;
    public float CostAmt;

    public Text NameUI;
    public string NameofItem;

    public Text EffectUI;
    public float HappinessAmt;

    PlayerStats PS;
    GlobalStats GS;

    // Start is called before the first frame update
    void Start()
    {
        isBought = false;

        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
    }

    // Update is called once per frame
    void Update()
    {
        CostUI.text = "₱ :" + CostAmt.ToString("F2");
        NameUI.text = NameofItem;
        EffectUI.text = "+" + HappinessAmt + " mood";

        if(CostAmt > GS.CurrentEarnings)
        {
            BuyButton.interactable = false;
        }
        else
        {
            BuyButton.interactable = true;
        }

        if (isBought)
        {
            BuyButton.interactable = false;
        }
    }

    public void BuyItem()
    {
        isBought = true;
        PS.HappinessLevel += HappinessAmt;
        GS.CurrentEarnings -= CostAmt;
    }
}
