﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShiftConditions : MonoBehaviour
{
    public Text TimeUI;

    public Image WeatherIcon;
    public Sprite Stormy, Rain, Overcast, Day, Night;
    public Text WeatherTextUI,WeatherDescription;

    GlobalStats GS;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        GS.AssignShiftTime();
        TimeUI.text = GS.CurrentShiftTime.ToString();
        GS.AssignWeather();

        switch (GS.CurrentWeather)
        {
            case GlobalStats.Weather.NoModifier:
                if(GS.CurrentShiftTime == GlobalStats.ShiftTime.Night)
                {
                    WeatherIcon.sprite = Night;
                    WeatherTextUI.text = "Clear night";
                    WeatherDescription.text = "No modifiers";
                }
                else
                {
                    WeatherIcon.sprite = Day;
                    WeatherTextUI.text = "Sunny";
                    WeatherDescription.text = "No modifiers";
                }
                break;

            case GlobalStats.Weather.Overcast:
                WeatherIcon.sprite = Overcast;
                WeatherTextUI.text = "Cloudy";
                WeatherDescription.text = "More people will want to buy";
                break;

            case GlobalStats.Weather.Rain:
                WeatherIcon.sprite = Rain;
                WeatherTextUI.text = "Rain";
                WeatherDescription.text = "Fewer people";
                break;

            case GlobalStats.Weather.Storm:
                WeatherIcon.sprite = Stormy;
                WeatherTextUI.text = "Stormy";
                WeatherDescription.text = "Even less people";
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
