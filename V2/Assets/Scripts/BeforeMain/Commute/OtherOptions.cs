﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OtherOptions : MonoBehaviour
{
    public Button OvertimeButton, BreakButton;
    public GameObject OvertimeDisplay;

    public GameObject ConfirmationMenu,OvertimeMenu,BreakMenu;
    GlobalStats GS;
    GlobalUI GU;

    // Start is called before the first frame update
    void Start()
    {
        GU = GetComponent<GlobalUI>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        ConfirmationMenu.SetActive(false);
        OvertimeMenu.SetActive(false);
        BreakMenu.SetActive(false);
        OvertimeButton.interactable = true;
        BreakButton.interactable = true;

        OvertimeDisplay.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(GS.IsOnOvertime)
        {
            OvertimeDisplay.SetActive(true);
        }

        if(ConfirmationMenu.activeInHierarchy)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                ConfirmationMenu.SetActive(false);
            }
        }
    }

    public void Overtime()
    {
        ConfirmationMenu.SetActive(true);
        OvertimeMenu.SetActive(true);
        BreakMenu.SetActive(false);
    }

    public void BreakTime()
    {
        ConfirmationMenu.SetActive(true);
        OvertimeMenu.SetActive(false);
        BreakMenu.SetActive(true);
    }
    public void OvertimeYes()
    {
        GS.IsOnOvertime = true;
        OvertimeButton.interactable = false;
        BreakButton.interactable = false;

        
        ConfirmationMenu.SetActive(false);
        OvertimeMenu.SetActive(false);
        BreakMenu.SetActive(false);
    }

    public void BreakYes(string scene)
    {
        GU.LoadScene(scene);

        GS.AmtofBreaks += 1;
        GS.IsOnBreak = true;
        ConfirmationMenu.SetActive(false);
        OvertimeMenu.SetActive(false);
        BreakMenu.SetActive(false);
    }

    public void No()
    {
        ConfirmationMenu.SetActive(false);
        OvertimeMenu.SetActive(false);
        BreakMenu.SetActive(false);
    }

    public void GotoWork()
    {
        Debug.Log(GS.AmtofBreaks);
        if(GS.AmtofBreaks < 3)
        {
            GU.LoadScene("5_ShiftScene");
        }
        else
        {
            GU.LoadScene("FiredScene");
        }
    }
}
