﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHappinessAmt : MonoBehaviour
{
    PlayerStats PS;

    // Start is called before the first frame update
    void Start()
    {
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = PS.HappinessLevel.ToString() +"/"+ PS.MaxHappiness.ToString();
    }
}
