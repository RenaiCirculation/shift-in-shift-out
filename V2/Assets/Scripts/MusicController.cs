﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public static MusicController instance;
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    AudioSource AS;

    public List<AudioClip> BGM = new List<AudioClip>();
    public int trackPlaying;

    public bool Playing;

    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
        AS.clip = BGM[0];
        trackPlaying = 0;
        AS.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(!AS.isPlaying)
        {
            Next();
        }
    }

    public string CurrentPlaying()
    {
        return AS.clip.name;
    }

    public void Next()
    {
        if (trackPlaying < BGM.Count - 1)
        {
            trackPlaying++;
        }
        else
        {
            trackPlaying = 0;
        }
        AS.clip = BGM[trackPlaying];
        AS.Play();
    }

    public void NextTrack()
    {        
        AS.Stop();
        if(trackPlaying < BGM.Count - 1)
        {
            trackPlaying++;            
        }
        else
        {
            trackPlaying = 0;
        }
        AS.clip = BGM[trackPlaying];       
        AS.Play();
    }
}
