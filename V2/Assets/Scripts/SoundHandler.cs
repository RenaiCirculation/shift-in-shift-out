﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundHandler : MonoBehaviour
{
    public AudioSource AS;
    float volume;

    public AudioClip ButtonHover, ButtonClicked;
    public AudioClip TypeClick;
    public AudioClip CorrectOrder, WrongOrder;
    public AudioClip WaitingSFX;
    public AudioClip EndMsgSFX, EndMsgRespSFX, EndMsgFadeSFX;
    public AudioClip CustomerArrive;

    public AudioClip GunCockSFX, GunShotSFX;
    public AudioClip GaspSFX;
    public AudioClip MailNotif;
    public AudioClip PhoneVibrate;
    public AudioClip ShiftDoneSFX;
  

  

    // Start is called before the first frame update
    void Awake()
    {
        AS = GetComponent<AudioSource>();
        volume = AS.volume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAudioClip(AudioClip x)
    {
        //if(volume != AS.volume)
        //{
        //    AS.volume = volume;
        //}
        AS.PlayOneShot(x);
    }

    public void PlayShiftDone()
    {
        AS.PlayOneShot(ShiftDoneSFX);
    }

    public void PlayClick()
    {
        AS.volume = .1f;
        AS.PlayOneShot(TypeClick);
    }

    public void CustomerArrived()
    {
        AS.PlayOneShot(CustomerArrive);
    }

    public void PlayCorrectOrder()
    {
        AS.PlayOneShot(CorrectOrder);
    }

    public void PlayWrongOrder()
    {
        AS.PlayOneShot(WrongOrder);
    }

    public void PlayWaitingMessage()
    {
        AS.PlayOneShot(WaitingSFX);
    }

    public void PlayerEndMessage()
    {
        AS.PlayOneShot(EndMsgSFX);
    }

    public void PlayerEndResp()
    {
        AS.PlayOneShot(EndMsgRespSFX);
    }

    public void PlayerEndNoAns()
    {
        AS.PlayOneShot(EndMsgFadeSFX);
    }
    
    public void GunCock()
    {
        AS.PlayOneShot(GunCockSFX);
    }

    public void GunShot()
    {
        AS.PlayOneShot(GunShotSFX);
    }

    public void PlayGasp()
    {
        AS.PlayOneShot(GaspSFX);
    }

    public void HasMail()
    {
        AS.PlayOneShot(MailNotif);
    }

    public void Vibrate()
    {       
        AS.PlayOneShot(PhoneVibrate);
    }
}