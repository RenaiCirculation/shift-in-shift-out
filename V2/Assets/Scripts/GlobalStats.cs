﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStats : MonoBehaviour
{
    public static GlobalStats instance;
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public float CurrentEarnings;
    public float thisDayEarnings;
    public float thisDayBonus;

    public int CurrentDay;
    public int EndGameDay;

    public enum ShiftTime
    {
        Morning,
        Noon,
        Night
    }
    public ShiftTime CurrentShiftTime;

    public enum Weather
    {
        NoModifier,
        Overcast,
        Rain,
        Storm
    }
    public Weather CurrentWeather;

    public bool IsOnOvertime;
    public bool IsOnBreak;
    public int AmtofBreaks;

    public float TotalSalaryEarned;
    public int TotalAmtOfBurgers;
    public int TotalAmtCustoLost;

    public bool ShownCustoScreen;

    public bool SkipDays;
    public int SkipDaysAmt;

    public bool CompletedMQ1;
    public bool CompletedMQ2;
    public bool CompletedMQ3;

    public bool ShowCongratsMsg1;
    public bool ShowCongratsMsg2;
    public bool ShowCongratsMsg3;

    // Start is called before the first frame update
    void Start()
    {
        //CurrentDay = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartValues()
    {
        CurrentEarnings = 1000;
        thisDayEarnings = 0;
        thisDayBonus = 0;

        AmtofBreaks = 0;
        IsOnOvertime = false;
        IsOnBreak = false;

        CurrentDay = 1;
        EndGameDay = 20;
        GetComponent<PlayerStats>().SetHappinessLvlStart();

        SkipDays = false;
        SkipDaysAmt = 0;

        TotalSalaryEarned = 0;
        TotalAmtOfBurgers = 0;
        TotalAmtCustoLost = 0;

        CompletedMQ1 = false;
        CompletedMQ2 = false;
        CompletedMQ3 = false;

        ShowCongratsMsg1 = false;
        ShowCongratsMsg2 = false;
        ShowCongratsMsg3 = false;

        GetComponent<ItemController>().UnboughtItems();
        GetComponent<QuestController>().UnQuests();

        ShownCustoScreen = false;
        
    }

    public void StartTutorialValues()
    {
        CurrentEarnings = 2000;
        thisDayEarnings = 0;
        thisDayBonus = 0;

        CurrentDay = 3;
        EndGameDay = 20;

        SkipDays = false;
        SkipDaysAmt = 0;

        TotalSalaryEarned = 0;
        TotalAmtOfBurgers = 0;
        TotalAmtCustoLost = 0;

        CompletedMQ1 = false;
        CompletedMQ2 = false;
        CompletedMQ3 = false;

        ShowCongratsMsg1 = false;
        ShowCongratsMsg2 = false;
        ShowCongratsMsg3 = false;

        GetComponent<ItemController>().UnboughtItems();
        GetComponent<QuestController>().UnQuests();

        ShownCustoScreen = false;
    }

    public void AdvanceDay()
    {
        Debug.Log("Advancing");
        if(SkipDays)
        {
            Debug.Log("Skipping " + SkipDaysAmt.ToString());
            CurrentDay += SkipDaysAmt;
            //SkipDaysAmt = 0;
            //SkipDays = false;
        }
        else
        {
            Debug.Log("Next day ");
            CurrentDay++;
        }

        IsOnOvertime = false;
        IsOnBreak = false;
        thisDayEarnings = 0;
        thisDayBonus = 0;

        if(ShownCustoScreen)
        {
            if(TotalAmtCustoLost <= 10)
            {
                ShownCustoScreen = false;
            }
        }
    }

    public void AssignShiftTime()
    {
        int x = Random.Range(0, 100);
        if(x > 66)
        {
            CurrentShiftTime = ShiftTime.Morning;
        }
        else if (x > 33 && x <=66)
        {
            CurrentShiftTime = ShiftTime.Noon;
        }
        else
        {
            CurrentShiftTime = ShiftTime.Night;
        }
    }

    public void AssignWeather()
    {
        if(CurrentShiftTime == ShiftTime.Night)
        {
            int x = Random.Range(0, 100);
            if(x > 30)
            {
                CurrentWeather = Weather.NoModifier;
            }
            else if (x<= 30 && x > 5)
            {
                CurrentWeather = Weather.Rain;
            }
            else
            {
                CurrentWeather = Weather.Storm;
            }
        }
        else
        {
            int x = Random.Range(0, 100);
            if (x > 50)
            {
                CurrentWeather = Weather.NoModifier;
            }
            else if (x > 20 && x <= 50)
            {
                CurrentWeather = Weather.Overcast;
            }
            else if (x > 10 && x <= 20)
            {
                CurrentWeather = Weather.Rain;
            }
            else
            {
                CurrentWeather = Weather.Storm;
            }
        }
    }
}
