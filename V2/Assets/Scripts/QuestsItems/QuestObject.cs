﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestObject : MonoBehaviour
{
    public GameObject DoneCover;
    public Image QuestBorder;

    public enum QType
    {
        Main,
        Side
    }

    public QType ThisQType;
    public Text TypeUI;

    public Text DescriptionUI;
    public string description;

    public Text DueDateUI;
    public int DueDate;

    public Text RequireUI;
    public string Requirement;    

    public enum ReqType
    {
        ReqMoney,
        ReqHamburger,
        ReqSalary
    }
    public ReqType ThisReqType;
    public int ReqAmtNeeded;

    public Text EffectUI;
    public string Effect;
    public int EffectAmt;

    public enum EffectType
    {
        PositiveHappiness,
        PositiveMoney,
    }
    public EffectType ThisEventType;

    public bool QuestFinished = false;
    public bool canTurnIn;
    public Button TurnInBtn;

    [Range(1,4)] public int CurHierarchy;

    GlobalStats GS;
    QuestItemsHandler QIH;

    QuestController QC;
    SoundHandler SH;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        QIH = GameObject.Find("Canvas").GetComponent<QuestItemsHandler>();
        QC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<QuestController>();
        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();

        DoneCover.SetActive(false);

        canTurnIn = false;
        TurnInBtn.interactable = false;

        TypeUI.text = ThisQType.ToString();
        DescriptionUI.text = description;
        DueDateUI.text = "Due: Day " + DueDate.ToString();
        RequireUI.text = Requirement;
        EffectUI.text = Effect;

        
    }

    void DisplayReq()
    {
        switch (ThisReqType)
        {
            case ReqType.ReqHamburger:
                RequireUI.text = GS.TotalAmtOfBurgers + " / " + Requirement;
                break;

            case ReqType.ReqSalary:
                RequireUI.text = GS.TotalSalaryEarned + " / " + Requirement;
                break;

            case ReqType.ReqMoney:
                RequireUI.text = GS.CurrentEarnings + " / " + Requirement;
                break;
        }

        switch (ThisQType)
        {
            case QType.Main:
                QuestBorder.color = Color.red;
                break;

            case QType.Side:
                QuestBorder.color = Color.yellow;
                break;
        }
    }

    public void PlayClip(AudioClip xd)
    {
        SH.PlayAudioClip(xd);
    }

    // Update is called once per frame
    void Update()
    {
        
        DisplayReq();
        CheckIfCanTurnIn();
        TurnInBtn.interactable = canTurnIn;
        DoneCover.SetActive(QuestFinished);        
    }

    public void CheckIfCanTurnIn()
    {
        if (DueDate < GS.CurrentDay)
        {
            if (!QuestFinished)
            {
                Debug.Log("fail");
                if(ThisQType == QType.Main)
                {
                    QIH.HasMainQFailed = true;
                }
                else
                {
                    QIH.HasMainQFailed = false;
                }
            }
            else
            {
                Debug.Log("Completed");
                QIH.HasMainQFailed = false;
            }
        }
        else
        {
            switch (ThisReqType)
            {
                case ReqType.ReqSalary:
                    if (GS.TotalSalaryEarned >= ReqAmtNeeded)
                    {
                        canTurnIn = true;
                    }
                    else
                    {
                        canTurnIn = false;
                    }
                    break;

                case ReqType.ReqHamburger:
                    if (GS.TotalAmtOfBurgers >= ReqAmtNeeded)
                    {
                        canTurnIn = true;
                    }
                    else
                    {
                        canTurnIn = false;
                    }
                    break;

                case ReqType.ReqMoney:
                    if (GS.CurrentEarnings >= ReqAmtNeeded)
                    {
                        canTurnIn = true;
                    }
                    else
                    {
                        canTurnIn = false;
                    }
                    break;
            }
        }
    }

    public void TurnInQuest()
    {
        if(ThisReqType == ReqType.ReqMoney)
        {
            GS.CurrentEarnings -= ReqAmtNeeded;
        }

        switch (ThisEventType)
        {
            case EffectType.PositiveHappiness:
                GS.gameObject.GetComponent<PlayerStats>().HappinessLevel += EffectAmt;
                break;

            case EffectType.PositiveMoney:
                GS.CurrentEarnings += EffectAmt;
                break;
        }

        if(ThisQType == QType.Main)
        {
            QC.MainQuestCompleted();
        }

        QuestFinished = true;
        QC.CompletedQuest(CurHierarchy);
    }
}
