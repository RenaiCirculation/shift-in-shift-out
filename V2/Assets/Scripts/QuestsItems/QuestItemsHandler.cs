﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestItemsHandler : MonoBehaviour
{
    QuestController QC;
    ItemController IC;
    GlobalStats GS;
    SoundHandler SH;

    GlobalUI Gui;

    public GameObject ItemSpawnLoc1, ItemSpawnLoc2, ItemSpawnLoc3;
    public GameObject QuestSpawnLoc1, QuestSpawnLoc2, QuestSpawnLoc3, QuestSpawnLoc4;

    public bool HasMainQFailed;

    public GameObject GoalsObj;
    

    public Text ResetText;
    public int DaysTilReset;

    bool PlayedSFX;
    // Start is called before the first frame update
    private void Awake()
    {
        HasMainQFailed = false;
        PlayedSFX = false;
    }

    void Start()
    {
        QC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<QuestController>();
        IC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<ItemController>();
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();
        Gui = GetComponent<GlobalUI>();
        SpawnItems();
        SpawnQuests();
    }

    // Update is called once per frame
    void Update()
    {
        if (QC.HasReset)
        {
            GoalsObj.SetActive(true);
            if(!PlayedSFX)
            {
                PlayedSFX = true;
                SH.Vibrate();
            }
        }
        else
        {
            PlayedSFX = false;
            GoalsObj.SetActive(false);
        }
        DisplayReset();
    }

    void DisplayReset()
    {
        int x = 0;
        switch (QC.Part)
        {
            case 1:
                x = 7 - GS.CurrentDay;
                break;

            case 2:
                x = 14 - GS.CurrentDay;
                break;

            case 3:
                x = 20 - GS.CurrentDay;
                break;
        }
        ResetText.text = "Reset in " + x.ToString() + " Days";
    }

    public void CloseWindow()
    {
        QC.HasReset = false;        
        GoalsObj.SetActive(false);
    }

    void SpawnQuests()
    {
        switch (QC.QuestsToDisplay.Count)
        {
            case 1:
                QC.QuestsToDisplay[0].GetComponent<QuestObject>().QuestFinished = QC.QuestDone1;
                GameObject x1 = Instantiate(QC.QuestsToDisplay[0], QuestSpawnLoc1.transform);
                x1.GetComponent<QuestObject>().CurHierarchy = 1;
                break;

            case 2:
                QC.QuestsToDisplay[1].GetComponent<QuestObject>().QuestFinished = QC.QuestDone2;
                GameObject x2 = Instantiate(QC.QuestsToDisplay[1], QuestSpawnLoc2.transform);
                x2.GetComponent<QuestObject>().CurHierarchy = 2;
                goto case 1;

            case 3:
                QC.QuestsToDisplay[2].GetComponent<QuestObject>().QuestFinished = QC.QuestDone3;
                GameObject x3 = Instantiate(QC.QuestsToDisplay[2], QuestSpawnLoc3.transform);
                x3.GetComponent<QuestObject>().CurHierarchy = 3;
                goto case 2;

            case 4:
                QC.QuestsToDisplay[3].GetComponent<QuestObject>().QuestFinished = QC.QuestDone4;
                GameObject x4 = Instantiate(QC.QuestsToDisplay[3], QuestSpawnLoc4.transform);
                x4.GetComponent<QuestObject>().CurHierarchy = 4;
                goto case 3;
        }

    }

    void SpawnItems()
    {
        switch(IC.ItemsToDisplay.Count)
        {
            case 1:
                IC.ItemsToDisplay[0].GetComponent<ItemObject>().CurNum = 1;
                IC.ItemsToDisplay[0].GetComponent<ItemObject>().hasBoughtItem = IC.HasBought1;
                Instantiate(IC.ItemsToDisplay[0], ItemSpawnLoc1.transform);               
                
                break;

            case 2:
                IC.ItemsToDisplay[1].GetComponent<ItemObject>().CurNum = 2;
                IC.ItemsToDisplay[1].GetComponent<ItemObject>().hasBoughtItem = IC.HasBought2;
                Instantiate(IC.ItemsToDisplay[1], ItemSpawnLoc2.transform);                

                goto case 1;

            case 3:
                IC.ItemsToDisplay[2].GetComponent<ItemObject>().CurNum = 3;
                IC.ItemsToDisplay[2].GetComponent<ItemObject>().hasBoughtItem = IC.HasBought3;
                Instantiate(IC.ItemsToDisplay[2], ItemSpawnLoc3.transform);               

                goto case 2;

        }       
    } 

    public void Advance(string scene)
    {
        
        if(HasMainQFailed)
        {
            //SceneManager.LoadScene( );
            //todo Quest Fail
            Gui.LoadScene("QuestFailedScene");
        }
        else
        {
            //SceneManager.LoadScene(scene);
            
            GS.AdvanceDay();
            Gui.LoadScene(scene);
        }
    }
}
