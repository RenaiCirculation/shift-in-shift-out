﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemObject : MonoBehaviour
{
    GlobalStats GS;
    PlayerStats PS;
    ItemController IC;
    HappinessMeter HM;

    public Image Icon;
    public Sprite IconToUse;

    public Text NameUI;
    public string NameString;

    public Text PriceUI;
    public float PriceAmt;

    public Text Effect1UI, Effect2UI;
    public string Effect1, Effect2;
    public int HappinessAmt, MaxHappinessAmt;

    public bool AddHappiness,AddMaxHappiness;

    public Button BuyBtn;
    public bool hasBoughtItem = false;

    public int CurNum;

    SoundHandler SH;

    public GameObject BoughtItemObj;
    public GameObject NotEnoughMoneyObj;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        PS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<PlayerStats>();
        IC = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<ItemController>();
        HM = GameObject.Find("Canvas").GetComponent<HappinessMeter>();

        SH = GameObject.FindGameObjectWithTag("SoundH").GetComponent<SoundHandler>();
        Icon.sprite = IconToUse;

        NameUI.text = NameString;
        PriceUI.text = "P " + PriceAmt.ToString();

        Effect1UI.text = Effect1;
        Effect2UI.text = Effect2;
        
        BoughtItemObj.SetActive(false);
        NotEnoughMoneyObj.SetActive(false);
    }

    public void PlayClip(AudioClip AC)
    {
        SH.PlayAudioClip(AC);
    }

    // Update is called once per frame
    void Update()
    {
        if (hasBoughtItem)
        {
            BuyBtn.interactable = false;
        }
        else
        {
            BuyBtn.interactable = true;
            if (GS.CurrentEarnings < PriceAmt)
            {
                BuyBtn.interactable = false;
                NotEnoughMoneyObj.SetActive(true);
            }
            else
            {
                NotEnoughMoneyObj.SetActive(false);                
            }
        }
        BoughtItemObj.SetActive(hasBoughtItem);


    }

    public void BuyItem()
    {
        if(PriceAmt <= GS.CurrentEarnings)
        {
            hasBoughtItem = true;
            switch (CurNum)
            {
                case 1:
                    IC.HasBought1 = true;
                    break;

                case 2:
                    IC.HasBought2 = true;
                    break;

                case 3:
                    IC.HasBought3 = true;
                    break;
            }

            GS.CurrentEarnings -= PriceAmt;

            if(AddHappiness)
            {
                HM.IncreaseHappinessBar(HappinessAmt);
            }

            if(AddMaxHappiness)
            {
                PS.IncreaseMaxHappiness(MaxHappinessAmt);
            }
        }
        
    }
}
