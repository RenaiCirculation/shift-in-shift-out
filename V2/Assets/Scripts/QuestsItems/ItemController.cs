﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public List<GameObject> ItemsToDisplay = new List<GameObject>();
    public List<GameObject> AllItems = new List<GameObject>();
    List<GameObject> Temp;

    public bool HasBought1, HasBought2, HasBought3;

    // Start is called before the first frame update
    void Start()
    {
        Temp = new List<GameObject>(AllItems);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UnboughtItems()
    {
        HasBought1 = false;
        HasBought2 = false;
        HasBought3 = false;

        ItemsToDisplay.Clear();
    }

    public void ResetItems()
    {
        Debug.Log("Reset Items");
        HasBought1 = false;
        HasBought2 = false;
        HasBought3 = false;

        ItemsToDisplay.Clear();
        Temp = new List<GameObject>(AllItems);
        for (int i = 0; i < 3; i++)
        {
            int x = Random.Range(0, Temp.Count);
            GameObject f = Temp[x];            
            ItemsToDisplay.Add(f);
            Temp.Remove(f); 
        }
    }
}   
