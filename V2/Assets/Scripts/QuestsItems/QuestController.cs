﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestController : MonoBehaviour
{
    public List<GameObject> QuestsToDisplay = new List<GameObject>();   

    public List<GameObject> QuestPart1 = new List<GameObject>();
    public List<GameObject> QuestPart2 = new List<GameObject>();
    public List<GameObject> QuestPart3 = new List<GameObject>();

    List<GameObject> Temp;

    public bool QuestDone1, QuestDone2, QuestDone3, QuestDone4;

    GlobalStats GS;

    public bool HasReset;

    [Range(1, 3)] public int Part;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        


    }

    // Update is called once per frame
    void Update()
    {       
    }

    public void UnQuests()
    {
        QuestDone1 = false;
        QuestDone2 = false;
        QuestDone3 = false;
        QuestDone4 = false;

        Part = 1;

        QuestsToDisplay.Clear();
    }


    public void ResetQuests()
    {
        QuestDone1 = false;
        QuestDone2 = false;
        QuestDone3 = false;
        QuestDone4 = false;

        HasReset = true;
        QuestsToDisplay.Clear();
        Debug.Log("Reset Quests");
        if (GS.CurrentDay < 7)
        {
            Part = 1;
            Temp = new List<GameObject>(QuestPart1);
        }
        else if(GS.CurrentDay >= 7 && GS.CurrentDay < 14)
        {
            Part = 2;
            Temp = new List<GameObject>(QuestPart2);
        }
        else
        {
            Part = 3;
            Temp = new List<GameObject>(QuestPart3);
        }

        GameObject main = null;
        foreach(GameObject g in Temp)
        {
            if(g.GetComponent<QuestObject>().ThisQType == QuestObject.QType.Main)
            {
                main = g;
                QuestsToDisplay.Add(g);                
                break;
            }
        }
        Temp.Remove(main);

        for (int i = 0; i < 3; i++)
        {
            int x = Random.Range(0, Temp.Count);
            GameObject f = Temp[x];
            QuestsToDisplay.Add(f);
            Temp.Remove(f);
        }
    }

    public void CompletedQuest(int x)
    {
        switch(x)
        {
            case 1:
                QuestDone1 = true;
                break;

            case 2:
                QuestDone2 = true;
                break;

            case 3:
                QuestDone3 = true;
                break;

            case 4:
                QuestDone4 = true;
                break;
        }
    }

    public void MainQuestCompleted()
    {
        GameObject.Find("Canvas").GetComponent<Mail>().HasMail = true;
        switch (Part)
        {
            case 1:
                GS.CompletedMQ1 = true;
                break;

            case 2:
                GS.CompletedMQ2 = true;
                break;

            case 3:
                GS.CompletedMQ3 = true;
                break;
        }

    }

    public bool HasMainFailed()
    {
        if (GS.CurrentDay > 1)
        {
            if(!QuestDone1)
            {
                if (QuestsToDisplay[0].GetComponent<QuestObject>().DueDate < GS.CurrentDay)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
