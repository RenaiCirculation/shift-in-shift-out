﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mail : MonoBehaviour
{
    public bool HasMail;


    public Image MailIcon;
    public Sprite NoMailSprite, HasMailSprite;

    public bool IsOpen;
    public GameObject Messager;
    RectTransform MessagerRT;


    public Vector3 ActivatePos, DeactivatePos;

    public GameObject QuestDone1, QuestDone2, QuestDone3;

    public SoundHandler SH;
    bool PlayedSFX;


    // Start is called before the first frame update
    void Start()
    {
        IsOpen = false;
        PlayedSFX = false;
        //MessagerRT = Messager.GetComponent<RectTransform>();

        //MessagerRT.transform.position = DeactivatePos;

        QuestDone1.SetActive(false);
        QuestDone2.SetActive(false);
        QuestDone3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {     
        if (HasMail)
        {
            if(!PlayedSFX)
            {
                SH.HasMail();
                PlayedSFX = true;
            }
           
            MailIcon.sprite = HasMailSprite;
        }
        else
        {
            PlayedSFX = false;
            MailIcon.sprite = NoMailSprite;
        }

        if (IsOpen)
        {
            Activated();
        }
        else
        {
            Deactivated();
        }

        Messager.SetActive(IsOpen);
        //Debug.Log(Messager.transform.position);
        CheckQuests();
        
    }

    void CheckQuests()
    {
        GlobalStats GS = GameObject.FindGameObjectWithTag("StatHandler").GetComponent<GlobalStats>();
        QuestDone1.SetActive(GS.CompletedMQ1);
        QuestDone2.SetActive(GS.CompletedMQ2);
        QuestDone3.SetActive(GS.CompletedMQ3);
    }

    void Activated()
    {
        //MessagerRT.transform.position = Vector3.Lerp(MessagerRT.transform.position, ActivatePos, 3 * Time.deltaTime);
        //Messager.transform.position = Vector3.Lerp(MessagerRT.transform.position, ActivatePos, 3 * Time.deltaTime);
        HasMail = false;
    }

    void Deactivated()
    {
        //MessagerRT.transform.position = Vector3.Lerp(MessagerRT.transform.position, DeactivatePos, 3 * Time.deltaTime);
        //Messager.transform.position = Vector3.Lerp(MessagerRT.transform.position, DeactivatePos, 3 * Time.deltaTime);
    }

    public void Activate()
    {
        IsOpen = !IsOpen;
    }
}
