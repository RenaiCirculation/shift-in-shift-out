﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugMenu : MonoBehaviour
{
    public InputField MoneyIF, DayIF, CustoLost, MoodIF;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RemoveCustomers()
    {
        if (SceneManager.GetActiveScene().name == "4_Shift")
        {
            GameObject[] Custo = GameObject.FindGameObjectsWithTag("Customer");
            foreach(GameObject x in Custo)
            {
                Destroy(x, 1);
            }
        }
    }

    public void RemoveCurCustomers()
    {
        if (SceneManager.GetActiveScene().name == "4_Shift")
        {
           GameObject.FindGameObjectWithTag("GameController").GetComponent<OrderHandler>().RemoveAllCurCustomers(); ;
        }

    }

    public void MoneyDebug()
    {
        if(MoneyIF.text != null)
            GlobalStats.instance.CurrentEarnings = float.Parse(MoneyIF.text);        
    }

    public void DayDebug()
    {
        if (DayIF.text != null)
            GlobalStats.instance.CurrentDay = int.Parse( DayIF.text);
    }   

    public void EndShift()
    {
        if(SceneManager.GetActiveScene().name == "4_Shift")
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<ShiftHandler>().internalTimer = 2f;
        }
    }

    public void CustoLostDebug()
    {
        if (CustoLost.text != null)
            GlobalStats.instance.TotalAmtCustoLost = int.Parse(CustoLost.text);
    }

    public void MoodDebug()
    {
        if(MoodIF.text != null)
        {
            GlobalStats.instance.gameObject.GetComponent<PlayerStats>().HappinessLevel = float.Parse(MoodIF.text);
        }
    }
}
