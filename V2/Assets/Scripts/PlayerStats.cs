﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int MaxHappiness;

    public float HappinessLevel;


    // Start is called before the first frame update
    void Start()
    {
        MaxHappiness = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if(HappinessLevel <= 0)
        {
            HappinessLevel = 0;
        }

        if(HappinessLevel >= MaxHappiness)
        {
            HappinessLevel = MaxHappiness;
        }
    }

    public void IncreaseMaxHappiness(int amt)
    {
        MaxHappiness += amt;
    }

    public void SetHappinessLvlStart()
    {
        HappinessLevel = 50;
        MaxHappiness = 100;
    }

    public void DecreaseHappinessLevel(float amt)
    {
        HappinessLevel -= amt;
    }
    public void IncreaseHappinessLevel(float amt)
    {
        HappinessLevel += amt;
    }
}
